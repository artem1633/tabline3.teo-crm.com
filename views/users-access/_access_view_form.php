<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\UsersAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-access-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row" style="padding: 20px;">

            <div class="col-md-12">
                <?= $form->field($model, 'user')->textInput(['disabled' => $model->isNewRecord ? false : true ])->label('Вам открыли совместный доступ к профилю') ?>
            </div>
            <div class="col-md-12"><hr></div>

            <div class="col-md-12">
                <?= $form->field($model, 'access_type')->radioList(['1'=>'Предоставить полный доступ','0'=>'Предоставить ограниченный доступ'],['class'=>'radio_list_disabled'])->label('<span style="color:red;">Тип доступа</span>'); ?>
            </div>

            <div id="additional" <?= $model->access_type == 1 ? 'style="display: none;"' : '' ?> >
                
                <div class="col-md-12"><hr></div>

                <div class="col-md-6">
                    <label style="color:red;">Страницы</label>
                    <span style="color:#9b9b9b;">
                        <?= $form->field($model, 'view_page')->checkBox(['disabled' => true]) ?>                    
                        <?= $form->field($model, 'update_page')->checkBox(['disabled' => true]) ?>
                    </span>
                </div>

                <div class="col-md-6">
                    <label style="color:red;">Заявки</label>
                    <span style="color:#9b9b9b;">
                        <?= $form->field($model, 'view_application')->checkBox(['disabled' => true]) ?>
                        <?= $form->field($model, 'update_application')->checkBox(['disabled' => true]) ?>
                    </span>
                </div>

                <div class="col-md-6">
                    <label style="color:red;">Другое</label>
                    <span style="color:#9b9b9b;">
                        <?= $form->field($model, 'view_statics')->checkBox(['disabled' => true]) ?>
                        <?= $form->field($model, 'update_settings')->checkBox(['disabled' => true]) ?>
                    </span>
                </div>
                <div style="display: none;">
                    <?= $form->field($model, 'user_from')->textInput() ?>
                </div>

            </div>

        </div>
  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>