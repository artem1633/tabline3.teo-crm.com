<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsersAccess */

?>
<div class="users-access-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
