<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use app\models\Products;

$this->title = 'Совместный доступ';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>

<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">Я открыл доступ</span>
                            <span class="hidden-xs">Я открыл доступ</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Доступные мне</span>
                            <span class="hidden-xs">Доступные мне</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="default-tab-1">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'active-pjax']) ?>
                                <div class="col-md-12 col-sm-12">
                                    <?=GridView::widget([
                                        'id'=>'crud-datatable',
                                        'dataProvider' => $dataProvider,
                                        //'filterModel' => $searchModel,
                                        'pjax'=>true,
                                        'columns' => require(__DIR__.'/_columns.php'),
                                        //'panelBeforeTemplate' => '',
                                        'toolbar'=> [
                                            ['content'=>
                                                Html::a('Открыть доступ', ['/users-access/create'],
                                                ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-success'])
                                            ],
                                        ],    
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,
                                        'panel' => [
                                            'headingOptions' => ['style' => 'display: none;'],
                                            'after'=>'',
                                        ]
                                        ])
                                    ?>                                    
                                </div>

                            <?php Pjax::end() ?>                        
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-2">
                        <div class="row">
                       
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'access-pjax']) ?>
                                <div class="col-md-12">
                                    <?=GridView::widget([
                                        'id'=>'crud-datatable',
                                        'dataProvider' => $dataProvider1,
                                        //'filterModel' => $searchArchives,
                                        'pjax'=>true,
                                        'columns' => require(__DIR__.'/_column_to_user.php'),
                                        //'panelBeforeTemplate' => '',
                                        'toolbar'=> [
                                            ['content'=>
                                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                                                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid'])
                                            ],
                                        ],
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,
                                        'panel' => [
                                            'headingOptions' => ['style' => 'display: none;'],
                                            'after'=>'',
                                        ]
                                        ])
                                    ?>
                                </div>
                            <?php Pjax::end() ?>                 
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
