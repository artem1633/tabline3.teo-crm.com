<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_from',
        'content' => function($data){
            return $data->userFrom->fio . ' (' . $data->userFrom->login . ')';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'access_type',
        'content' => function($data){
            if($data->access_type == 0) return 'Предоставить ограниченный доступ';
            if($data->access_type == 1) return 'Предоставить полный доступ';
        }
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate} &nbsp; {leadDelete}',
        'buttons'  => [            
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/users-access/view-access', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['role'=>'modal-remote','title'=>'Просмотр', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/users-access/remove', 'id' => $model->id]);
                return Html::a('<span class="fa fa-sign-out"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Отказаться от доступа', 
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Отказаться от доступа',
                    'data-confirm-message'=>'<span style="color:red; text-align:center; font-size:16px;">Вы уверены что хотите отказаться от доступа?</span>'
                ]);
            },
        ]
    ]

];   