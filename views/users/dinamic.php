<h1>jQuery Add & Remove Dynamically Input Fields in PHP</h1>
<form name="add_me" id="add_me">
	<table id="dynamic">
		<!-- <input type="text" name="name[]" placeholder="Enter Your Name"/> -->
		<button type="button" class="btn btn-primary" name="add" id="add_input">Add</button>
	</table>
	<input type="button" class="btn btn-success" name="submit" id="submit" value="Submit"/>
</form>

<?php 
$this->registerJs(<<<JS
$(document).ready(function(){
 var i=1;
 $('#add_input').click(function(){
 i++;
 $('#dynamic').append('<tr id="row'+i+'"><td><input type="text" class="form-control" name="name[]" placeholder="Enter Your Name"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-primary btn_remove">Remove</button></td></tr>');
 });
 $(document).on('click', '.btn_remove', function(){
 var button_id = $(this).attr("id");
 $('#row'+button_id+'').remove();
 });
 $('#submit').click(function(){
 $.ajax({
 url:"insert.php",
 method:"POST",
 data:$('#add_me').serialize(),
 success: function(data)
 {
 alert(data);
 $('#add_me')[0].reset();
 }
 });
 });
});
JS
);
?>
