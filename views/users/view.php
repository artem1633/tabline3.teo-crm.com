<?php
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

CrudAsset::register($this);
if (!file_exists('avatars/'.$model->avatar) || $model->avatar == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->avatar;
}


?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'change-pjax']) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Личный кабинет</h3>
                    <span class="pull-right">
                        <a class="btn btn-warning btn-xs" title="Изменить" role="modal-remote" href="<?=Url::toRoute(['/users/change', 'id' => $model->id])?>"><i class="glyphicon glyphicon-pencil" ></i></a>
                    </span>
                </div>
                <div class="panel-body">
                    <div style="margin-top: 10px;" class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td rowspan="6" style="text-align: center;"><?=Html::img($path, ['style' => 'width:250px; height:250px;', 'class' => 'img-circle', ])?>
                                    </td>
                                    <td colspan="2"><b><?=$model->getAttributeLabel('fio')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('login')?></b></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><?=$model->fio?></td>
                                    <td><?=$model->login?></td>
                                </tr>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('registry_date')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('telephone')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('last_payment_date')?></b></td>
                                </tr>
                                <tr>
                                    <td><?=\Yii::$app->formatter->asDate($model->registry_date, 'php:d.m.Y')?></td>
                                    <td><?=$model->telephone?></td>
                                    <td><?=\Yii::$app->formatter->asDate($model->last_payment_date, 'php:d.m.Y')?></td>
                                </tr>
                                <tr>
                                    <td><b><?=$model->getAttributeLabel('partner_code')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('tariff_id')?></b></td>
                                    <td><b><?=$model->getAttributeLabel('end_access')?></b></td>
                                </tr>
                                <tr>
                                    <td><?=$model->partner_code?></td>
                                    <td><?=$model->tariff->name?></td>
                                    <td><?=\Yii::$app->formatter->asDate($model->end_access, 'php:d.m.Y')?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>