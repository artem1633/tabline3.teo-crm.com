<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use app\models\additional\HistoryPages;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'login',
            [
                'attribute' => 'type',
                'value' => function($data){
                    return $data->getTypeDescription();
                }
            ],
            'telephone',
            'total_payment_amount',
            'registry_date',
            'last_payment_date',
            'end_access',
            [
                'attribute' => 'tariff_id',
                'value' => function($data){
                    return $data->tariff->name;
                }
            ],
            'partner_code',
            'promo_code',
        ],
    ]) ?>

    <?php if($model->type == 2) echo Html::a('Набор прав', ['/permissions/update', 'user_id' => $model->id], ['style' => 'width:100%;', 'role'=>'modal-remote','title'=> 'Набор прав','class'=>'btn btn-success']); ?>

    <?php if($model->type == 3) { 

        $history = HistoryPages::find()->where(['user_id' => $model->id])->all();
    ?>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th>Дата</th>
                        <th>Страница/Ссылка</th>
                        <th>Статус</th>
                    </tr>
                    <?php 
                    foreach ($history as $value) {
                    ?>
                    <tr>
                        <td><?=\Yii::$app->formatter->asDate($value->date, 'php:d.m.Y')?></td>
                        <td><?=$value->page?></td>
                        <td><?=$value->getStatusDescription()?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>               
        </div>

    <?php } ?>

</div>
