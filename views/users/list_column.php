<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'relation',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tariff_id',
        'header' => 'Статус',
        'content' => function($data){
            if(Yii::$app->user->identity->id == $data->id) return 'Текущий профиль';
            else return '';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tariff_id',
        'content' => function($data){
            if($data->tariff_id == 1) return '<span class="label label-warning">'.$data->tariff->name.'</label>';
            else return '<span class="label label-danger">'.$data->tariff->name.'</label>';
        }
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => ' {leadUpdate}',
        'buttons'  => [
            'leadUpdate' => function ($url, $model) {
                if($model->id != Yii::$app->user->identity->id){
                    $url = Url::to(['/users/toggle', 'id' => $model->id]);
                    return Html::a('Переключить', $url, [
                        'role'=>'modal-remote','title'=>'Переключить', 
                        'data-confirm'=>false, 'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены переключиться на этот аккаунт?',
                    ]);
                }
            },
        ]
    ]

];   