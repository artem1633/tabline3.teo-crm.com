<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use app\models\Products;

$this->title = 'Договор оферта';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>
<h2 style="text-align: center;">Договор оферта</h2>
<br>
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="">
                        <a href="#default-tab-1" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Договор оферта&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Договор оферта&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Политика конфиденциальности&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Политика конфиденциальности&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#default-tab-3" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">&nbsp;&nbsp;&nbsp; &nbsp;Юридическая информация&nbsp;&nbsp;&nbsp; &nbsp;</span>
                            <span class="hidden-xs">&nbsp;&nbsp;&nbsp; &nbsp;Юридическая информация&nbsp;&nbsp;&nbsp; &nbsp;</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade " id="default-tab-1">
                        <div class="row" style="margin-top: 20px;">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'design-pjax']) ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Договор оферта</h3>
                                            <span class="pull-right">
                                                <a class="btn btn-xs btn-warning" data-pjax="0" title="Изменить" href="<?=Url::toRoute(['edit-offer'])?>"><i class="fa fa-pencil"></i></a>
                                            </span>
                                        </div>
                                        <div class="panel-body">   
                                            <div class="table-responsive">              
                                                <?=$offer->offer?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php Pjax::end() ?>                        
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-2">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'common-pjax']) ?>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Политика конфиденциальности</h3>
                                            <span class="pull-right">
                                                <a class="btn btn-xs btn-warning" data-pjax="0" title="Изменить" href="<?=Url::toRoute(['edit-privacy'])?>"><i class="fa fa-pencil"></i></a>
                                            </span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <?=$privacy->content?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php Pjax::end() ?>
                    </div>

                    <div class="tab-pane fade active in" id="default-tab-3">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'legal-pjax']) ?>
                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    Юридическая информация
                                                </h3>
                                                    <span class="pull-right">
                                                        <a class="btn btn-xs btn-warning" role="modal-remote" title="Изменить" href="<?=Url::toRoute(['edit-legal', 'id' => 1])?>">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    </span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered">    
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('director')?></td>
                                                            <td ><?= $legal->director?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('address')?></td>
                                                            <td ><?= $legal->address?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('mailing_address')?></td>
                                                            <td ><?= $legal->mailing_address?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('state_registration')?></td>
                                                            <td ><?= $legal->state_registration?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('inn')?></td>
                                                            <td ><?= $legal->inn?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('ogrn')?></td>
                                                            <td ><?= $legal->ogrn?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold;"><?=$legal->getAttributeLabel('r_s')?></td>
                                                            <td ><?= $legal->r_s?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold; "><?=$legal->getAttributeLabel('bik')?></td>
                                                            <td ><?= $legal->bik?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size: 16px; font-weight: bold; "><?=$legal->getAttributeLabel('kor_schot')?></td>
                                                            <td ><?= $legal->kor_schot?></td> 
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php Pjax::end() ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
