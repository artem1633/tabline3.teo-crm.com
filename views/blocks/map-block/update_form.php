<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

\johnitvn\ajaxcrud\CrudAsset::register($this);


if($model->show_zoom && $model->show_select) $parameters = [ Map::CONTROL_ZOOM, Map::CONTROL_TYPE_SELECTOR, ];
if(!$model->show_zoom && $model->show_select) $parameters = [ Map::CONTROL_TYPE_SELECTOR, ];
if($model->show_zoom && !$model->show_select) $parameters = [ Map::CONTROL_ZOOM, ];
if(!$model->show_zoom && !$model->show_select) $parameters = [ ];
?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'map-pjax']) ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Карта</h3>

					<div class="btn-group pull-right" style="margin-top: -20px;">
			            <button type="button" class="btn btn-warning btn-xs"> <b>Действия</b></button>
			            <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			                <span style="margin-top: 0px;" class="caret"></span>
			            </button>

			            <ul class="dropdown-menu" role="menu">			                
			                <li><?= Html::a( 'Настройка', ['setting?id='.$model->id, ], ['role' => 'modal-remote',])?></li>
			                <?php if($model->add_mark) { ?><li><?= Html::a( 'Добавить метку', ['/map-marks/create?id='.$model->id, ], ['data-pjax' => '0'])?></li> <?php } ?>
			                <li><?= Html::a( 'Назад в страницу', ['/users-page/page?user_id='.$model->page->user_id, ], ['data-pjax' => '0',])?></li>                
			            </ul>

			        </div>

				</div>
				<div class="panel-body">
<?= YandexMaps::widget([
    'htmlOptions' => [
        'style' => 'height: 400px;',
    ],
    'map' => new Map('yandex_map', [
        'center' => [$model->center_x, $model->center_y],//'Tashkent',
        'zoom' => 10,
        'controls' => $parameters,
        'behaviors' => $model->lock_map ? array('default', 'scrollZoom') : array(),
        'type' => "yandex#map",
	    [
	        //	'minZoom' => 9,
	        //	'maxZoom' => 11,
	        'controls' => [
	          "new ymaps.control.SmallZoomControl()",
	          "new ymaps.control.TypeSelector(['yandex#map', 'yandex#satellite'])",
	        ],
	    ],
    ],
    [
        'objects' => $marks,
    ])
]) ?>
				</div>
			</div>
		</div>
	</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>
