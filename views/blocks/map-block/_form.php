<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;


?>

<div class="map-block-form">

    <?php $form = ActiveForm::begin(); ?>


	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Карта</h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">
<?= YandexMaps::widget([
    'htmlOptions' => [
        'style' => 'height: 400px;',
    ],
    'map' => new Map('yandex_map', [
        'center' => [55.7372, 37.6066],//'Tashkent',
        'zoom' => 10,
        'controls' => [Map::CONTROL_SEARCH, Map::CONTROL_ZOOM, Map::CONTROL_TYPE_SELECTOR, ],
        'behaviors' => array('default', 'scrollZoom'),
        'type' => "yandex#map",
	    [
	        //	'minZoom' => 9,
	        //	'maxZoom' => 11,
	        'controls' => [
	          "new ymaps.control.SmallZoomControl()",
	          "new ymaps.control.TypeSelector(['yandex#map', 'yandex#satellite'])",
	        ],
	    ],
    ],
    [
        'objects' => [
        	new Placemark(new Point(55.7372, 37.6066), 
	        	[ 'balloonContent' => '<strong>Адрес</strong> <br><a target="_blank" href="daryo.uz">Описания</a>',], 
	        	[
			       'draggable' => false,
			       'preset' => 'islands#dotIcon',
			       'iconColor' => 'red',
			       'events' => [
			           'dragend' => 'js:function (e) {
			               console.log(e.get(\'target\').geometry.getCoordinates());
			           }'
		        	]
	        	]
	        ),
	        new Placemark(new Point(55.6372, 37.6066), 
	        	[ 'balloonContent' => '<strong>Адрес</strong> <br><a target="_blank" href="daryo.uz">Описания</a>',], 
	        	[
			       'draggable' => false,
			       'preset' => 'islands#dotIcon',
			       'iconColor' => '#2E9BB9',
			       'events' => [
			           'dragend' => 'js:function (e) {
			               console.log(e.get(\'target\').geometry.getCoordinates());
			           }'
		        	]
	        	]
	        )
        ]
    ])
]) ?>
				</div>
			</div>
		</div>
	</div>

    <?= $form->field($model, 'page_id')->textInput() ?>

    <?= $form->field($model, 'coordinates')->textarea(['rows' => 6]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){

	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var panel = $(this);
		if(!panel.hasClass('panel-collapsed')) {
			panel.parents('.panel').find('.panel-body').slideUp();
			panel.addClass('panel-collapsed');
			panel.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
			
		} else {
			panel.parents('.panel').find('.panel-body').slideDown();
			panel.removeClass('panel-collapsed');
			panel.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
			
		}
	});

});
JS
);
?>


		<!-- <div class="container">
		    <div class="row">
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Panel 1</h3>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down"></i></span>
						</div>
						<div class="panel-body">Panel content</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Panel 2</h3>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">Panel content</div>
				</div>
			</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">Panel 3</h3>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">Panel content</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-warning">
						<div class="panel-heading">
							<h3 class="panel-title">Panel 4</h3>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">Panel content</div>
					</div>
				</div>
			</div>
		</div> -->
