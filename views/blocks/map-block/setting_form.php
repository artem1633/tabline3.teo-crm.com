<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Карта</b></span>
                        <span class="hidden-xs"><b>Карта</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>
                    <div class="row">
                        <div style="margin-left: 90px;">
                            <div class="col-md-12">
                                <?= $form->field($model, "lock_map")->checkbox([]); ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, "add_mark")->checkbox([]); ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, "show_select")->checkbox([]); ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, "show_zoom")->checkbox([]); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>