<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\MapBlock */
?>
<div class="map-block-update">

    <?= $this->render('update_form', [
        'model' => $model,
        'marks' => $marks,
    ]) ?>

</div>
