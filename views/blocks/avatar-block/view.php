<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\AvatarBlock */
?>
<div class="avatar-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'avatar_size',
            'hide_avatar',
        ],
    ]) ?>

</div>
