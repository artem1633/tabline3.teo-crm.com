<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\SocialBlock */
?>
<div class="social-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'display',
            'facebook',
            'facebook_link',
            'facebook_title',
            'instagram',
            'instagram_link',
            'instagram_title',
            'vk',
            'vk_link',
            'vk_title',
            'odno',
            'odno_link',
            'odno_title',
            'twitter',
            'twitter_link',
            'twitter_title',
            'youtube',
            'youtube_link',
            'youtube_title',
            'pinterest',
            'pinterest_link',
            'pinterest_title',
            'snapchat',
            'snapchat_link',
            'snapchat_title',
        ],
    ]) ?>

</div>
