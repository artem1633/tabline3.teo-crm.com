<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
use mihaildev\ckeditor\CKEditor;

if (!file_exists('images/unique-selling-block/'.$model->image) || $model->image == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-images.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/unique-selling-block/'.$model->image;
}
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Предложение</b></span>
                        <span class="hidden-xs"><b>Предложение</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <div style="padding:20px;">
                        <div class="row">
                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>            
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <?= $form->field($model, 'button_text')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
                            </div>          
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'text')->widget(CKEditor::className(),[
                                'editorOptions' => [
                                    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                                    'inline' => false, //по умолчанию false
                                    'height' => '100px',
                                ],
                            ]);
                            ?>
                            </div>          
                            <div class="col-md-8 col-xs-12">
                                <div class="table-responsive">
                                    <center>
                                        <?=Html::img($path, [
                                            'style' => 'width:500px; height:200px;',
                                            //'class' => 'img-circle',
                                        ])?>
                                    </center>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <?= $form->field($model, 'file')->fileInput(); ?>
                            </div>
                        </div>

                        <div style="display: none;">
                            <?= $form->field($model, 'page_id')->textInput() ?>        
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
