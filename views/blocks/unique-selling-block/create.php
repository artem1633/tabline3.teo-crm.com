<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\UniqueSellingBlock */

?>
<div class="unique-selling-block-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
