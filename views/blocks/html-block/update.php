<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\HtmlBlock */
?>
<div class="html-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
