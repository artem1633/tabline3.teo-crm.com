<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\SeparatorBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="separator-block-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'separator_size')->dropDownList($model->getAlignmentSize(), []) ?>

	<div style="display: none;">
    	<?= $form->field($model, 'page_id')->textInput() ?>
  	</div>
  	
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
