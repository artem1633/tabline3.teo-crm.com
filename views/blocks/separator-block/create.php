<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\SeparatorBlock */

?>
<div class="separator-block-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
