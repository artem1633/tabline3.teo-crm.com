<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

if($model->list_data != null && !is_array($model->list_data))
    $model->list_datas = explode(',', $model->list_data);

if($model->select_data != null && !is_array($model->select_data))
    $model->select_datas = explode(',', $model->select_data);
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Форма контактов</b></span>
                        <span class="hidden-xs"><b>Форма контактов</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>
                    <?= $form->field($model, 'type')->dropDownList($model->getList(), ['prompt' => 'Выберите тип', 'id' => 'type']) ?>

                    <div style="display: none;">
                        <?= $form->field($model, 'name')->textInput() ?>
                        <?= $form->field($model, 'telephone')->textInput() ?>
                        <?= $form->field($model, 'email')->textInput() ?>
                        <?= $form->field($model, 'line')->textInput() ?>  
                        <?= $form->field($model, 'number')->textInput() ?> 
                        <?= $form->field($model, 'list')->textInput() ?> 
                        <?= $form->field($model, 'check')->textInput() ?>  
                        <?= $form->field($model, 'select')->textInput() ?>  
                        <?= $form->field($model, 'text')->textInput() ?>
                        <?= $form->field($model, 'country')->textInput() ?>
                        <?= $form->field($model, 'page_id')->textInput() ?>        
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->name) { ?> style="display: none;" <?php } ?> id="accordion1">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#infoPanel1">
                                        Имя <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="name" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'name_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'name_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'name_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->telephone) { ?> style="display: none;" <?php } ?> id="accordion2">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#infoPanel2">
                                        Телефон <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="telephone" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'telephone_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'telephone_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'telephone_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->email) { ?> style="display: none;" <?php } ?> id="accordion3">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#infoPanel3">
                                        E-mail <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="email" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'email_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'email_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'email_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->line) { ?> style="display: none;" <?php } ?> id="accordion4">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#infoPanel4">
                                        Строка <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="line" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'line_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'line_description')->textarea(['rows' => 3])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'line_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->number) { ?> style="display: none;" <?php } ?> id="accordion5">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#infoPanel5">
                                        Число <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="number" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'number_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'number_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'number_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->list) { ?> style="display: none;" <?php } ?> id="accordion6">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion6" href="#infoPanel6">
                                        Список <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="list" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'list_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'list_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'list_datas')->widget(\kartik\select2\Select2::class, [
                                            'options' => ['multiple' => true],
                                            'pluginOptions' => [
                                                'tags' => true,
                                            ],
                                        ]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'list_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="footer-widget-panel panel-group" <?php if(!$model->check) { ?> style="display: none;" <?php } ?> id="accordion7">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion7" href="#infoPanel7">
                                        Галочка <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="check" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel7" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'check_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'check_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'check_selected')->checkbox([]); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'check_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->select) { ?> style="display: none;" <?php } ?> id="accordion8">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion8" href="#infoPanel8">
                                        Выбор <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="select" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'select_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'select_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'select_datas')->widget(\kartik\select2\Select2::class, [
                                            'options' => ['multiple' => true],
                                            'pluginOptions' => [
                                                'tags' => true,
                                            ],
                                        ]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'select_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->text) { ?> style="display: none;" <?php } ?> id="accordion9">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion9" href="#infoPanel9">
                                        Текстовое поле <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="text" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel9" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'text_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'text_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'text_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="footer-widget-panel panel-group" <?php if(!$model->country) { ?> style="display: none;" <?php } ?> id="accordion10">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #aadbf3;">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion10" href="#infoPanel10">
                                        Страна <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </h4>
                                <a href="#" class="pull-right text-danger" id="country" style="margin-top: -20px;" ><i class="glyphicon glyphicon-trash"></i></a>
                            </div>
                            <div id="infoPanel10" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'country_title')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'country_description')->textInput(['maxlength' => true])->label('Описание поля') ?>
                                    </div>
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'country_required')->checkBox([]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    <?php if (!Yii::$app->request->isAjax){ ?>
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    <?php } ?>

                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <div style="padding: 5px;">
                        <div class="row">
                            <div class="col-md-7">
                                <?= $form->field($model, 'title_text')->textInput() ?>
                            </div>
                            <div class="col-md-5">
                                <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>80,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-7">
                                <?= $form->field($model, 'name_in_menu')->textInput() ?>
                            </div>
                            <div class="col-md-5">
                                <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>80,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'button_text')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'button_color')->textInput(['type' => 'color']) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'button_background')->textInput(['type' => 'color']) ?>
                            </div>
                        </div>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'action_click')->dropDownList($model->getActionList(), ['id' => 'action_click']) ?>
                                </div>
                                <div class="col-md-12" id="sms" <?= $model->action_click != 'sms' ? 'style="display: none;"' : '' ?> >
                                    <?= $form->field($model, 'sms')->textInput([]) ?>            
                                </div>
                                <div class="col-md-12" id="site" <?= $model->action_click != 'site' ? 'style="display: none;"' : '' ?> >
                                    <?= $form->field($model, 'site')->textInput([]) ?>
                                </div>
                                <div class="col-md-12" id="page" <?= $model->action_click != 'page' ? 'style="display: none;"' : '' ?> >
                                    <?= $form->field($model, 'page')->dropDownList($model->getUsersPages(),[]) ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS
    $('#type').on('change', function() 
    {  
        var type = this.value;

        if( type == 'name' && $('#contactformblock-name').val() == 0 ){
            $('#contactformblock-name').val(1);
            $('#accordion1').show();
        }

        if( type == 'telephone' && $('#contactformblock-telephone').val() == 0 ){
            $('#contactformblock-telephone').val(1);
            $('#accordion2').show();
        }

        if( type == 'email' && $('#contactformblock-email').val() == 0 ){
            $('#contactformblock-email').val(1);
            $('#accordion3').show();
        }

        if( type == 'line' && $('#contactformblock-line').val() == 0 ){
            $('#contactformblock-line').val(1);
            $('#accordion4').show();
        }

        if( type == 'number' && $('#contactformblock-number').val() == 0 ){
            $('#contactformblock-number').val(1);
            $('#accordion5').show();
        }

        if( type == 'list' && $('#contactformblock-list').val() == 0 ){
            $('#contactformblock-list').val(1);
            $('#accordion6').show();
        }

        if( type == 'check' && $('#contactformblock-check').val() == 0 ){
            $('#contactformblock-check').val(1);
            $('#accordion7').show();
        }

        if( type == 'select' && $('#contactformblock-select').val() == 0 ){
            $('#contactformblock-select').val(1);
            $('#accordion8').show();
        }

        if( type == 'text' && $('#contactformblock-text').val() == 0 ){
            $('#contactformblock-text').val(1);
            $('#accordion9').show();
        }

        if( type == 'country' && $('#contactformblock-country').val() == 0 ){
            $('#contactformblock-country').val(1);
            $('#accordion10').show();
        }

    }
    );

    $('#name').on('click', function() 
    { 
        $('#contactformblock-name').val(0);
        $('#contactformblock-name_title').val('Имя');
        $('#contactformblock-name_description').val('Введите ваше имя');
        $('#accordion1').hide();
    });

    $('#telephone').on('click', function() 
    { 
        $('#contactformblock-telephone').val(0);
        $('#contactformblock-telephone_title').val('Телефон');
        $('#contactformblock-telephone_description').val('Введите номер телефона');
        $('#accordion2').hide();
    });

    $('#email').on('click', function() 
    { 
        $('#contactformblock-email').val(0);
        $('#contactformblock-email_title').val('E-mail');
        $('#contactformblock-email_description').val('Введите ваш email');
        $('#accordion3').hide();
    });

    $('#line').on('click', function() 
    { 
        $('#contactformblock-line').val(0);
        $('#contactformblock-line_title').val('Строка');
        $('#contactformblock-line_description').val('Введите текст здесь');
        $('#accordion4').hide();
    });

    $('#number').on('click', function() 
    { 
        $('#contactformblock-number').val(0);
        $('#contactformblock-number_title').val('Число');
        $('#contactformblock-number_description').val('Введите любое число');
        $('#accordion5').hide();
    });

    $('#list').on('click', function() 
    { 
        $('#contactformblock-list').val(0);
        $('#contactformblock-list_title').val('Список');
        $('#contactformblock-list_description').val('Выбор из списка');
        $('#accordion6').hide();
    });

    $('#check').on('click', function() 
    { 
        $('#contactformblock-check').val(0);
        $('#contactformblock-check_title').val('Галочка');
        $('#contactformblock-check_description').val('Описание поля');
        $('#accordion7').hide();
    });

    $('#select').on('click', function() 
    { 
        $('#contactformblock-select').val(0);
        $('#contactformblock-select_title').val('Выбор');
        $('#contactformblock-select_description').val('Описание поля');
        $('#accordion8').hide();
    });

    $('#text').on('click', function() 
    { 
        $('#contactformblock-text').val(0);
        $('#contactformblock-text_title').val('Текстовое поле');
        $('#contactformblock-text_description').val('Поля для ввода текста');
        $('#accordion9').hide();
    });

    $('#country').on('click', function() 
    { 
        $('#contactformblock-country').val(0);
        $('#contactformblock-country_title').val('Страна');
        $('#contactformblock-country_description').val('Выберите');
        $('#accordion10').hide();
    });

JS
);
?>

<?php 
$this->registerJs(<<<JS
    $('#action_click').on('change', function() 
    {  
        var type = this.value;

        $('#sms').hide();
        $('#site').hide();
        $('#page').hide();

        if(type == 'sms') { $('#sms').show();   }
        if(type == 'site') { $('#site').show(); }
        if(type == 'page') { $('#page').show(); }
    }
);
JS
);
?>