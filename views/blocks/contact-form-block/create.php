<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\ContactFormBlock */

?>
<div class="contact-form-block-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
