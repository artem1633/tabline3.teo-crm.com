<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\VideoBlock */
?>
<div class="video-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'link',
            'description:ntext',
        ],
    ]) ?>

</div>
