<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Ссылка</b></span>
                        <span class="hidden-xs"><b>Ссылка</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Статистика</b></span>
                        <span class="hidden-xs"><b>Статистика</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>                    
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'caption')->textInput(['maxlength' => true]) ?>            
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <?= $form->field($model, 'action_click')->dropDownList($model->getActionList(), ['id' => 'action_click']) ?>
                            </div>
                            <div class="col-md-7" id="site_url" <?= $model->action_click != 'site_url' ? 'style="display: none;"' : '' ?> >
                                <?= $form->field($model, 'site_url')->textInput([]) ?>            
                            </div>
                            <div class="col-md-7" id="call" <?= $model->action_click != 'call' ? 'style="display: none;"' : '' ?> >
                                <?= $form->field($model, 'call')->textInput([]) ?>
                            </div>
                            <div class="col-md-7" id="email" <?= $model->action_click != 'email' ? 'style="display: none;"' : '' ?> >
                                <?= $form->field($model, 'email')->textInput([]) ?>
                            </div>
                            <div class="col-md-7" id="other_page_name" <?= $model->action_click != 'other_page_name' ? 'style="display: none;"' : '' ?> >
                                <?= $form->field($model, 'other_page_name')->textInput([]) ?>
                            </div>
                        </div>

                        <div style="display: none;">
                            <?= $form->field($model, 'page_id')->textInput() ?>      
                        </div>
                      
                        <?php if (!Yii::$app->request->isAjax){ ?>
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        <?php } ?>

                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-3">
                    <br>
                    <div class="row">
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">День</h3>
                                    <span class="pull-right"><?=date('d.m.Y')?></span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$day_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Неделя</h3>
                                    <span class="pull-right">
                                        <?=date('d.m.Y', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) ). ' - ' .date('d.m.Y')?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$week_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Месяц</h3>
                                    <span class="pull-right">
                                        <?= date('01.m.Y') . ' - ' .date('d.m.Y') ?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$month_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS
    $('#action_click').on('change', function() 
    {  
        var type = this.value;

        $('#site_url').hide();
        $('#call').hide();
        $('#email').hide();
        $('#other_page_name').hide();

        if(type == 'site_url') { $('#site_url').show();   }
        if(type == 'call') { $('#call').show(); }
        if(type == 'email') { $('#email').show();  }
        if(type == 'other_page_name') { $('#other_page_name').show(); }
    }
);
JS
);
?>
