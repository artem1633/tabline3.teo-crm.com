<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\TextBlock */

?>
<div class="text-block-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
