<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\TextBlock */
?>
<div class="text-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'text_size',
            'alignment',
        ],
    ]) ?>

</div>
