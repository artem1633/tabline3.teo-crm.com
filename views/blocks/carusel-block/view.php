<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\CaruselBlock */
?>
<div class="carusel-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'number',
            'size_slide',
            'time:datetime',
            'image',
            'title',
            'text:ntext',
        ],
    ]) ?>

</div>
