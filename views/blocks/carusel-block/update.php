<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\CaruselBlock */
?>
<div class="carusel-block-update">

    <?= $this->render('update_form', [
        'model' => $model,
        'carusels' => $carusels,
        'number' => $number,
        'count' => $count,
        'day_count' => $day_count,
        'week_count' => $week_count,
        'month_count' => $month_count,
    ]) ?>

</div>
