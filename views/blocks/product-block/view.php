<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\ProductBlock */
?>
<div class="product-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'number',
            'view',
            'product_id',
            'contact_form_id',
            'checkbox',
            'foto_visible',
            'name_visible',
            'description_visible',
            'price_visible',
            'contact_form_visible',
            'button_visible',
            'visible',
            'title_text',
            'visible_in_menu',
            'name_in_menu',
        ],
    ]) ?>

</div>
