<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

$k=0;
?>
<?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Внешний вид</b></span>
                        <span class="hidden-xs"><b>Внешний вид</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Оплаты</b></span>
                        <span class="hidden-xs"><b>Оплаты</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-4" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Модули</b></span>
                        <span class="hidden-xs"><b>Модули</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>                    
                    <div style="padding: 20px;">
                        <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input"> + Добавить новый товар</button>
                        <div id="dynamic">
                            <?php
                            foreach ($products as $product) {
                                $k++;
                            ?>
                            <div class="row" id="row<?=$k?>">
                                <div class="col-md-12">
                                    <label class="control-label" ><br>Товар<?=$k?> </label>
                                </div> 
                                <div class="col-md-11 col-xs-10"> 
                                    <select class="form-control" id="products" name="products[]"><?=$product->getProductsList()?></select>
                                </div> 
                                <div class="col-md-1 col-xs-1" style="margin-left:-20px;">
                                    <button type="button" name="remove" id="<?=$k?>" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                </div> 
                                    <input type="hidden" class="form-control" name="product_id[]" value="<?=$product->id?>"/> 
                            </div>
                            <?php
                            } ?>
                            <input type="hidden" class="form-control" id="count" name="count" value="<?=$k?>"/> 
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'view')->dropDownList($model->getViewList(), []) ?>            
                            </div>
                            <div class="col-md-1">
                                <?= $form->field($model, 'checkbox')->checkBox([]) ?>
                            </div>
                            <div class="col-md-11">
                                <?= Html::a('Отправляя эту форму, я принимаю соглашение об обработке пользовательских данных', ['/common/legal-information', 'user_id' => $model->page->user_id], [ 'data-pjax' => '0', 'target' => '_blank']); ?>
                            </div>
                        </div>
                    </div>

                        <?php if (!Yii::$app->request->isAjax){ ?>
                            <div class="form-group">
                                <?= Html::submitButton('Создать', ['class' =>  'btn btn-success']) ?>
                            </div>
                        <?php } ?>

                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div style="padding: 20px;">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'title_text')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'name_in_menu')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'foto_visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'name_visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'description_visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'price_visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'contact_form_visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'button_visible')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>100,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-3">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="color:#269abc; text-align: center;"> В работе</h3>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="default-tab-4">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="color:#269abc; text-align: center;"> В работе</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var i = document.getElementById('count').value;
    var options = '';
    $.post( "/product-block/lists", function( data ){ options = data; /*alert(options); $( "#products" ).html( data);*/ });
    var fileCollection = new Array();
    $('#add_input').click(function(){
    i++;
    $('#dynamic').append('<div class="row" id="row'+i+'"><div class="col-md-12"><label class="control-label" ><br>Товар'+i+' </label></div> <div class="col-md-11 col-xs-10"> <select class="form-control" id="products" name="products[]">'+options+'</select> </div> <div style="margin-left:-20px;" class="col-md-1 col-xs-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> </div> </div>');
    });

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

});
JS
);
?>