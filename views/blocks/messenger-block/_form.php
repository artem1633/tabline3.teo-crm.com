<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Мессенджеры</b></span>
                        <span class="hidden-xs"><b>Мессенджеры</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Статистика</b></span>
                        <span class="hidden-xs"><b>Статистика</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>                    
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'display')->dropDownList($model->getDisplayList(), []) ?>            
                                </div>
                            </div>

                            <div class="footer-widget-panel panel-group" id="accordion1">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;"><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#infoPanel1">WhatsApp</a></h4>
                                    </div>
                                    <div id="infoPanel1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'whatsapp_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?= $form->field($model, 'whatsapp_phone')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'whatsapp_text')->textArea(['rows' => 3]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'whatsapp')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-widget-panel panel-group" id="accordion2">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#infoPanel2">Telegram</a></h4>
                                    </div>
                                    <div id="infoPanel2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'telegram_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'telegram_user')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'telegram')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-widget-panel panel-group" id="accordion3">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#infoPanel3">Facebook</a></h4>
                                    </div>
                                    <div id="infoPanel3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'facebook_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'facebook_user')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'facebook')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-widget-panel panel-group" id="accordion4">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#infoPanel4">ВКонтакте</a></h4>
                                    </div>
                                    <div id="infoPanel4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'vk_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'vk_user')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'vk')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-widget-panel panel-group" id="accordion5">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion5" href="#infoPanel5">Viber</a></h4>
                                    </div>
                                    <div id="infoPanel5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'viber_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'viber_phone')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'viber')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="footer-widget-panel panel-group" id="accordion6">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion6" href="#infoPanel6">Skype</a></h4>
                                    </div>
                                    <div id="infoPanel6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'skype_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'skype_user')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'skype')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            <div class="footer-widget-panel panel-group" id="accordion7">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion7" href="#infoPanel7">Звонок</a></h4>
                                    </div>
                                    <div id="infoPanel7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'phone')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div class="footer-widget-panel panel-group" id="accordion8">
                                <div class="panel panel-default">
                                    <div class="panel-heading" style="background-color: #aadbf3;" ><h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion8" href="#infoPanel8">E-mail</a></h4>
                                    </div>
                                    <div id="infoPanel8" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'email_title')->textInput(['maxlength' => true]) ?>
                                            </div>
                                            <div class="col-md-12">
                                                <?= $form->field($model, 'email')->checkBox([ 'label' => 'Показать/Скрыть' ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  

                            <div style="display: none;">
                                <?= $form->field($model, 'page_id')->textInput() ?>        
                            </div>
                      
                        <?php if (!Yii::$app->request->isAjax){ ?>
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>
                        <?php } ?>

                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-3">
                    <br>
                    <div class="row">
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">День</h3>
                                    <span class="pull-right"><?=date('d.m.Y')?></span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$day_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Неделя</h3>
                                    <span class="pull-right">
                                        <?=date('d.m.Y', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) ). ' - ' .date('d.m.Y')?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$week_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 20px;">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Месяц</h3>
                                    <span class="pull-right">
                                        <?= date('01.m.Y') . ' - ' .date('d.m.Y') ?>
                                    </span>
                                </div>
                                <div class="panel-body">
                                    <center><h3><b> <span style="color:red;"><?=$month_count?></span> раз кликано</b></h3></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
