<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\MessengerBlock */
?>
<div class="messenger-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'display',
            'whatsapp',
            'whatsapp_title',
            'whatsapp_phone',
            'whatsapp_text:ntext',
            'telegram',
            'telegram_title',
            'telegram_user',
            'facebook',
            'facebook_title',
            'facebook_user',
            'vk',
            'vk_title',
            'vk_user',
            'viber',
            'viber_title',
            'viber_phone',
            'skype',
            'skype_title',
            'skype_user',
            'phone',
            'phone_number',
            'email:email',
            'email_title:email',
        ],
    ]) ?>

</div>
