<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

?>

<div class="map-marks-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'title')->textInput(['id' => 'title']) ?>            
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'description')->textInput([]) ?>         
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'color')->textInput(['type' => 'color']) ?>            
        </div>
    </div>
    
    <div style="display: none;">        
        <?= $form->field($model, 'map_id')->textInput() ?>
        <?= $form->field($model, 'coordinate_x')->textInput(['id' => 'coordinate_x']) ?>
        <?= $form->field($model, 'coordinate_y')->textInput(['id' => 'coordinate_y']) ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Метка</h3>
                    <div class="pull-right">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', [ 'style' => 'margin-top:-40px;', 'class' => $model->isNewRecord ? 'btn btn-success btn-xs' : 'btn btn-warning btn-xs']) ?>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <?= YandexMaps::widget([
                        'htmlOptions' => [
                            'style' => 'height: 400px;',
                        ],
                        'map' => new Map('yandex_map', [
                                'center' => [$model->coordinate_x, $model->coordinate_y],
                                'zoom' => 12,
                                'controls' => [Map::CONTROL_ZOOM],
                                'behaviors' => [Map::BEHAVIOR_DRAG],
                                'type' => "yandex#map",
                            ],
                        [
                            'objects' => [
                                new Placemark(new Point($model->coordinate_x, $model->coordinate_y), [], [
                                    'draggable' => true,
                                    'preset' => 'islands#dotIcon',
                                    'iconColor' => $model->color,
                                    'events' => [
                                        'dragend' => 'js:function (e) {
                                            
                                            //console.log(e.get(\'target\').geometry.getCoordinates());                                        
                                            var coords = e.get(\'target\').geometry.getCoordinates();
                                            $( "#coordinate_x" ).val( coords[0]);
                                            $( "#coordinate_y" ).val( coords[1]);

                                            var house = ymaps.geocode(coords, {kind: \'house\'});

                                            house.then(
                                                function (res) {
                                                    var street = res.geoObjects.get(0);
                                                    //console.log(street.properties);
                                                    var name = street.properties.get(\'name\');
                                                    var dom_number = "";
                                                    var q=0;
                                                    for (var i = 0; i < name.length; i++) {
                                                        //alert(name.charAt(i));
                                                        if(q==1) dom_number += name.charAt(i);
                                                        if(name.charAt(i) == ",") q=1;
                                                        //var a = name.charCodeAt(i);
                                                    }
                                                    $( "#title" ).val( name);
                                                });                                                   
                                        }',                                
                                    ]
                                ])
                            ]
                        ])
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>