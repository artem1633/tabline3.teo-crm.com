<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\MapMarks */
?>
<div class="map-marks-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'map_id',
            'coordinate_x',
            'coordinate_y',
            'title',
            'description:ntext',
            'color',
        ],
    ]) ?>

</div>
