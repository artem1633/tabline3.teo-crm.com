<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\MapMarks */

?>
<div class="map-marks-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
