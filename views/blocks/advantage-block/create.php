<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\AdvantageBlock */

?>
<div class="advantage-block-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
