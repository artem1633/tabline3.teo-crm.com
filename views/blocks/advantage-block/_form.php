<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

?>

<?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Преимущество</b></span>
                        <span class="hidden-xs"><b>Преимущество</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Настройка</b></span>
                        <span class="hidden-xs"><b>Настройка</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>
                    <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input"> + Добавить новый слайд</button>
                    <div id="dynamic"></div>
                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <div class="row">
                        <div class="col-md-7">
                            <?= $form->field($model, 'title_text')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-7">
                            <?= $form->field($model, 'name_in_menu')->textInput() ?>
                        </div>
                        <div class="col-md-5">
                            <?= $form->field($model, 'visible_in_menu')->widget(SwitchInput::classname(), [
                                'type' => SwitchInput::CHECKBOX,
                                'pluginOptions'=>[
                                    'handleWidth'=>80,
                                    'onText'=>'Да',
                                    'offText'=>'Нет'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var i=0;
    var fileCollection = new Array();
    $('#add_input').click(function(){
    i++;
    $('#dynamic').append('<div class="row" id="row'+i+'"><div class="col-md-12"><label class="control-label" ><br>Слайд'+i+' </label></div> <div class="col-md-11"> <input type="text" class="form-control" name="titles[]" placeholder="Заголовок"/> </div> <div class="col-md-1" style="margin-left:-20px;" ><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> </div> <div class="col-md-11"><br><textarea  class="form-control" name="texts[]" placeholder="Описание"></textarea></div><div class="col-md-11"><br><div id="images-to-upload'+i+'"></div><div class="col-md-12"><br><input type="file" name="images[]" accept=".svg,.jpg,.jpeg,.png" class="btn_image"  id="'+i+'"></div></div></div>');
    });

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

    $(document).on('change', '.btn_image', function(e){
        var files = e.target.files;
        var button_id = $(this).attr("id");
        //alert(button_id);
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%;" src="'+e.target.result+'"> ';
                $('#images-to-upload'+button_id+'').html('');
                //document.getElementById('theID').value = 'new value';
                $('#images-to-upload'+button_id+'').append(template);
            };
        });
    });

});
JS
);
?>