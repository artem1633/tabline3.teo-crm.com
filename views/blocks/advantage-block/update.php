<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\AdvantageBlock */
?>
<div class="advantage-block-update">

    <?= $this->render('update_form', [
        'advantages' => $advantages,
        'number' => $number,
        'count' => $count,
        'model' => $model,
    ]) ?>

</div>
