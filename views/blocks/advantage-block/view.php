<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\AdvantageBlock */
?>
<div class="advantage-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'number',
            'image',
            'title',
            'text:ntext',
        ],
    ]) ?>

</div>
