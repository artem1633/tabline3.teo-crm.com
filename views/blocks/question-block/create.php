<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\blocks\QuestionBlock */

?>
<div class="question-block-create">
    <?= $this->render('_form', [
    	'model' => $model,
    ]) ?>
</div>
