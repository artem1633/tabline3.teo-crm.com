<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\QuestionBlock */
?>
<div class="question-block-update">

    <?= $this->render('update_form', [
        'questions' => $questions,
        'number' => $number,
        'count' => $count,
        'model' => $model,
    ]) ?>

</div>
