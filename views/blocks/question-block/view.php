<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\blocks\QuestionBlock */
?>
<div class="question-block-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_id',
            'question_title:ntext',
            'answer:ntext',
        ],
    ]) ?>

</div>
