<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\additional\PromoCodes */
?>
<div class="promo-codes-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            [
                'attribute' => 'access_date',
                'value' => function($data){
                    return date('d.m.Y', strtotime($data->access_date));
                }
            ],
            'remaining_input',
            [
                'attribute' => 'status',
                'value' => function($data){
                    return $data->getStatusDescription();
                }
            ],
            'protsent',
            'ended_input',
        ],
    ]) ?>

</div>
