<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\additional\Permissions;

$type = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;
$disabled = false;
if($type == 2)
{
    $permission = Permissions::find()->where(['user_id' => $user_id])->one();
    if(!$permission->change_promocode && !$model->isNewRecord) $disabled = true;
}

/* @var $this yii\web\View */
/* @var $model app\models\additional\PromoCodes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-codes-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>                
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6">
                <?= $form->field($model, 'status')->dropDownList($model->getStatusList(),[])
                ?>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6">
                <?= $form->field($model, 'access_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите дату', 'disabled' => $disabled],
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                    ]
                    ])
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6">
                <?= $form->field($model, 'remaining_input')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6">
                <?= $form->field($model, 'protsent')->textInput(['type' => 'number']) ?>
            </div>
        </div>
  
    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
