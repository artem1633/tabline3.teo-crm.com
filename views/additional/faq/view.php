<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Faq */
?>
<div class="faq-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'question:ntext',
            'answer:ntext',
            [
            	'attribute' => 'status',
            	'value' => $model->getStatusDescription(),
            ],
        ],
    ]) ?>

</div>
