<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Faq */
?>
<div class="faq-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
