<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Permissions */
?>
<div class="permissions-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
