<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Permissions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="permissions-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'view_user')->checkBox() ?>

    <?= $form->field($model, 'delete_user')->checkBox() ?>

    <?= $form->field($model, 'create_user')->checkBox() ?>

    <?= $form->field($model, 'change_tariff')->checkBox() ?>

    <?= $form->field($model, 'view_page')->checkBox() ?>

    <?= $form->field($model, 'change_content_page')->checkBox() ?>

    <?= $form->field($model, 'change_price_list')->checkBox() ?>

    <?= $form->field($model, 'answer_to_questions')->checkBox() ?>

    <?= $form->field($model, 'change_promocode')->checkBox() ?>

    <?= $form->field($model, 'close_window')->checkBox() ?>

    <?= $form->field($model, 'free_period')->checkBox() ?>

    <?= $form->field($model, 'execute_order')->checkBox() ?>

    <?= $form->field($model, 'unlock_registration')->checkBox() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
