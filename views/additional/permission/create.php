<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\additional\Permissions */

?>
<div class="permissions-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
