<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\additional\Permissions */
?>
<div class="permissions-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'view_user',
            'delete_user',
            'create_user',
            'change_tariff',
            'view_page',
            'change_content_page',
            'change_price_list',
            'answer_to_questions',
            'change_promocode',
            'close_window',
            'free_period',
            'execute_order',
            'unlock_registration',
        ],
    ]) ?>

</div>
