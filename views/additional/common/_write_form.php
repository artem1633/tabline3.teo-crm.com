<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

?>
<br>
<?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Обратная связь</h3>
            <span class="pull-right">
                <?= Html::submitButton('Отправить сообщение', ['class' => 'btn btn-warning btn-xs']) ?>
            </span>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'sender_name')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'sender_email')->textInput() ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline' => false, //по умолчанию false
                            'height' => '180px',
                        ],]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>