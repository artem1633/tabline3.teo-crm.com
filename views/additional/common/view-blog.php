<?php
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

CrudAsset::register($this);

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'blog-pjax']) ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><?=$model->title?></h3>
					<span class="pull-right">
						<?php if($dostup) { ?>
							<a class="btn btn-warning btn-xs" role="modal-remote" href="<?=Url::toRoute(['/common/edit-blog', 'id' => $model->id])?>"><i class="glyphicon glyphicon-pencil" ></i></a>
	                        <a href="<?=Url::toRoute(['/common/delete-blog', 'id' => $model->id])?>" 
	                        	class="btn btn-danger btn-xs" 
	                        	title="Удалить" 
	                        	aria-label="Удалить" 
	                        	data-pjax="0" 
	                        	data-confirm="Вы уверены, что хотите удалить этот элемент?" 
	                        	data-method="post">
	                        	<span style="font-size:12px;" class="glyphicon glyphicon-trash"></span>
	                        </a>
	                    <?php } ?>
						<a class="btn btn-success btn-xs" data-pjax="0" href="<?=Url::toRoute(['/common/blog'])?>">Назад <i class="fa fa-arrow-right" ></i></a>
					</span>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<?=$model->text?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>