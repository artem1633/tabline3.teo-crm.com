<?php
use yii\helpers\Html;
?>

<h1 style="text-align: center; color:#337ab7; font-weight: bold;">Вопросы и ответы</h1>
<div class="row">
	<br>
	<?php 
	foreach ($faq as $value) {
	?>
		<div class="col-md-12">
			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title"><?=$value->question?></h3>
				</div>
				<div class="panel-body">
					<div style="font-weight: bold; font-size: 15px;">
						<?=$value->answer?>
					</div>
				</div>
			</div>
		</div>
	<?php }	?>
</div> 
