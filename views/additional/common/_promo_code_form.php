<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="common-form">

    <?php $form = ActiveForm::begin(); ?>    
        <div class="row">
            <div class="col-md-12">
                <label class="control-label" for="promocodes-code">Укажите промокод</label>
                <input type="text" id="promocodes-code" class="form-control" name="PromoCodes[promo-code]" maxlength="255" aria-required="true" aria-invalid="true" required>
            </div>
        </div>
        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>
    <?php ActiveForm::end(); ?>
    
</div>
