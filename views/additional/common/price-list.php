<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 


CrudAsset::register($this);

if($user->tariff->key == 'free') $tariff = 'Ваш тариф ' . $user->tariff->name;
else $tariff = 'Ваш ' . $user->tariff->name .' тариф оплачен до ' . Yii::$app->formatter->asDate($user->end_access, 'php:d.m.Y');
?>


<?php Pjax::begin(['enablePushState' => false, 'id' => 'tariff-list-pjax']) ?>

<div class="has-text-centered has-mb-8"> 
	<h3>Цены и тарифы</h3>  
	<h4 class="has-text-danger"><?=$tariff?></h4>   
</div>

<div class="row price-list" style="padding: 0px;" > 

	<div class="col-md-6 col-xs-12 col-sm-12 has-mb-4"> 
		<div style="text-align: center; margin-bottom: 20px; margin-top:20px;">
			<?php
				$data = json_decode($free->price_list, true);
				$first = '';
				foreach ($data as $key => $value) {
					if($key == 0) 
					{
						$first = $value["price"] . " ₽/ ".$value['time']." месяц";
					}
			?>
				<a class="btn btn-warning" onclick='document.getElementById("price_free").innerHTML = "<?=$value["price"] . " ₽/ ".$value['time']." месяц" ?>"; ' ><?=$value['time']?> месяц</a>
			<?php
				}
			?>		
		</div>
		<div class="price-column">
			<div style="background: #1f5688;color:#fff"> 
				<div class="has-p-2"> 
					<h3 class="has-text-centered"><?=$free->name?> тариф</h3> 
					<p class="has-text-centered" id="price_free" style="opacity:.6"><?=$first?></p> 
				</div> 
			</div>
			<div style="background: #348fe2;color:#fff"> 
				<div class="has-p-2"> Стандартные блоки: текст, ссылка, отступ </div>
				<div class="has-p-2"> Настройка размеров текста и аватара </div>
				<div class="has-p-2"> Готовые цветовые схемы </div>
				<div class="has-p-2"> Общая статистика </div>
				<div class="has-p-2"> Вставка кнопок соц. сетей </div> 
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<div class="has-p-2 has-text-grey-light is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only" style="opacity: .5"> &nbsp; </div>
				<!-- <div class="has-p-2 has-text-centered"> Бесплатно навсегда </div>  -->
			</div>  
		</div> 
	</div> 
	<div class="col-md-6 col-xs-12 col-sm-12 has-mb-4"> 
		<div style="text-align: center; margin-bottom: 20px; margin-top:20px;">
			<?php
				$data = json_decode($pro->price_list, true);
				$first = '';
				foreach ($data as $key => $value) {
					if($key == 0) 
					{
						$first = $value["price"] . " ₽/ ".$value['time']." месяц";
					}
			?>
				<a class="btn btn-warning" onclick=' $.post( "/common/set-session?key=<?=$pro->key?>&index=<?=$key?>", function( data ){});  document.getElementById("price").innerHTML = "<?=$value["price"] . " ₽/ ".$value['time']." месяц" ?>"; ' ><?=$value['time']?> месяц</a>
			<?php
				}
			?>	
		</div>
		<div class="price-column" data-tariff="pro"> 
			<div style="background: #993734;color:#fff"> 
				<div class="has-p-2"> 
					<h3 class="has-text-centered"><?=$pro->name?> тариф</h3> 
					<p class="has-text-centered" style="color:#fece6c"><span class="priceMonthly" id="price" ><?=$first?></span></p>
				</div>
			</div>
			<div style="background: #ff5b57;color:#fff"> 
				<div class="has-p-2"> Всё, что есть во Free </div> 
				<div class="has-p-2"> Статистика кликов по ссылкам </div> 
				<div class="has-p-2"> Своя цветовая схема под свой бренд (ручная настройка цветов темы)</div>
				<div class="has-p-2"> Написать в один клик </div> 
				<div class="has-p-2"> Написать в WhatsApp с шаблоном текста </div>
				<div class="has-p-2"> Вставка видео и карусели картинок </div>
				<div class="has-p-2"> Вставка HTML-кода </div>
				<div class="has-p-2"> Facebook pixel </div>
				<div class="has-p-2"> Создание дополнительных внутренних страниц </div>
				<div class="has-p-2"> Формы для приема заявок </div>
				<div class="has-p-2"> Уведомления в Телеграм </div>
				<div class="has-p-2"> Прием платежей (подключение платежных систем) </div>
				<div class="has-p-2"> Интеграция с онлайн-кассами </div>
				<div class="has-p-2"> CRM-система (вкладка Заявки) </div>
				<div class="has-p-2"> Подключение мессенджеров </div>
				<div class="has-p-2"> Блок “Меню” </div>
				<div class="has-p-2"> Блок “Карта” </div>
				<div class="has-p-2"> Блок “Вопрос/ответ” </div>
				<div class="has-p-2"> Блок “Уникальное торговое предложение” </div> 
				<div class="has-p-2"> Блок “Преимущества” </div>
			</div> 
			<div style="margin-top: 5px">  
				<a href="<?=Url::toRoute(['/common/change-tariff',])?>" role="modal-remote" class="btn btn-success" style="width: 100%; font-weight: bold; font-size: 18px;" >
					<i class="fa fa-credit-card"></i> Продлить <?=$pro->name?>
				</a>
			</div> 
		</div> 
	</div>  
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
