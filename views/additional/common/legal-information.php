<?php
use yii\helpers\Html;
?>

<div class="common-form">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Юридической информация</h3>
				</div>
				<div class="panel-body">
					<center>
						<div style="font-weight: bold; font-size: 20px;">							
							<?=$model->name?>
						</div>							
					</center>
					<div style="font-size:16px;">
						<?=$model->text?>
					</div>
				</div>
			</div>
		</div>
	</div>    
</div>
