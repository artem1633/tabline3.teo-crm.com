<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'pay_type_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'summa')->hiddenInput()->label(false) ?>

<br>
<center>
    <div class="row" style="width:90%; text-align: left;">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Контакты</h3>
                    <span class="pull-right">
                        <div class="form-group">
                            <?= Html::submitButton('Далее <i class="glyphicon glyphicon-forward"></i>' , ['class' => 'btn btn-xs btn-warning']) ?>
                        </div>
                    </span>                    
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <?= $form->field($model, 'address')->textarea(['rows' => 3]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</center>
<?php ActiveForm::end(); ?>