<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Products;
use kartik\touchspin\TouchSpin;

?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
<br>
<center>
    <div class="row" style="width:90%; text-align: left;">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Корзинка</h3>
                    <span class="pull-right">
                        <div class="form-group">
                            <?= Html::submitButton('Далее  <i class="glyphicon glyphicon-forward"></i>' , ['class' => 'btn btn-xs btn-warning']) ?>
                        </div>
                    </span>
                	
                </div>
                <div class="panel-body">                    
                    <?php
                    $k = -1;
                    foreach ($products as $product) 
                    {
                        $k++;
                        $good = Products::findOne($product);
                        $name = $good->name;
                        $price = $good->price * $counts[$k];
                    ?>
                        <div class="row" id="row<?=$k?>">
                            <hr>
                            <input type="hidden" class="form-control" id="products[]" name="products[]" value="<?=$product?>"/> 
                            <input type="hidden" class="form-control" id="product_price<?=($k+1)?>" name="product_price<?=($k+1)?>" value="<?=$good->price?>"/> 
                            <div class="col-md-2 col-xs-5 col-sm-6">
                                <div id="images-to-upload<?=$k?>">
                                    <img style="width:80px; height: 80px;" src="<?= 'http://' . $_SERVER['SERVER_NAME'] . '/images/no-images.jpg' ?>">
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-7 col-sm-6" style="margin-top:25px;color:#5ea1da;"> 
                                <label><?=$name?></label>
                            </div> 
                            <div class="col-md-3 col-xs-12 col-sm-4" style="margin-top:20px;">
                                <?= TouchSpin::widget([
                                        'name' => 'counts[]',
                                        'pluginEvents' => [
                                            'change' => "function(){
                                                
                                                var value = this.value;
                                                var id = $(this).attr('id') + '';
                                                var sln = id.length;
                                                var result = '';
                                                for(var i=0 ; i< sln; i++)
                                                {
                                                    var char = id.charAt(i);
                                                    if(char >= '0' && char <= '9') result += char;
                                                }

                                                var price = 'price' + result;
                                                document.getElementById(price).innerHTML = (value * $('#product_price'+result).val()) +  ' ₽';
                                            }",
                                        ],
                                        'value' => $counts[$k],
                                        'pluginOptions' => [
                                            'step' => 1,
                                            'min' => 1,
                                        ]
                                    ]);
                                ?>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4" style="margin-top:25px;">                                
                                <label id="price<?=($k+1)?>"><?=$price?> ₽</label>
                            </div>  
                            <div class="col-md-1 col-xs-6 col-sm-4" style="margin-top:20px;">
                                <button type="button" name="remove" id="<?=$k?>" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> 
                            </div> 
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</center>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

    /*$('#w').on('change', function() 
    { 
        var type = this.value;
        alert(type);
    });*/

});
JS
);
?>