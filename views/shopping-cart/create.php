<?php

use yii\helpers\Html;

$session = Yii::$app->session;
$first = '';
$second = '';
if($model->step == 2) { $first = 'active'; }
if($model->step == 3) { $second = 'active'; $first = 'active'; }
?>


<div class="shopping-cart-create">
    <?php 
    	if($session['list'] == null) {
    		echo $this->render('error_form', [
    			'user_id' => $user_id,
    		]);	
    	}else{
    ?>
			<div class="container">
			    <ul class="progressbar">
			        <li class="<?=$first?>">Корзинка</li>
			        <li class="<?=$second?>">Контакты</li>
			        <li>Оплата</li>
				</ul>
			</div>
    <?php
	    	if($model->step == 1){
		    	echo $this->render('cart_form', [
			        'model' => $model,
			        'products' => $products,
	            	'counts' => $counts,
			    ]);
		    }
		    if($model->step == 2){
		    	echo $this->render('shopping_form', [
			        'model' => $model,
			        'products' => $products,
	            	'counts' => $counts,
			    ]);
		    }
		    if($model->step == 3){
		    	echo $this->render('pay_form', [
			        'model' => $model,
			        'products' => $products,
	            	'counts' => $counts,
			    ]);
		    }
		}
    ?>
</div>
