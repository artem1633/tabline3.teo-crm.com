<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'name')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'email')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'phone')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'address')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'pay_type_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'summa')->hiddenInput()->label(false) ?>

<br>
<center>
    <div class="row" style="width:90%; text-align: left;">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Оплата</h3>
                    <span class="pull-right">
                        <div class="form-group">
                            <?= Html::submitButton('Далее  <i class="glyphicon glyphicon-forward"></i>' , ['class' => 'btn btn-xs btn-warning']) ?>
                        </div>
                    </span>                    
                </div>
                <div class="panel-body" style="height:400px;">
                    <h1 style="text-align: center; margin-top: 120px;">
                        В работе
                    </h1>
                </div>
            </div>
        </div>
    </div>
</center>
<?php ActiveForm::end(); ?>