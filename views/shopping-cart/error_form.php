<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Products;
use kartik\touchspin\TouchSpin;

?>
<center>
    <div class="row" style="width:90%; text-align: left;">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Корзинка</h3>
                    <span class="pull-right">
                        <div class="form-group">
                            <?= Html::a('Магазин', ['/products/shop?user_id='.$user_id,], ['class' => 'btn btn-xs btn-warning',  'title' => 'Магазин']); ?>
                        </div>
                    </span>                	
                </div>
                <div class="panel-body" style="height:400px;">
                    <h1 style="text-align: center; margin-top: 80px;">
                        Корзинка пусто
                        <br>
                        <br>
                        <div class="text-danger">
                            <i class="fa fa-shopping-basket"></i>
                        </div>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</center>