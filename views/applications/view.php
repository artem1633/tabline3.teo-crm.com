<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(); ?>
<div class="table-responsive">
    <table class="table table-bordered">
        <tbody>
        <tr>
            <td><b><?=$model->getAttributeLabel('date')?></b></td>
            <td><?= date('H:i:s d.m.Y', strtotime($model->date) )?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('name')?></b></td>
            <td><?=$model->name?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('page_id')?></b></td>
            <td><?=Html::encode($model->page->name)?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('place')?></b></td>
            <td><?= $model->place?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('account_number')?></b></td>
            <td><?= '№ ' . $model->account_number?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('status')?></b></td>
            <td>
                <?= $form->field($model, 'status')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getStatusList(),
                    'options' => [
                        'onchange'=>'
                            $.post( "/applications/set-status?status='.'"+$(this).val()+"&id="+'.$model->id.', function( data ){
                            });' 
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(''); 
                ?>
            </td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('pay')?></b></td>
            <td><?= $model->getPayDescription()?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('pay_type')?></b></td>
            <td>
                <?php if($model->pay == 'not_payed') { ?>
                    <?= $form->field($model, 'pay_type')->label()->widget(\kartik\select2\Select2::classname(), [
                        'data' => $model->getPayTypeList(),
                        'options' => [
                            'placeholder' => 'Выберите тип оплаты',
                            'onchange'=>'
                                $.post( "/applications/set-pay-type?status='.'"+$(this).val()+"&id="+'.$model->id.', function( data ){
                                });' 
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(''); 
                    ?>
                <?php } else { ?>
                    <?= $model->getPayTypeDescription()?>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('budget')?></b></td>
            <td><?=Html::encode($model->budget)?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('purpose_of_payment')?></b></td>
            <td><?= $model->purpose_of_payment?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('pay_date')?></b></td>
            <td><?= $model->pay_date != null ? date('H:i:s d.m.Y', strtotime($model->pay_date) ) : '' ?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('utm')?></b></td>
            <td><?=$model->utm?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('contacts')?></b></td>
            <td><?=$model->contacts?></td>
        </tr>
        <tr>
            <td><b><?=$model->getAttributeLabel('fields')?></b></td>
            <td><?=$model->fields?></td>
        </tr>                                   
        </tbody>
    </table>
</div>
<?php ActiveForm::end(); ?>