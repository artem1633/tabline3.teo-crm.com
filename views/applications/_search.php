<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<div class="client-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-4"> 
                <?= $form->field($model, 'search_name')->textInput([ 'value' => (isset($post['ApplicationsSearch']['search_name']) ? $post['ApplicationsSearch']['search_name'] : '' ) ]) ?>
            </div>

            <div class="col-md-3"> 
                <?= $form->field($model, 'search_page')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getPageList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'value' => (isset($post['ApplicationsSearch']['search_page']) ? $post['ApplicationsSearch']['search_page'] : '' ),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

            <div class="col-md-3"> 
                <?= $form->field($model, 'search_status')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getStatusList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'value' => (isset($post['ApplicationsSearch']['search_status']) ? $post['ApplicationsSearch']['search_status'] : '') ,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-1">
                <?= Html::submitButton('Поиск' , ['style'=>'margin-top:25px;', 'class' =>  'btn btn-primary']) ?>
            </div>
            <div class="col-md-1">
                <?= Html::a('<i class="fa fa-bars fa-rotate-90"></i>', ['/applications/sorting',], [ 'title' => 'Настроить колонки таблицы', 'style'=>'margin-top:25px;', 'class' => 'btn btn-default', 'data-pjax'=>0, ]); ?>
            </div>
         </div>   
    <?php ActiveForm::end(); ?>
    
</div>
