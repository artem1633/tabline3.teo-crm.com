<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<div class="client-form" style="padding: 20px;">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-4"> 
                <?= $form->field($model, 'search_name')->textInput([ 'value' => (isset($post['Contacts']['search_name'] ) ? $post['Contacts']['search_name'] : '') ]) ?>
            </div>

            <div class="col-md-4"> 
                <?= $form->field($model, 'search_email')->textInput([ 'value' => (isset($post['Contacts']['search_email']) ? $post['Contacts']['search_email']: '')  ]) ?>
            </div>

            <div class="col-md-3"> 
                <?= $form->field($model, 'search_phone')->textInput([ 'value' => (isset($post['Contacts']['search_phone']) ? $post['Contacts']['search_phone']: '')  ]) ?>
            </div>
            <div class="col-md-1">
                <div class="form-group" >
                    <?= Html::submitButton('Поиск' , ['style'=>'margin-top:25px;', 'class' =>  'btn btn-primary']) ?>
                </div>
            </div>
         </div>   
    <?php ActiveForm::end(); ?>
    
</div>
