<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\Pjax;
use app\models\Applications;

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>

        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">Заявки</span>
                            <span class="hidden-xs">Заявки</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">Контакты</span>
                            <span class="hidden-xs">Контакты</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="default-tab-1">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'active-pjax']) ?>
                                <div class="col-md-12 col-sm-12">  
                                    <?= $this->render('_search', ['model' => $searchModel, 'post' => $post]); ?>
                                    <br>
                                    <?=GridView::widget([
                                        'id'=>'crud-datatable',
                                        'dataProvider' => $dataProvider,
                                        //'filterModel' => $searchModel,
                                        'pjax'=>true,
                                        'columns' => Applications::getColumns(),
                                        'toolbar'=> [
                                            [
                                                'content'=>'{export}'
                                            ],
                                        ],       
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,          
                                        'panel' => [
                                            'type' => 'primary', 
                                            'heading' => 'Список заявок',
                                            'before'=>'',
                                            'after'=>'',
                                        ]
                                    ])?>
                                </div>
                            <?php Pjax::end() ?>                        
                        </div>
                    </div>
                    <div class="tab-pane fade" id="default-tab-2">
                        <div class="row">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'active-pjax']) ?>
                                <div class="col-md-12 col-sm-12">  
                                    <?= $this->render('_contact_search', ['model' => $contact, 'post' => $post]); ?>
                                    <br>
                                    <?=GridView::widget([
                                        'id'=>'crud-datatable',
                                        'dataProvider' => $contactProvider,
                                        //'filterModel' => $searchModel,
                                        'pjax'=>true,
                                        'columns' => require(__DIR__.'/_contact_columns.php'),
                                        'toolbar'=> [
                                            [
                                                'content'=>'{export}'
                                            ],
                                        ],       
                                        'striped' => true,
                                        'condensed' => true,
                                        'responsive' => true,          
                                        'panel' => [
                                            'type' => 'primary', 
                                            'heading' => 'Список контактов',
                                            'before'=>'',
                                            'after'=>'',
                                        ]
                                    ])?>
                                </div>
                            <?php Pjax::end() ?>                                     
                        </div>
                    </div>

                </div>

            </div>
        </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
