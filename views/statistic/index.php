<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Статистика';
?>
<h1>Статистика</h1>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>&nbsp;&nbsp;&nbsp; &nbsp;День&nbsp; &nbsp;&nbsp;&nbsp;</b></span>
                        <span class="hidden-xs"><b>&nbsp;&nbsp;&nbsp; &nbsp;День&nbsp; &nbsp;&nbsp;&nbsp;</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>&nbsp;&nbsp;&nbsp; &nbsp;Неделя&nbsp;&nbsp;&nbsp; &nbsp;</b></span>
                        <span class="hidden-xs"><b>&nbsp;&nbsp;&nbsp; &nbsp;Неделя&nbsp;&nbsp;&nbsp; &nbsp;</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>&nbsp;&nbsp;&nbsp; &nbsp;Месяц&nbsp;&nbsp;&nbsp; &nbsp;</b></span>
                        <span class="hidden-xs"><b>&nbsp;&nbsp;&nbsp; &nbsp;Месяц&nbsp;&nbsp;&nbsp; &nbsp;</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="default-tab-1">
                    <br>
                    <center><h3><?=date('d.m.Y')?></h3></center>
                    <div style="padding: 30px" class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th><b>Заголовок</b></th>
                                <th><b>Клики</b></th>
                            </tr>
                            <?php
                            foreach ($dayStatistics as $value) {
                            ?>
                                <tr>
                                    <td style="color:#337ab7;"><?=$value['title']?></td>
                                    <td style="color:#337ab7;"><?=$value['count']?></td>
                                </tr>
                            <?php
                                }
                             ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-2">
                    <br>
                    <center><h3><?=date('d.m.Y', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) ). ' - ' .date('d.m.Y')?></h3></center>
                    <div style="padding: 30px" class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th><b>Заголовок</b></th>
                                <th><b>Клики</b></th>
                            </tr>
                            <?php
                            foreach ($weekStatistics as $value) {
                            ?>
                                <tr>
                                    <td style="color:#337ab7;"><?=$value['title']?></td>
                                    <td style="color:#337ab7;"><?=$value['count']?></td>
                                </tr>
                            <?php
                                }
                             ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="default-tab-3">
                    <br>
                    <center><h3><?= date('01.m.Y') . ' - ' .date('d.m.Y') ?></h3></center>
                    <div style="padding: 30px" class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th><b>Заголовок</b></th>
                                <th><b>Клики</b></th>
                            </tr>
                            <?php
                            foreach ($monthStatistics as $value) {
                            ?>
                                <tr>
                                    <td style="color:#337ab7;"><?=$value['title']?></td>
                                    <td style="color:#337ab7;"><?=$value['count']?></td>
                                </tr>
                            <?php
                                }
                             ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>