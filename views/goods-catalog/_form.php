<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Products;

?>

<div class="goods-catalog-form">

    <?php $form = ActiveForm::begin(); ?>
		<div style="display: none;">
			<?= $form->field($model, 'hide')->checkBox([]) ?>			
		</div>
		<section>
		    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</section>
		<br>
		<section>
			<?= $form->field($model, 'products')->label()->widget(\kartik\select2\Select2::classname(), [
	            'data' => $model->getProductsList(),
	            'options' => [
	                'placeholder' => 'Выберите',
	            ],
	            'pluginOptions' => [
	                'allowClear' => true,
	                'multiple' => true,
	            ],
	        ]); ?>
		</section>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
