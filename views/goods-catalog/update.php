<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GoodsCatalog */
?>
<div class="goods-catalog-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
