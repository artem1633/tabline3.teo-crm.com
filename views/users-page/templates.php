<?php
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

CrudAsset::register($this);
?>


<?php Pjax::begin(['enablePushState' => false, 'id' => 'template-pjax']) ?>
<div class="templates">

	<?php if(Yii::$app->user->identity->type == 0){ ?>
		<center>
			<?= Html::a('Создать новый шаблон страниц', ['add-template'], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) ?>
		</center>
	<?php } ?>

	<div class="row" style="margin-top: 50px;">
		<?php
			foreach ($templates as $template) {

				$update = '';
				if(Yii::$app->user->identity->type == 0) $update = '<a data-pjax="0" href="'.Url::toRoute(['/users-page/page', 'id' => $template->id]).'"><i class="glyphicon glyphicon-pencil" ></i></a> <a role="modal-remote" href="'.Url::toRoute(['/users-page/add-template', 'id' => $template->id]).'"><i class="glyphicon glyphicon-eye-open" ></i></a> ';

				if (!file_exists('images/templates/'.$template->image) || $template->image == null) {
				    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-images.jpg';
				} else {
				    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/templates/'.$template->image;
				}

		?>
			<div class="col-md-6 col-xs-12 col-sm-6 has-mb-4">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title"><?=$update . $template->name?></h3>
						<span class="btn-group pull-right">
				            <button type="button" class="btn btn-warning btn-xs arxiv">Добавить к себе как ...</button>
				            <button type="button" class="btn btn-warning btn-xs dropdown-toggle dropdown1" data-toggle="dropdown" aria-expanded="false">
				                <span style="margin-top: 1px;" class="caret"></span>
				            </button>
				            <ul class="dropdown-menu" role="menu">
				                <li><?= Html::a('Профайл', ['/users-page/import-template', 'type' => 'profile', 'template_id' => $template->id], ['role'=>'modal-remote','class'=>'top','style'=>'width :200px;text-align: left;'])?></li>
				                <li><?= Html::a('Главной страницы', ['/users-page/import-template', 'type' => 'main', 'template_id' => $template->id], ['role'=>'modal-remote','class'=>'top','style'=>'width :200px;text-align: left;'])?></li>
				            </ul>
				        </span>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<center>
						        <?=Html::img($path, [
						            'style' => 'width:350px; height:250px;',
						            //'class' => 'img-circle',
						        ])?>
							</center>
							<br>
				            <table class="table table-bordered">    
				                <tr>
				                    <td style="font-weight: bold; font-size: 16px;">Ссылка</td>
				                    <td>
				                    	<?= Html::a('<span class="is-hidden-mobile">http://'. $_SERVER['SERVER_NAME'].'/'.$template->link_name .'	</span>', ['/'.$template->link_name], ['data-pjax' => '0', 'target'=> "_blank"]) ?>
				                    </td>
				                </tr>				               
				                <tr>
				                    <td style="font-weight: bold; font-size: 16px;">Описание</td>
				                   	<td><?=$template->description?></td>
				                </tr>				                
				            </table>
				        </div>
					</div>
				</div>
			</div>
		<?php
			}
		?>
	</div>    
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>