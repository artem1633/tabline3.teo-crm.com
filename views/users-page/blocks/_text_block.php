<?php

?>

<div class="text-block" id="text_block">
	<div style="text-align:center; font-weight:bold;">
		<?= $textBlock->title_text ?>
	</div>
	<div style="text-align: <?= $textBlock->getAlignmentSizeDescription() ?>; font-size: <?= $textBlock->getTextSizeDescription() ?>; ">
  		<?= $textBlock->text ?>
 	</div>
</div>