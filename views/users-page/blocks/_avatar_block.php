<?php

use yii\helpers\Html;

$size = $avatarBlock->getSizeDescription();
if (!file_exists('avatars/'.Yii::$app->user->identity->avatar) || Yii::$app->user->identity->avatar == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->avatar;
}

if($avatarBlock->hide_avatar) $user_name='';
else $user_name = Yii::$app->user->identity->fio;

?>

<div class="avatar-block" id="avatar_block" style="width: <?=$size?>px; margin: 0 auto;">
  <img src="<?=$path?>" alt="" style="width: <?=$size?>px; height: <?=$size?>px; border-radius: 50%; margin: 0 auto;">
  <p style="margin: 0; font-size: 12px;text-align: center;"><?=$user_name?></p>
</div>