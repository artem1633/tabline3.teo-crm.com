<?php

use yii\helpers\Html;

$class = '';
if( $messengerBlock->display == 'with_icons' ) $class = '1';
if( $messengerBlock->display == 'without_design' )  $class = 'back-green';
if( $messengerBlock->display == 'with_design' ) $class = '';
if( $messengerBlock->display == 'with_icons_design' ) $class = 'second';
if( $messengerBlock->display == 'with_link_design' ) $class = 'second round';
?>


<?php if($class == '1') { ?>
  <div class="new-social">
    <?php if($messengerBlock->whatsapp){ ?>
      <a href="#"> <?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/whatsapp.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->telegram){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/telegram.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->facebook){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/Facebook.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->vk){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/vk.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->viber){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/viber.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->skype){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/skype.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->phone){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/phone.png', [])?></a>
    <?php } ?>

    <?php if($messengerBlock->email){ ?>
      <a href="#"><?=Html::img("http://".$_SERVER['SERVER_NAME'].'/images/icons/mail.png', [])?></a>
    <?php } ?>
  </div>
<?php } else {?>

  <div class="add-social-blocks <?=$class?> ">
    <?php if($messengerBlock->whatsapp){ ?>
      <a href="<?=$messengerBlock->whatsapp_phone?>" target="_blank" class="block-social">
          <i class="fa fa-whatsapp"></i>
          <p><?=$messengerBlock->whatsapp_title?></p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->telegram){ ?>
      <a href="https://t.me/<?=$messengerBlock->telegram_user?>" target="_blank" class="block-social">
          <i class="fa fa-telegram"></i>
          <p><?=$messengerBlock->telegram_title?></p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->facebook){ ?>
      <a href="https://www.facebook.com/messages/t/<?=$messengerBlock->facebook_user?>" target="_blank" class="block-social">
          <i class="fa fa-facebook"></i>
          <p><?=$messengerBlock->facebook_title?></p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->vk){ ?>
      <a href="https://vk.com/im?media=&sel=<?=$messengerBlock->vk_user?>" target="_blank" class="block-social">
          <i class="fa fa-vk"></i>
          <p><?=$messengerBlock->vk_title?></p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->viber){ ?>
      <a href="<?=$messengerBlock->viber_phone?>" target="_blank" class="block-social">
          <i class="fa fa-mobile-phone"></i>
          <p><?=$messengerBlock->viber_title?></p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->skype){ ?>
      <a href="<?=$messengerBlock->skype_user?>" target="_blank" class="block-social">
          <i class="fa fa-skype"></i>
          <p><?=$messengerBlock->skype_title?></p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->phone){ ?>
      <a href="<?=$messengerBlock->phone_number?>" target="_blank" class="block-social">
          <i class="fa fa-phone"></i>
          <p>Написать в телефон</p>
      </a>
    <?php } ?>

    <?php if($messengerBlock->email){ ?>
      <a href="<?=$messengerBlock->email_title?>" target="_blank" class="block-social">
          <i class="fa fa-envelope-o"></i>
          <p>Написать в email</p>
      </a>
    <?php } ?>
  </div>
<?php } ?>