<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

\johnitvn\ajaxcrud\CrudAsset::register($this);


if($model->show_zoom && $model->show_select) $parameters = [ Map::CONTROL_ZOOM, Map::CONTROL_TYPE_SELECTOR, ];
if(!$model->show_zoom && $model->show_select) $parameters = [ Map::CONTROL_TYPE_SELECTOR, ];
if($model->show_zoom && !$model->show_select) $parameters = [ Map::CONTROL_ZOOM, ];
if(!$model->show_zoom && !$model->show_select) $parameters = [ ];
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Карта</h3>
				</div>
				<div class="panel-body">
                    <?= YandexMaps::widget([
                        'htmlOptions' => [
                            'style' => 'height: 400px;',
                        ],
                        'map' => new Map('yandex_map', [
                            'center' => [$model->center_x, $model->center_y],//'Tashkent',
                            'zoom' => 10,
                            'controls' => $parameters,
                            'behaviors' => $model->lock_map ? array('default', 'scrollZoom') : array(),
                            'type' => "yandex#map",
                    	    [
                    	        //	'minZoom' => 9,
                    	        //	'maxZoom' => 11,
                    	        'controls' => [
                    	          "new ymaps.control.SmallZoomControl()",
                    	          "new ymaps.control.TypeSelector(['yandex#map', 'yandex#satellite'])",
                    	        ],
                    	    ],
                        ],
                        [
                            'objects' => $marks,
                        ])
                    ]) ?>
				</div>
			</div>
		</div>
	</div>
