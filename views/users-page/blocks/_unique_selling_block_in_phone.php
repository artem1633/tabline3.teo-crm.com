<?php
use yii\helpers\Html;

if (!file_exists('images/unique-selling-block/'.$uniqueSellingBlock->image) || $uniqueSellingBlock->image == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-images.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/unique-selling-block/'.$uniqueSellingBlock->image;
}
?>

<div class="unical" style="clear:both; background-image: url(<?=$path?>);" >
   <h3><?=$uniqueSellingBlock->title?></h3>
   <p><?=$uniqueSellingBlock->text?></p>
   <a href="<?= 'http://' .  $uniqueSellingBlock->link?>" target="_blank"><?=$uniqueSellingBlock->button_text?></a>
</div>