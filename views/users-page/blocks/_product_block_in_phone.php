<?php
use yii\helpers\Html;
use app\models\blocks\ProductBlock;
use app\models\additional\Common;

$currency = Common::find()->where(['user_id' => $user_id])->one()->getCurrencyFont();
?>
<?php if($productBlock[0]->view == 'slider') { ?>
<div class="product-block" id="product_block">
    <div id="myCarouselProduct" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php 
            $i = -1;
            foreach ($productBlock as $value) {
                $i++;
                $data = json_decode($value->product->photos, true);
                $path = $data[0]['path'];
            ?>
            <li data-target="#myCarouselProduct" data-slide-to="<?=$i?>" <?= $i == 0 ? 'class="active"' : '' ?> ></li>
            <?php } ?>
        </ol>
    
        <div class="carousel-inner">
            <?php 
            $i = 0;
            foreach ($productBlock as $value) {
                $i++;
                $data = json_decode($value->product->photos, true);
                $path = "http://". $_SERVER['SERVER_NAME'] . '/' . $data[0]['path'];
                if($value->product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $value->product->last_price . ' ' . $currency . '</s> ';
                else $last = '';
            ?>
                <div class="item <?= $i == 1 ? 'active' : '' ?> " >
                    <br>
                    <center><img style="height: 130px;" src="<?=$path?>"></center>
                    <div style=" margin-top: 10px; font-size: 20px;">
                        <?=$value->product->name?>
                    </div>
                    <center><div style="color: black; margin-top: 10px; font-size: 14px; width: 300px;">
                        <?=$value->product->description?>
                    </div></center>
                    <center><div style="margin-top: 10px; font-size: 16px; font-weight: bold;">
                        <?= $last . $value->product->price . ' ' . $currency;?>
                        <br>
                        <?= Html::a('Купить', ['/#'], [ 'style' => 'margin-top: 10px;', 'class' => 'btn btn-warning']); ?>
                    </div></center>
                </div>

            <?php } ?>
        </div>
    
        <a class="left carousel-control" href="#myCarouselProduct" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarouselProduct" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<?php } ?>
<?php if($productBlock[0]->view == 'table') { ?>
    <div class="table-responsive">
        <table class="table table-bordered">
            <?php 
            $i = 0;
            foreach ($productBlock as $value) {
                $i++;
                $data = json_decode($value->product->photos, true);
                $path = $data[0]['path'];
                if($value->product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $value->product->last_price . ' ' . $currency . '</s> ';
                else $last = '';
            ?>
                <tr>
                    <td>                        
                        <div class="advantage-block">
                            <div id="myCarouselPro" class="carousel slide" data-ride="carousel">                        
                                <div class="carousel-inner">
                                    <?php 
                                    $i = 0;
                                    foreach ($data as $image) {
                                        $i++;
                                        $path = "http://". $_SERVER['SERVER_NAME'] . '/' . $image['path'];
                                    ?>
                                        <div class="item <?= $i == 1 ? 'active' : '' ?> " >
                                            <center><img style="height: 160px; width: 180px;" src="<?=$path?>"></center>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div style=" font-size: 16px; font-weight: bold;">
                            <?= $last . $value->product->price . ' ' . $currency . '&nbsp;&nbsp;';?>
                            <?= Html::a('Купить', ['/#'], [ 'class' => 'btn btn-warning btn-xs']); ?>
                        </div>
                    </td> 
                   <td>
                        <div style=" margin-top: 10px; font-size: 16px;font-weight: bold;">
                            <?=$value->product->name?>
                        </div>
                        <div style="color: black; margin-top: 10px; font-size: 14px;">
                            <?=$value->product->description?>
                        </div>
                   </td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php } ?>
<?php if($productBlock[0]->view == 'list') { ?>
    <select class="form-control" id="products" name="products[]"><?= ProductBlock::ProductsList($productBlock)?></select>
    <br>
    <div id="goods">        
    </div>
<?php } ?>

<?php 
$this->registerJs(<<<JS

$('#products').on('change', function() 
    {  
        var product = this.value;

        $.post( "/products/get-product-cart?id="+product, function( data ){
            $( "#goods" ).html( data);
        });

    }
    );
JS
);
?>