<?php
use yii\helpers\Html;
?>

<div class="question-block">
	<?php 
		foreach ($questionBlock as $question) {
	?>
	<div class="equations">
	  <b><?=$question->question_title?></b>
	  <p class="answers"><br><?=$question->answer?></p>
	</div>
	<?php }
	?>
</div>