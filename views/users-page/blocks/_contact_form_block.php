<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
                
?>

<div class="clients-field-form" id="contact_form_block">
	<div style="text-align:center; font-weight:bold;">
        <?= $model->title_text ?>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
	    <?php if($model->name == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->name_title ?></h4></label>
	            <?= $form->field($model, 'name_description')->textInput(['value' => '', 'maxlength' => true])->label($model->name_description) ?>            
	        </div>
	    <?php } ?>
	</div>
	<div class="row">
	    <?php if($model->telephone == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->telephone_title ?></h4></label>
	            <?= $form->field($model, 'telephone_description')->textInput(['value' => '', 'maxlength' => true])->label($model->telephone_description) ?>            
	        </div>
	    <?php } ?>
	</div>
	<div class="row">
	    <?php if($model->email == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->email_title ?></h4></label>
	            <?= $form->field($model, 'email_description')->textInput(['value' => '', 'maxlength' => true])->label($model->email_description) ?>            
	        </div>
	    <?php } ?>
	</div>
	<div class="row">
	    <?php if($model->line == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->line_title ?></h4></label>
	            <?= $form->field($model, 'line_description')->textInput(['value' => '', 'maxlength' => true])->label($model->line_description) ?>            
	        </div>
	    <?php } ?>
	</div> 
	<div class="row">
	    <?php if($model->number == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->number_title ?></h4></label>
	            <?= $form->field($model, 'number_description')->textInput(['value' => '', 'type' => 'number'])->label($model->number_description) ?>            
	        </div>
	    <?php } ?> 
	</div>
	<div class="row">
	    <?php if($model->list == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->list_title ?></h4></label>
	    		<?= $form->field($model, 'list_datas')->dropDownList($model->getListDatas(), ['prompt' => 'Выберите'])->label($model->list_description) ?>
	        </div>
	    <?php } ?> 
	</div>
	<div class="row">
	    <?php if($model->check == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->check_title ?></h4></label>
	            <?= $form->field($model, 'check_description')->checkbox([])->label($model->check_description) ?>            
	        </div>
	    <?php } ?> 
	</div>
	<div class="row">
	    <?php if($model->select == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->select_title ?></h4></label>
	            <?= $form->field($model, 'select_description')->checkboxList($model->getSelectList(), [ 'separator' => '<br>'])->label($model->select_description) ?>            
	        </div>
	    <?php } ?> 
	</div>
	<div class="row">
	    <?php if($model->text == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->text_title ?></h4></label>
	            <?= $form->field($model, 'text_description')->textArea(['value' => '', 'rows' => 3])->label($model->text_description) ?>
	        </div>
	    <?php } ?> 
	</div>
    <div class="row">
	    <?php if($model->country == 1) { ?>
	        <div class="col-md-4" style="float:none; margin:0 auto;">
	        	<label><h4><?= $model->country_title ?></h4></label>
	    		<?= $form->field($model, 'country_description')->dropDownList($model->getListCountries(), ['prompt' => 'Выберите'])->label('') ?>
	        </div>
	    <?php } ?> 
	</div> 
	<div style="display: none;" >
        <?= $form->field($model, 'id')->textInput([]) ?>
    </div>

	 <div class="row">
		<div class="col-md-4" style="float:none; margin:0 auto;">
		    <?= Html::submitButton($model->button_text, ['class' => 'btn', 'target' => '_blank', 'style' => 'width:100%; background:'.$model->button_background.'; color:'.$model->button_color.';' ]) ?>
	    </div>
	</div>

    <?php ActiveForm::end(); ?>
    
</div>
