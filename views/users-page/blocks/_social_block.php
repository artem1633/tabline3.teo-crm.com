<?php

$class = '';
if( $socialBlock->display == 'with_text' ) $class = '';
if( $socialBlock->display == 'without_text' ) $class = 'second';
if( $socialBlock->display == 'without_design' ) $class = 'back-green';
if( $socialBlock->display == 'with_design' ) $class = 'second round';
?>
<div class="add-social-blocks <?=$class?> " id="social_block" >
    <div style="text-align:center; font-weight:bold;">
        <?= $socialBlock->title_text ?>
    </div>
	<?php if($socialBlock->facebook){ ?>
		<a href="<?=$socialBlock->facebook_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-facebook"></i>
		    <p><?=$socialBlock->facebook_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->instagram){ ?>
		<a href="<?=$socialBlock->instagram_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-instagram"></i>
		    <p><?=$socialBlock->instagram_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->vk){ ?>
		<a href="<?=$socialBlock->vk_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-vk"></i>
		    <p><?=$socialBlock->vk_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->odno){ ?>
		<a href="<?=$socialBlock->odno_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-odnoklassniki"></i>
		    <p><?=$socialBlock->odno_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->twitter){ ?>
		<a href="<?=$socialBlock->twitter_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-twitter"></i>
		    <p><?=$socialBlock->twitter_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->youtube){ ?>
		<a href="<?=$socialBlock->youtube_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-youtube"></i>
		    <p><?=$socialBlock->youtube_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->pinterest){ ?>
		<a href="<?=$socialBlock->pinterest_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-pinterest"></i>
		    <p><?=$socialBlock->pinterest_title?></p>
		</a>
	<?php } ?>

	<?php if($socialBlock->snapchat){ ?>
		<a href="<?=$socialBlock->snapchat_link?>" onclick="$.post('/users-page/set-click?table_name=social_block&id=<?=$socialBlock->id?>');" target="_blank" class="block-social">
		    <i class="fa fa-snapchat"></i>
		    <p><?=$socialBlock->snapchat_title?></p>
		</a>
	<?php } ?>
</div>