<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsersPage */
?>
<div class="users-page-view">
 
	<div style="padding: 1px;">
		<?= Html::a('Главная страница', ['set-main', 'user_id' => $user_id, 'page_id' => $actual->id,], ['role'=>'modal-remote','title'=> 'Сделать страницу как активное','class'=>'btn ', 'style' => 'width:100%; background-color: #d9edf7; ']) ?>
	</div>
	<div style="padding: 1px;">
		<?= Html::a('<i class="fa fa-plus"></i> Создать новую страницу', ['add', 'user_id' => $user_id, 'active' => 0, 'is_main' => 0 ], ['role'=>'modal-remote','title'=> 'Создать новую страницу','class'=>'btn ', 'style' => 'width:100%; background-color: #d9edf7; ']) ?>
	</div>
	<div style="margin-top: 5px;">
		<?php 
			foreach ($pages as $page) {
		?>
		<div style="padding: 1px;">
			<?= 
				Html::a($page->name, ['set-main', 'user_id' => $user_id, 'page_id' => $page->id,], ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn ', 'style' => 'width:92%; background-color: #d9edf7; ']) . ' ' . 
				Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/users-page/update', 'id' => $page->id], ['role'=>'modal-remote','title'=> 'Изменить страницу',]) . ' ' . 
				Html::a('<span class="glyphicon glyphicon-trash"></span>', [ '/users-page/delete', 'id' => $page->id ], 
				[
					'role'=>'modal-remote','title'=>'Удалить', 
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'style' => 'cursor:pointer',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы действительно хотите удалить эту страницу?'                    
                ])
			?>
		</div>
		<?php
			}
		?> 
	</div>

</div>
