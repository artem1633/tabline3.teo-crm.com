<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (!file_exists('images/templates/'.$model->image) || $model->image == null) {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-images.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/templates/'.$model->image;
}

?>

<div class="users-page-form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
	
		<div style="padding:20px;">
			<div class="row">			
			    <div class="col-md-12">
			        <center>
			        <?=Html::img($path, [
			            'style' => 'width:350px; height:250px;',
			            //'class' => 'img-circle',
			        ])?>
			        </center>
			    </div>

				<div class="col-md-6">
			    	<?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Шаблоны страниц' ]) ?>
			    </div>

				<div class="col-md-6">
				    <?= $form->field($model, 'link_name')->textInput(['maxlength' => true, ]) ?>
				</div>

				<div class="col-md-12">
			    	<?= $form->field($model, 'description')->textArea(['rows' => 3]) ?>
			    </div>

				<div class="col-md-12">
			    	<?= $form->field($model, 'file')->fileInput(); ?>
				</div>
			  
				<?php if (!Yii::$app->request->isAjax){ ?>
				  	<div class="form-group">
				        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				    </div>
				<?php } ?>
			</div>
		</div>

    <?php ActiveForm::end(); ?>
    
</div>
