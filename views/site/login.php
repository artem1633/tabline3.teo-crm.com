<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ButtonDropdown;

$this->title = 'Авторизация';
?>
<div class="site-login">
    <center>
        <div class="row">
            <div class="col-md-12">
                <h1><?= Html::encode($this->title) ?> </h1>
            </div>
        </div>
    </center>

    <div class="row">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            //'layout' => 'horizontal',
        ]); ?>
        <div class="row">
            <div class="col-md-6" style="margin-left: 25%;">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>            
            </div>            
        </div>
        <div class="row">
            <div class="col-md-6" style="margin-left: 25%;">
                <?= $form->field($model, 'password')->passwordInput() ?>        
            </div>            
        </div>
        
        <div class="row">
            <div class="col-xs-3" style="margin-left: 25%;">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="form-group">
                <div class="col-xs-6" style="margin-left: 25%;">
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'style' => 'width:100%', 'name' => 'login-button']) ?>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
