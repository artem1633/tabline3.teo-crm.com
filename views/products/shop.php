<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\additional\Common;

$currency = Common::find()->where(['user_id' => $user->id])->one()->getCurrencyFont();

$size = '150';
if (!file_exists('avatars/' . $user->avatar) || $user->avatar == '')  $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
else $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/' . $user->avatar;
$session = Yii::$app->session;
$count = 0;
foreach ($session['list'] as $key => $value) 
{
    $count += $value['count'];
}

?>

<div class="avatar-block" id="avatar_block" style="width: <?=$size?>px; margin: 0 auto;">
  	<img src="<?=$path?>" alt="" style="width: <?=$size?>px; height: <?=$size?>px; border-radius: 50%; margin: 0 auto;">
  	<p style="margin: 0; font-size: 12px;text-align: center;"><?=$user->fio?></p>
</div>

<div id="listProducts">  
	<div class="row row-small" style="padding: 50px;"> 
	<?php 
		foreach ($products as $product) 
		{
			$data = json_decode($product->photos, true);
			$path = "http://". $_SERVER['SERVER_NAME'] . '/';
			$q=false;
	        foreach ($data as $value) 
	        {
	            $path .= $value['path'];
	            $q=true;
	            break;
	        }
	        if(!$q) $path .= 'images/no-images.jpg';
	?>
		<div class="col-xs-12 col-sm-6 col-md-4" style="padding-top: 20px;">
			<div class="table-responsive">
	        	<table class="table table-bordered">
	                <tr>
	                    <td colspan="3">                        
	                        <div class="advantage-block">
	                            <div id="myCarouselPro" class="carousel slide" data-ride="carousel">                        
	                                <div class="carousel-inner">
	                                    <?php 
	                                    $i = 0;
						                if($product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $product->last_price . ' ' . $currency . '</s><br>';
						                else $last = '';
						                if($product->catalog_id != null) $catalog = '<span style="color:#337ab7!important;">' . $product->catalog->name . ' / </span> <br>';
						                else $catalog = '';
	                					$data = json_decode($product->photos, true);
	                                    foreach ($data as $image) {
	                                        $i++;
	                                        $path = "http://". $_SERVER['SERVER_NAME'] . '/' . $image['path'];
	                                    ?>
	                                        <div class="item <?= $i == 1 ? 'active' : '' ?> " >
	                                            <center>	                                            	
	                                            	<?= Html::a('<img style="height: 100%; width: 100%;" src="'.$path.'">', ['/products/view?id='.$product->id], ['class' => 'product-container-outer']); ?>
	                                            </center>
	                                        </div>
	                                    <?php }
	                                    	if($i == 0){ 
	                                    	$path = "http://". $_SERVER['SERVER_NAME'] . '/images/no-images.jpg';
	                                    ?>
	                                    	<div class="item active " >
	                                            <center>	                                            	
	                                            	<?= Html::a('<img style="height: 100%; width: 100%;" src="'.$path.'">', ['/products/view?id='.$product->id], ['class' => 'product-container-outer']); ?>
	                                            </center>
	                                        </div>
	                                    <?php } ?>
	                                </div>
	                            </div>
	                        </div>
	                    </td>
	                </tr>
	                <tr>
	                   	<td>
		                    <div style="font-size: 13px;font-weight: bold; text-align: center;">
		                        <?= $catalog . $product->name?>
		                    </div>
	                   	</td>
	                    <td>
	                        <div style=" font-size: 12px; font-weight: bold; color:red; text-align: center;">
	                            <?= $last . $product->price . ' ' . $currency;?>
	                        </div>	                    	
	                    </td>
	                    <td style="width: 30px; ">
	                        <a onclick=" alert('Продукт успешно добавлен на корзину!'); $.post('/products/set-product?product_id=<?=$product->id?>&count=1'); $.pjax.reload({container:'#cash-pjax', async: false}); " class="btn btn-warning" style="font-size: 16px; font-weight: bold;"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>	
	                    </td>	                	
	                </tr>
	        	</table>
	    	</div>
			
		</div>  
	<?php }	?>
	</div>  
	<div class="col-xs-12 col-sm-12 col-md-12" style="text-align: center; padding-top: 20px;">
		<?php Pjax::begin(['enablePushState' => false, 'id' => 'cash-pjax']) ?>
			<?= Html::a('<i class="fa fa-shopping-basket "></i> <sup style="color:blue; font-weight:bold;">'.$count.'</sup>', ['/shopping-cart/index?user_id='.$user->id,], ['class' => 'btn btn-success', 'style' => 'width:200px;', 'title' => 'Корзинка']); ?>
		<?php Pjax::end() ?>
	</div>
</div>