<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<div class="client-form" style="margin-top: 20px;">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-3"> 
                <?= $form->field($model, 'search_name_archive')->textInput(['value' => $post['ProductsSearch']['search_name_archive'] ]) ?>
            </div>

            <div class="col-md-3"> 
                <?= $form->field($model, 'search_price_archive')->textInput([ 'type' => 'number', 'value' => $post['ProductsSearch']['search_price_archive'] ]) ?>
            </div>

            <div class="col-md-3"> 
                <?= $form->field($model, 'search_catalog_archive')->label()->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getCatalogList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'value' => $post['ProductsSearch']['search_catalog_archive'] ,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>


            <div class="col-md-2">
                <div class="form-group" >
                    <?= Html::submitButton('Поиск' , ['style'=>'margin-top:25px;','class' =>  'btn btn-primary']) ?>
                </div>
            </div>
         </div>   
    <?php ActiveForm::end(); ?>
    
</div>
