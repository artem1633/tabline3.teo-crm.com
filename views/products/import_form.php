<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
 <?php $form = ActiveForm::begin(['options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]) ?>
 
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'file')->label('Загрузить excel файл',[])->fileInput([/*'class'=>'sr-only'*/]) ?>
        </div>
        <div class="col-md-4">
            <?=Html::a('Шаблон файл', ['/products/send'],[ 'style' => 'margin-top:20px;', 'title'=> 'Пример шаблон','class'=>'btn btn-info btn-sm'])?>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>