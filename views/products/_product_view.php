<?php
use yii\helpers\Html;
use app\models\additional\Common;
use app\models\blocks\ProductBlock;

$currency = Common::find()->where(['user_id' => $product->user_id])->one()->getCurrencyFont();
$productBlock = ProductBlock::findOne($product_id);

?>
    <div class="table-responsive">
        <table class="table table-bordered">
            <?php 
                $data = json_decode($product->photos, true);
                if($product->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $product->last_price . ' ' . $currency . '</s> ';
                else $last = '';
            ?>
                <tr>
                    <td>
                        <div id="myCarouselPro" class="carousel slide" data-ride="carousel">                        
                            <div class="carousel-inner">
                                <?php 
                                $i = 0;
                                foreach ($data as $image) {
                                    $i++;
                                    $path = "http://". $_SERVER['SERVER_NAME'] . '/' . $image['path'];
                                ?>
                                    <div class="item <?= $i == 1 ? 'active' : '' ?> " >
                                        <?php if($productBlock->foto_visible){ ?>
                                            <center><img style="height: 160px; width: 180px;" src="<?=$path?>"></center>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div style=" font-size: 16px; font-weight: bold; text-align: center;">
                            <?php if($productBlock->price_visible){ ?>
                                <?= $last . $product->price . ' ' . $currency . ' &nbsp;&nbsp;';?>
                            <?php }
                                if($productBlock->button_visible){ ?>
                                    <a onclick = "alert('Продукт успешно добавлен на корзину!'); $.post('/products/set-product?product_id=<?=$product->id?>&count=1'); /*$.pjax.reload({container:'#cash-pjax', async: false});*/ " class="btn btn-warning">Купить</a>
                            <?php } ?>
                        </div>
                    </td> 
                    <td>
                        <?php if($productBlock->name_visible){ ?>
                            <div style=" margin-top: 10px; font-size: 16px;font-weight: bold; text-align: center;">
                                <?=$product->name?>
                            </div>
                        <?php }
                            if($productBlock->description_visible){ ?>
                            <div style="color: black; margin-top: 10px; font-size: 14px; text-align: center;">
                                <?=$product->description?>
                            </div>
                        <?php } ?>
                   </td>
                </tr>
        </table>
    </div>