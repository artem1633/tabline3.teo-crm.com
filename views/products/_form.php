<?php
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;

$k=0;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#product-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs"><b>Общая информация</b></span>
                        <span class="hidden-xs"><b>Общая информация</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#product-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Опция</b></span>
                        <span class="hidden-xs"><b>Опция</b></span>
                    </a>
                </li>
                <li class="">
                    <a href="#product-4" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><b>Ссылка</b></span>
                        <span class="hidden-xs"><b>Ссылка</b></span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane fade active in" id="product-1">
                    <div style="padding: 20px;" >
                        
                        <div class="row">
                            <div class="col-md-4">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'last_price')->textInput(['type' => 'number']) ?>
                            </div>
                        </div>

                        
                        <div class="row">
                            <div class="col-md-4">
                                <?= $form->field($model, 'catalog_id')->widget(Select2::classname(), [
                                    'data' => $model->getCatalogList(),
                                    'options' => ['placeholder' => 'Выберите ...'],
                                    'pluginEvents' => [
                                        "change" => "function() 
                                        {                                        
                                        }",
                                    ],
                                    'pluginOptions' => [
                                        'tags' => true,
                                        'allowClear' => true,
                                    ],
                                    ]);
                                ?>
                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'weight')->textInput(['type' => 'number']) ?>
                            </div>
                            <div class="col-md-2">
                                <?= $form->field($model, 'delivery')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>30,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'view_in_catalog')->widget(SwitchInput::classname(), [
                                    'type' => SwitchInput::CHECKBOX,
                                    'pluginOptions'=>[
                                        'handleWidth'=>105,
                                        'onText'=>'Да',
                                        'offText'=>'Нет'
                                    ]
                                ]) ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
                            </div>
                        </div>
                        
                        <div style="display: none;" >
                            <?= $form->field($model, 'photos')->textarea(['rows' => 3]) ?>
                        </div>
                        <input id="number" name="number" type="hidden" value="<?=$number?>">
                        <input id="path" name="path" type="hidden" value="<?= "images/products/".$number."/" ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <?= \kato\DropZone::widget([
                                   'options' => [
                                        'url'=> Url::to(['/products/upload', 'number' => $number]),
                                        'paramName'=>'image',
                                        //'maxFilesize' => '10',
                                        'addRemoveLinks' => true,
                                        'dictRemoveFile' => 'Удалить',
                                        'dictDefaultMessage' => "Выберите файлы",
                                        'acceptedFiles' => 'image/*',
                                        'init' => new JsExpression("function(file){ 

                                            /*var file_image = 'http://taplink.loc/images/products/5/images (1).jpg';
                                            var mockFile = { name: 'images (1).jpg', size: 6002, type: 'image/jpg' };*/
                                            var resultJsonInput = $('#products-photos').val();
                                            if(resultJsonInput.length > 0) {
                                                var resultArray = JSON.parse(resultJsonInput);
                                            } else {
                                                var resultArray = Array();
                                            }

                                            var length = resultArray.length;
                                            var path = 'http://taplink.loc/';
                                            //alert(path);
                                            for (i = 0; i < length; i++) {
                                                
                                                var file_image = path + resultArray[i].path;
                                                //alert(file_image);
                                                var mockFile = { name: resultArray[i].name, size: resultArray[i].size, type: resultArray[i].type };
                                                this.options.addedfile.call(this, mockFile);
                                                this.options.thumbnail.call(this, mockFile, file_image);
                                                mockFile.previewElement.classList.add('dz-success');
                                                mockFile.previewElement.classList.add('dz-complete');
                                            }
     
                                        }"),
                                   ],
                                   'clientEvents' => [
                                        'complete' => "function(file, index){
                                            var image = {
                                                name: file.name,
                                                size: file.size,
                                                type: file.type,
                                                path: ($('#path').val()+file.name),
                                            }; 
                                            /*console.log(image);*/
                                            var resultJsonInput = $('#products-photos').val();
                                            if(resultJsonInput.length > 0) {
                                                var resultArray = JSON.parse(resultJsonInput);
                                            } else {
                                                var resultArray = Array();
                                            }
                                            resultArray.push(image);
                                            var JsonResult = JSON.stringify(resultArray);
                                            $('#products-photos').val(JsonResult);

                                        }",
                                        'removedfile' => "function(file){
                                            $.get('remove-file',
                                            {'id':$('#number').val(), 'filename':file.name},
                                                function(data){ 

                                                    var resultJsonInput = $('#products-photos').val();
                                                    if(resultJsonInput.length > 0) {
                                                        var resultArray = JSON.parse(resultJsonInput);
                                                    } else {
                                                        var resultArray = Array();
                                                    }

                                                    var index = -1;
                                                    var length = resultArray.length;
                                                    for (i = 0; i < length; i++) {
                                                        if( resultArray[i].name == file.name && resultArray[i].size == file.size && resultArray[i].type == file.type) index = i;
                                                    }
                                                    resultArray.splice(index,1);
                                                    var JsonResult = JSON.stringify(resultArray);
                                                    $('#products-photos').val(JsonResult);
                                                    
                                                    $.get('set-image',
                                                    {'id':$('#number').val(), 'photos':JsonResult},
                                                        function(data){ }
                                                    );

                                                }     
                                            );
                                        }"
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="tab-pane fade" id="product-3">
                    <div style="padding: 20px;">
                        
                        <button type="button" class="btn btn-warning btn-xs pull-center" name="add" id="add_input">
                            + Добавить опцию
                        </button>
                        <div id="dynamic">
                            <?php
                                foreach ($options as $option) {
                                    $k++;
                                ?>
                                <div class="row" id="row<?=$k?>">
                                    <div class="col-md-12">
                                        <label class="control-label" ><br>Опция<?=$k?> </label>
                                    </div> 
                                    <div class="col-md-6"> 
                                        <input type="text" class="form-control" name="titles[]" value="<?=$option->name?>" placeholder="Укажите название опции"/> 
                                    </div> 
                                    <div class="col-md-5"> 
                                        <input type="text" class="form-control" name="prices[]" value="<?=$option->price?>" placeholder="Цена"/> 
                                    </div> 
                                    <div class="col-md-1" style="margin-left:-20px;" >
                                        <button type="button" name="remove" id="<?=$k?>" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div> 
                                    <input type="hidden" class="form-control" name="option_id[]" value="<?=$option->id?>"/> 
                                </div>
                                <?php
                                } ?>
                                <input type="hidden" class="form-control" id="count" name="count" value="<?=$k?>"/> 
                        </div>
                    </div>  
                </div>

                <div class="tab-pane fade" id="product-4">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'link')->textInput(['disabled' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var i = document.getElementById('count').value;
    var fileCollection = new Array();
    $('#add_input').click(function(){
    i++;
    $('#dynamic').append('<div class="row" id="row'+i+'"><div class="col-md-12"><label class="control-label" ><br>Опция'+i+' </label></div> <div class="col-md-6"> <input type="text" class="form-control" name="titles[]" placeholder="Укажите название опции"/> </div><div class="col-md-5"><input type="text" class="form-control" name="prices[]" placeholder="Цена"/></div> <div class="col-md-1" style="margin-left:-20px;" ><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id");
        $('#row'+button_id+'').remove();
    });

});
JS
);
?>