<?php

namespace app\modules\page\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\UsersPage;
use app\models\PageBlocks;
use app\models\Applications;
use app\models\blocks\ContactFormBlock;
use app\models\additional\Contacts;
use yii\web\NotFoundHttpException;
use app\models\Users;

/**
 * Default controller for the `page` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (!isset($_GET['user'])) throw new NotFoundHttpException('The requested page does not exist.');
        //$user = Users::find()->where(['login' => $_GET['user']])->one();
        $user_page = UsersPage::find()->where(['link_name' => $_GET['user'] ])->one();
        if($user_page != null)
        {    
            if($request->post())
            {
                $model = new ContactFormBlock(); 
                if($model->load($request->post()) && $model->validate())
                {
                    $id = $request->post()['ContactFormBlock']['id'];
                    $template = ContactFormBlock::findOne($id);
                    $fields = ''; $contacts = '';
                    if($template->name)
                    {
                        $fields .= $template->name_title . ' : ' . $model->name_description . ';<br>';
                    }
                    if($template->telephone)
                    {
                        $fields .= $template->telephone_title . ' : ' . $model->telephone_description . ';<br>';
                        if($model->telephone_description != '') $contacts .= $model->telephone_description . ',';
                    }
                    if($template->email)
                    {
                        $fields .= $template->email_title . ' : ' . $model->email_description . ';<br>';
                        if($model->email_description != '') $contacts .= $model->email_description . ',';
                    }
                    if($template->line)
                    {
                        $fields .= $template->line_title . ' : ' . $model->line_description . ';<br>';
                    }
                    if($template->number)
                    {
                        $fields .= $template->number_title . ' : ' . $model->number_description . ';<br>';
                    }
                    if($template->list)
                    {
                        $fields .= $template->list_title . ' : ' . $model->list_datas . ';<br>';
                    }
                    if($template->check)
                    {
                        $fields .= $template->check_title . ' : ' . ($model->check_description ? 'Да' : 'Нет') . ';<br>';
                    }
                    if($template->select_description)
                    {
                        $selects = '';
                        foreach ($model->select_description as $key => $value) {
                            $selects .= $value . ',';
                        }
                        $selects = substr($selects, 0, strlen( $selects ) -1 );

                        $fields .= $template->check_title . ' : ' . $selects . ';<br>';
                    }
                    if($template->text)
                    {
                        $fields .= $template->text_title . ' : ' . $model->text_description . ';<br>';
                    }
                    if($template->country)
                    {
                        $fields .= $template->country_title . ' : ' . $model->country_description . ';<br>';
                    }

                    /*$client  = @$_SERVER['HTTP_CLIENT_IP'];
                    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                    $remote  = @$_SERVER['REMOTE_ADDR'];
                    $result  = array('country'=>'', 'city'=>'');
                     
                    if(filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
                    elseif(filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
                    else $ip = $remote;
                     
                    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".Yii::$app->request->getRemoteIP()));    
                    if($ip_data && $ip_data->geoplugin_countryName != null)
                    {
                        $result = $ip_data->geoplugin_countryCode;
                    }*/

                    $application = new Applications();
                    $application->page_id = $template->page_id;
                    //$application->place = Yii::$app->request->getRemoteIP();
                    try {
                        $ip = Yii::$app->geoip->ip(Yii::$app->request->getRemoteIP());
                        $country   = $ip->country ? $ip->country  : 'Неизвестно';
                        $city      = $ip->city    ? $ip->city     : 'Неизвестно';
                        if($country == 'Неизвестно' && $city == 'Неизвестно') $application->place = 'Неизвестно';
                        else $application->place = $country . ' ' . $city;
                    } catch (\Exception $e) {
                        $application->place = 'Неизвестно';
                    }
                    $application->date = date('Y-m-d H:i:s');
                    $application->status = 'new';
                    $application->account_number = Applications::getLastId();
                    $application->budget = 0;
                    $application->pay = 'not_payed';
                    $application->purpose_of_payment = '';
                    $application->pay_type = '';
                    $application->pay_date = null;
                    $application->utm = '';
                    $application->name = $model->name_description;
                    $application->contacts = $contacts;
                    $application->fields = $fields;
                    $application->save();

                    $contact = Contacts::find()->where(['user_id' => $user_page->user_id, 'name' => $model->name_description, 'email' => $model->email_description, 'phone' => $model->telephone_description ])->one();
                    if($contact == null)
                    {
                        $contact = new Contacts();
                        $contact->user_id = $user_page->user_id;
                        $contact->name = $model->name_description;
                        $contact->email = $model->email_description;
                        $contact->phone = $model->telephone_description;
                        $contact->save();
                    }
                    
                    if($template->action_click == 'sms') {
                        Yii::$app->session->setFlash('success', $template->sms);
                    }
                    if($template->action_click == 'site') {
                        return $this->redirect(Url::to( 'http://'.$template->site, true));
                    }
                    if($template->action_click == 'page') {
                        $page = UsersPage::findOne($template->page);
                        return $this->redirect(Url::to( 'http://'.$_SERVER['SERVER_NAME'] . '/' . $page->link_name, true));
                    }
                }
            }
            $blocks = PageBlocks::find()->where(['page_id' => $user_page->id])->orderBy([ 'sorting' => SORT_ASC])->all();
            return $this->render('@app/views/users-page/link', [
                'user_page' => $user_page,
                'blocks' => $blocks,
            ]);
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
