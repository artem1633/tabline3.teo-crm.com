<?php

namespace app\modules\subsidiary;

/**
 * subsidiary module definition class
 */
class Subsidiary extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\subsidiary\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
