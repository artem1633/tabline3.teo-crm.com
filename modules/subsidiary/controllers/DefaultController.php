<?php

namespace app\modules\subsidiary\controllers;

use yii\web\Controller;
use app\models\UsersPage;
use app\models\PageBlocks;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `subsidiary` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	/*echo "<pre>";
    	print_r($_GET);
    	echo "</pre>";
    	die;*/
        if (!isset($_GET['user']) || !isset($_GET['page']) ) throw new NotFoundHttpException('The requested page does not exist.');
        $user_page = UsersPage::find()->where(['link_name' => $_GET['page'],])->one();
        if($user_page != null)
        {    
            $blocks = PageBlocks::find()->where(['page_id' => $user_page->id])->orderBy([ 'sorting' => SORT_ASC])->all();
            return $this->render('@app/views/users-page/link', [
                'user_page' => $user_page,
                'blocks' => $blocks,
            ]);
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
