<?php

use yii\db\Migration;

/**
 * Handles the creation of table `legal`.
 */
class m180902_095723_create_legal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('legal', [
            'id' => $this->primaryKey(),
            'director' => $this->string(255)->comment('ФИО директора'),
            'address' => $this->text()->comment('Юридический адрес'),
            'mailing_address' => $this->text()->comment('Почтовый адрес'),
            'state_registration' => $this->string(255)->comment('Свидетельство о гос. регистрации'),
            'inn' => $this->string(255)->comment('ИНН'),
            'ogrn' => $this->string(255)->comment('ОГРН'),
            'r_s' => $this->string(255)->comment('Р/с'),
            'bik' => $this->string(255)->comment('Бик'),
            'kor_schot' => $this->string(255)->comment('К/с'),
        ]);

        $this->insert('legal',array(
            'director' => 'Иванов Иван Иванович',
            'address' => null,
            'mailing_address' => null,
            'state_registration' => null,
            'inn' => null,
            'ogrn' => null,
            'r_s' => null,
            'bik' => null,
            'kor_schot' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('legal');
    }
}
