<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page_blocks`.
 */
class m180715_174244_create_page_blocks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('page_blocks', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'table_name' => $this->string(255),
            'sorting' => $this->integer(),
            'field_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('page_blocks');
    }
}
