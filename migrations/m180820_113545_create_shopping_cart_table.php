<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shopping_cart`.
 */
class m180820_113545_create_shopping_cart_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('shopping_cart', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'email' => $this->string(255),
            'phone' => $this->string(255),
            'address' => $this->text(),
            'pay_type_id' => $this->integer(),
            'summa' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('shopping_cart');
    }
}
