<?php

use yii\db\Migration;

/**
 * Handles the creation of table `advantage_block`.
 */
class m180723_080758_create_advantage_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('advantage_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'number' => $this->integer(),
            'image' => $this->string(255),
            'title' => $this->string(255),
            'text' => $this->text(),
        ]);

        $this->createIndex('idx-advantage_block-page_id', 'advantage_block', 'page_id', false);
        $this->addForeignKey("fk-advantage_block-page_id", "advantage_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-advantage_block-page_id','advantage_block');
        $this->dropIndex('idx-advantage_block-page_id','advantage_block');
        
        $this->dropTable('advantage_block');
    }
}
