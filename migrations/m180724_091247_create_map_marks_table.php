<?php

use yii\db\Migration;

/**
 * Handles the creation of table `map_marks`.
 */
class m180724_091247_create_map_marks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('map_marks', [
            'id' => $this->primaryKey(),
            'map_id' => $this->integer(),
            'coordinate_x' => $this->string(255),
            'coordinate_y' => $this->string(255),
            'title' => $this->string(255),
            'description' => $this->text(),
            'color' => $this->string(10),
        ]);

        $this->createIndex('idx-map_marks-map_id', 'map_marks', 'map_id', false);
        $this->addForeignKey("fk-map_marks-map_id", "map_marks", "map_id", "map_block", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-map_marks-map_id','map_marks');
        $this->dropIndex('idx-map_marks-map_id','map_marks');

        $this->dropTable('map_marks');
    }
}
