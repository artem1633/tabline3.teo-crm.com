<?php

use yii\db\Migration;
use app\models\Users;
use app\models\additional\Columns;
use app\models\Products;
use app\models\Applications;

/**
 * Class m180912_082635_add_default_values_to_columns_table
 */
class m180912_082635_add_default_values_to_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $users = Users::find()->all();
        foreach ($users as $user) 
        {
            if(Columns::find()->where(['user_id' => $user->id, 'table_name' => 'products'])->one() == null)
            {
                Products::setDefaultValues($user->id);
            }

            if(Columns::find()->where(['user_id' => $user->id, 'table_name' => 'applications'])->one() == null)
            {
                Applications::setDefaultValues($user->id);
            }
        }
    }

    public function down()
    {

    }
}
