<?php

use yii\db\Migration;

/**
 * Handles the creation of table `map_block`.
 */
class m180724_035135_create_map_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('map_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->comment('Страница'),
            'center_x' => $this->string(255)->comment('Координата X'),
            'center_y' => $this->string(255)->comment('Координата Y'),
            'lock_map' => $this->boolean()->defaultValue(1)->comment('Зафиксировать карту'),
            'add_mark' => $this->boolean()->defaultValue(1)->comment('Добавить отдельные ссылки для каждой метки'),
            'show_select' => $this->boolean()->defaultValue(1)->comment('Показывать выбор карты'),
            'show_zoom' => $this->boolean()->defaultValue(1)->comment('Показывать опцию масштабирования'),
            'show_panorama' => $this->boolean()->defaultValue(1)->comment('Показывать интерфейс панорамы улиц'),
        ]);

        $this->createIndex('idx-map_block-page_id', 'map_block', 'page_id', false);
        $this->addForeignKey("fk-map_block-page_id", "map_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-map_block-page_id','map_block');
        $this->dropIndex('idx-map_block-page_id','map_block');
        
        $this->dropTable('map_block');
    }
}
