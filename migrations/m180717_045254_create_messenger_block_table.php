<?php

use yii\db\Migration;

/**
 * Handles the creation of table `messenger_block`.
 */
class m180717_045254_create_messenger_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('messenger_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'display' => $this->string(50),
            'whatsapp' => $this->boolean(),
            'whatsapp_title' => $this->string(255),
            'whatsapp_phone' => $this->string(255),
            'whatsapp_text' => $this->text(),
            'telegram' => $this->boolean(),
            'telegram_title' => $this->string(255),
            'telegram_user' => $this->string(255),
            'facebook' => $this->boolean(),
            'facebook_title' => $this->string(255),
            'facebook_user' => $this->string(255),
            'vk' => $this->boolean(),
            'vk_title' => $this->string(255),
            'vk_user' => $this->string(255),
            'viber' => $this->boolean(),
            'viber_title' => $this->string(255),
            'viber_phone' => $this->string(255),
            'skype' => $this->boolean(),
            'skype_title' => $this->string(255),
            'skype_user' => $this->string(255),
            'phone' => $this->boolean(),
            'phone_number' => $this->string(255),
            'email' => $this->boolean(),
            'email_title' => $this->string(255),
        ]);

        $this->createIndex('idx-messenger_block-page_id', 'messenger_block', 'page_id', false);
        $this->addForeignKey("fk-messenger_block-page_id", "messenger_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-messenger_block-page_id','messenger_block');
        $this->dropIndex('idx-messenger_block-page_id','messenger_block');
        
        $this->dropTable('messenger_block');
    }
}
