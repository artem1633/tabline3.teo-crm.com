<?php

use yii\db\Migration;

/**
 * Handles the creation of table `goods_catalog`.
 */
class m180811_090112_create_goods_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('goods_catalog', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'hide' => $this->boolean()->defaultValue(0),
        ]);

        $this->createIndex('idx-goods_catalog-user_id', 'goods_catalog', 'user_id', false);
        $this->addForeignKey("fk-goods_catalog-user_id", "goods_catalog", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-goods_catalog-user_id','goods_catalog');
        $this->dropIndex('idx-goods_catalog-user_id','goods_catalog');
        
        $this->dropTable('goods_catalog');
    }
}
