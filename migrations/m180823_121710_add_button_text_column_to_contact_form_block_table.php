<?php

use yii\db\Migration;

/**
 * Handles adding button_text to table `contact_form_block`.
 */
class m180823_121710_add_button_text_column_to_contact_form_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contact_form_block', 'name_required', $this->boolean());
        $this->addColumn('contact_form_block', 'telephone_required', $this->boolean());
        $this->addColumn('contact_form_block', 'email_required', $this->boolean());
        $this->addColumn('contact_form_block', 'line_required', $this->boolean());
        $this->addColumn('contact_form_block', 'number_required', $this->boolean());
        $this->addColumn('contact_form_block', 'list_required', $this->boolean());
        $this->addColumn('contact_form_block', 'check_required', $this->boolean());
        $this->addColumn('contact_form_block', 'select_required', $this->boolean());
        $this->addColumn('contact_form_block', 'text_required', $this->boolean());
        $this->addColumn('contact_form_block', 'country_required', $this->boolean());

        $this->addColumn('contact_form_block', 'button_text', $this->string(255));
        $this->addColumn('contact_form_block', 'action_click', $this->string(255));
        $this->addColumn('contact_form_block', 'sms', $this->string(255));
        $this->addColumn('contact_form_block', 'site', $this->string(255));
        $this->addColumn('contact_form_block', 'page', $this->string(255));
        $this->addColumn('contact_form_block', 'button_color', $this->string(10));
        $this->addColumn('contact_form_block', 'button_background', $this->string(10));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contact_form_block', 'name_required');
        $this->dropColumn('contact_form_block', 'telephone_required');
        $this->dropColumn('contact_form_block', 'email_required');
        $this->dropColumn('contact_form_block', 'line_required');
        $this->dropColumn('contact_form_block', 'number_required');
        $this->dropColumn('contact_form_block', 'list_required');
        $this->dropColumn('contact_form_block', 'check_required');
        $this->dropColumn('contact_form_block', 'select_required');
        $this->dropColumn('contact_form_block', 'text_required');
        $this->dropColumn('contact_form_block', 'country_required');

        $this->dropColumn('contact_form_block', 'button_text');
        $this->dropColumn('contact_form_block', 'action_click');
        $this->dropColumn('contact_form_block', 'sms');
        $this->dropColumn('contact_form_block', 'site');
        $this->dropColumn('contact_form_block', 'page');
        $this->dropColumn('contact_form_block', 'button_color');
        $this->dropColumn('contact_form_block', 'button_background');
    }
}
