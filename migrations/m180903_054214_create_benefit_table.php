<?php

use yii\db\Migration;

/**
 * Handles the creation of table `benefit`.
 */
class m180903_054214_create_benefit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('benefit', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название изображения'),
            'teg' => $this->text()->comment('Тег'),
            'image' => $this->string(255)->comment('Фото'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('benefit');
    }
}
