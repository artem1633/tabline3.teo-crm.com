<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clicks`.
 */
class m180815_120557_create_clicks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clicks', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(50),
            'field_id' => $this->integer(),
            'name' => $this->string(255),
            'date' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('clicks');
    }
}
