<?php

use yii\db\Migration;

/**
 * Handles the creation of table `video_block`.
 */
class m180716_191208_create_video_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('video_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'link' => $this->string(500),
            'description' => $this->text(),
            'show_elements' => $this->boolean(),
        ]);

        $this->createIndex('idx-video_block-page_id', 'video_block', 'page_id', false);
        $this->addForeignKey("fk-video_block-page_id", "video_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-video_block-page_id','video_block');
        $this->dropIndex('idx-video_block-page_id','video_block');

        $this->dropTable('video_block');
    }
}
