<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faq`.
 */
class m180901_125358_create_faq_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'question' => $this->text()->comment('Вопрос'),
            'answer' => $this->text()->comment('Ответ'),
            'status' => $this->integer()->comment('Статус'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('faq');
    }
}
