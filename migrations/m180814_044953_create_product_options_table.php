<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_options`.
 */
class m180814_044953_create_product_options_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_options', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'name' => $this->string(255),
            'price' => $this->float(),
        ]);

        $this->createIndex('idx-product_options-product_id', 'product_options', 'product_id', false);
        $this->addForeignKey("fk-product_options-product_id", "product_options", "product_id", "products", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_options-product_id','product_options');
        $this->dropIndex('idx-product_options-product_id','product_options');
        
        $this->dropTable('product_options');
    }
}
