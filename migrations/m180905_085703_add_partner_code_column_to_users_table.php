<?php

use yii\db\Migration;

/**
 * Handles adding partner_code to table `users`.
 */
class m180905_085703_add_partner_code_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'partner_code', $this->string(255));
        $this->addColumn('users', 'paternity_user_code', $this->string(255));
        $this->addColumn('users', 'promo_code', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'partner_code');
        $this->dropColumn('users', 'paternity_user_code');
        $this->dropColumn('users', 'promo_code');
    }
}
