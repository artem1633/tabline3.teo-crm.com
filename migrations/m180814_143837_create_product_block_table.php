<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_block`.
 */
class m180814_143837_create_product_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'number' => $this->integer(),
            'view' => $this->string(20),
            'product_id' => $this->integer(),
            'contact_form_id' => $this->integer(),
            'checkbox' => $this->boolean(),
            'foto_visible' => $this->boolean(),
            'name_visible' => $this->boolean(),
            'description_visible' => $this->boolean(),
            'price_visible' => $this->boolean(),
            'contact_form_visible' => $this->boolean(),
            'button_visible' => $this->boolean(),
            'visible' => $this->boolean(),
            'title_text' => $this->string(255),
            'visible_in_menu' => $this->boolean(),
            'name_in_menu' => $this->string(255),
        ]);

        $this->createIndex('idx-product_block-page_id', 'product_block', 'page_id', false);
        $this->addForeignKey("fk-product_block-page_id", "product_block", "page_id", "users_page", "id");

        $this->createIndex('idx-product_block-product_id', 'product_block', 'product_id', false);
        $this->addForeignKey("fk-product_block-product_id", "product_block", "product_id", "products", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_block-page_id','product_block');
        $this->dropIndex('idx-product_block-page_id','product_block');

        $this->dropForeignKey('fk-product_block-product_id','product_block');
        $this->dropIndex('idx-product_block-product_id','product_block');

        $this->dropTable('product_block');
    }
}
