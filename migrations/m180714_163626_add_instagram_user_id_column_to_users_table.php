<?php

use yii\db\Migration;

/**
 * Handles adding instagram_user_id to table `users`.
 */
class m180714_163626_add_instagram_user_id_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'instagram_user_id', $this->string(500));
        $this->addColumn('users', 'facebook_user_id', $this->string(500));
        $this->addColumn('users', 'vk_user_id', $this->string(500));
        $this->addColumn('users', 'google_account_user_id', $this->string(500));

        $this->addColumn('users', 'registry_date', $this->date());
        $this->addColumn('users', 'last_payment_date', $this->date());
        $this->addColumn('users', 'total_payment_amount', $this->float()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'instagram_user_id');
        $this->dropColumn('users', 'facebook_user_id');
        $this->dropColumn('users', 'vk_user_id');
        $this->dropColumn('users', 'google_account_user_id');

        $this->dropColumn('users', 'registry_date');
        $this->dropColumn('users', 'last_payment_date');
        $this->dropColumn('users', 'total_payment_amount');
    }
}
