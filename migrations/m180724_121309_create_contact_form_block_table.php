<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact_form_block`.
 */
class m180724_121309_create_contact_form_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contact_form_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'name' => $this->boolean(),
            'name_title' => $this->string(255),
            'name_description' => $this->string(255),
            'telephone' => $this->boolean(),
            'telephone_title' => $this->string(255),
            'telephone_description' => $this->string(255),
            'email' => $this->boolean(),
            'email_title' => $this->string(255),
            'email_description' => $this->string(255),
            'line' => $this->boolean(),
            'line_title' => $this->string(255),
            'line_description' => $this->text(),
            'number' => $this->boolean(),
            'number_title' => $this->string(255),
            'number_description' => $this->string(255),
            'list' => $this->boolean(),
            'list_title' => $this->string(255),
            'list_description' => $this->string(255),
            'list_data' => $this->text(),
            'check' => $this->boolean(),
            'check_title' => $this->string(255),
            'check_description' => $this->string(255),
            'check_selected' => $this->boolean(),
            'select' => $this->boolean(),
            'select_title' => $this->string(255),
            'select_description' => $this->string(255),
            'select_data' => $this->text(),
            'text' => $this->boolean(),
            'text_title' => $this->string(255),
            'text_description' => $this->string(255),
            'country' => $this->boolean(),
            'country_title' => $this->string(255),
            'country_description' => $this->string(255),
        ]);

        $this->createIndex('idx-contact_form_block-page_id', 'contact_form_block', 'page_id', false);
        $this->addForeignKey("fk-contact_form_block-page_id", "contact_form_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-contact_form_block-page_id','contact_form_block');
        $this->dropIndex('idx-contact_form_block-page_id','contact_form_block');
        
        $this->dropTable('contact_form_block');
    }
}
