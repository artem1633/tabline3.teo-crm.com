<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tariffs`.
 */
class m180826_180229_create_tariffs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tariffs', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'price_list' => $this->text(),
        ]);

        $this->insert('tariffs',array(
            'name' => 'Free',
            'key' => 'free',
            'price_list' => '[{"time":1,"price":0},{"time":3,"price":0},{"time":6,"price":0},{"time":12,"price":0}]',
        ));

        $this->insert('tariffs',array(
            'name' => 'Pro',
            'key' => 'pro',
            'price_list' => '[{"time":1,"price":149},{"time":3,"price":349},{"time":6,"price":599},{"time":12,"price":999}]',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tariffs');
    }
}
