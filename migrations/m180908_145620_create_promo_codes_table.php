<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo_codes`.
 */
class m180908_145620_create_promo_codes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('promo_codes', [
            'id' => $this->primaryKey(),
            'code' => $this->string(255)->comment('Промокод'),
            'access_date' => $this->date()->comment('Срок действия'),
            'remaining_input' => $this->integer()->comment('Количество доступных вводов'),
            'status' => $this->boolean()->comment('Статус'),
            'protsent' => $this->integer()->comment('Процент скидки по данному промо-коду'),
            'ended_input' => $this->integer()->defaultValue(0)->comment('Количество совершенных вводов'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('promo_codes');
    }
}
