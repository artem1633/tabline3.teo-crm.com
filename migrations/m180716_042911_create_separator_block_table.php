<?php

use yii\db\Migration;

/**
 * Handles the creation of table `separator_block`.
 */
class m180716_042911_create_separator_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('separator_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'separator_size' => $this->string(50),
        ]);

        $this->createIndex('idx-separator_block-page_id', 'separator_block', 'page_id', false);
        $this->addForeignKey("fk-separator_block-page_id", "separator_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-separator_block-page_id','separator_block');
        $this->dropIndex('idx-separator_block-page_id','separator_block');
        
        $this->dropTable('separator_block');
    }
}
