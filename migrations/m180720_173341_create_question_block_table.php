<?php

use yii\db\Migration;

/**
 * Handles the creation of table `question_block`.
 */
class m180720_173341_create_question_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('question_block', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'page_id' => $this->integer(),
            'question_title' => $this->text(),
            'answer' => $this->text(),
        ]);

        $this->createIndex('idx-question_block-page_id', 'question_block', 'page_id', false);
        $this->addForeignKey("fk-question_block-page_id", "question_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-question_block-page_id','question_block');
        $this->dropIndex('idx-question_block-page_id','question_block');
        
        $this->dropTable('question_block');
    }
}
