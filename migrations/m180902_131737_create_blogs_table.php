<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blogs`.
 */
class m180902_131737_create_blogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('blogs', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'date' => $this->date(),
            'text' => $this->text(),
            'image' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('blogs');
    }
}
