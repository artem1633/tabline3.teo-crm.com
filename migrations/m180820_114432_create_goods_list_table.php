<?php

use yii\db\Migration;

/**
 * Handles the creation of table `goods_list`.
 */
class m180820_114432_create_goods_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('goods_list', [
            'id' => $this->primaryKey(),
            'shopping_cart_id' => $this->integer(),
            'product_id' => $this->integer(),
            'count' => $this->integer(),
        ]);

        $this->createIndex('idx-goods_list-shopping_cart_id', 'goods_list', 'shopping_cart_id', false);
        $this->addForeignKey("fk-goods_list-shopping_cart_id", "goods_list", "shopping_cart_id", "shopping_cart", "id");

        $this->createIndex('idx-goods_list-product_id', 'goods_list', 'product_id', false);
        $this->addForeignKey("fk-goods_list-product_id", "goods_list", "product_id", "products", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-goods_list-shopping_cart_id','goods_list');
        $this->dropIndex('idx-goods_list-shopping_cart_id','goods_list');

        $this->dropForeignKey('fk-goods_list-product_id','goods_list');
        $this->dropIndex('idx-goods_list-product_id','goods_list');

        $this->dropTable('goods_list');
    }
}
