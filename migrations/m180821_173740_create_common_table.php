<?php

use yii\db\Migration;

/**
 * Handles the creation of table `common`.
 */
class m180821_173740_create_common_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('common', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'timezone' => $this->integer(),
            'pixel_id' => $this->integer(),
            'currency' => $this->integer(),
            'html_code' => $this->text(),
        ]);

        $this->createIndex('idx-common-user_id', 'common', 'user_id', false);
        $this->addForeignKey("fk-common-user_id", "common", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-common-user_id','common');
        $this->dropIndex('idx-common-user_id','common');
        
        $this->dropTable('common');
    }
}
