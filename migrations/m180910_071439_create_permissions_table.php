<?php

use yii\db\Migration;

/**
 * Handles the creation of table `permissions`.
 */
class m180910_071439_create_permissions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('permissions', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->defaultValue(1)->comment('Пользователь'),
            'view_user' => $this->boolean()->defaultValue(1)->comment('Возможность просматривать информацию о пользователях'),
            'delete_user' => $this->boolean()->defaultValue(1)->comment('Возможность удалять пользователей'),
            'create_user' => $this->boolean()->defaultValue(1)->comment('Возможность создавать пользователей'),
            'change_tariff' => $this->boolean()->defaultValue(1)->comment('Возможность вручную менять тарифный план пользователя и его срок действия'),
            'view_page' => $this->boolean()->defaultValue(1)->comment('Возможность просматривать страницы'),
            'change_content_page' => $this->boolean()->defaultValue(1)->comment('Редактирование контентных страниц'),
            'change_price_list' => $this->boolean()->defaultValue(1)->comment('Редактирование цен на тарифы'),
            'answer_to_questions' => $this->boolean()->defaultValue(1)->comment('Ответы на сообщения пользователей'),
            'change_promocode' => $this->boolean()->defaultValue(1)->comment('Изменение срока действия промо-периода для новых пользователей'),
            'close_window' => $this->boolean()->defaultValue(1)->comment('Отключать у отдельных пользователей всплывающее окно при переходе на страницу'),
            'free_period' => $this->boolean()->defaultValue(1)->comment('Включение/отключение бесплатного периода использования платного тарифа при регистрации по реферальной ссылке или коду партнерской программы'),
            'execute_order' => $this->boolean()->defaultValue(1)->comment('Исполнять заявки по выплатам в реферальной программе'),
            'unlock_registration' => $this->boolean()->defaultValue(1)->comment('Разблокировать для регистрации имя пользователя'),
        ]);

        $this->addCommentOnTable('permissions', 'Набор прав');

        $this->createIndex('idx-permissions-user_id', 'permissions', 'user_id', false);
        $this->addForeignKey("fk-permissions-user_id", "permissions", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-permissions-user_id','permissions');
        $this->dropIndex('idx-permissions-user_id','permissions');

        $this->dropTable('permissions');
    }
}
