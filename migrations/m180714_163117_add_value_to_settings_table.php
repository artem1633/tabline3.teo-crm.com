<?php

use yii\db\Migration;

/**
 * Class m180714_163117_add_value_to_settings_table
 */
class m180714_163117_add_value_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('settings',array(
            'name' => 'VK ID приложения',
            'key' => 'vk_id_app',
            'value' =>'6632896',
        ));

        $this->insert('settings',array(
            'name' => 'VK Защищённый ключ',
            'key' => 'vk_secret_key',
            'value' =>'lZVFmbCW4NRgD4c5Q53y',
        ));

        $this->insert('settings',array(
            'name' => 'VK Api Callback',
            'key' => 'vk_api_callback',
            'value' =>'http://rieltor.samar.uz/site/vkontakte',
        ));
    }

    public function down()
    {

    }
}
