<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m180811_153310_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'price' => $this->float(),
            'last_price' => $this->float(),
            'catalog_id' => $this->integer(),
            'weight' => $this->float(),
            'delivery' => $this->integer(),
            'view_in_catalog' => $this->integer(),
            'photos' => $this->text(),
            'status' => $this->string(20),
            'date_cr' => $this->datetime(),
            'date_up' => $this->datetime(),
            'link' => $this->string(255),
        ]);

        $this->createIndex('idx-products-user_id', 'products', 'user_id', false);
        $this->addForeignKey("fk-products-user_id", "products", "user_id", "users", "id");

        $this->createIndex('idx-products-catalog_id', 'products', 'catalog_id', false);
        $this->addForeignKey("fk-products-catalog_id", "products", "catalog_id", "goods_catalog", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-products-user_id','products');
        $this->dropIndex('idx-products-user_id','products');

        $this->dropForeignKey('fk-products-catalog_id','products');
        $this->dropIndex('idx-products-catalog_id','products');

        $this->dropTable('products');
    }
}
