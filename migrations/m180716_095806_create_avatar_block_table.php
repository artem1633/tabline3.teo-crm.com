<?php

use yii\db\Migration;

/**
 * Handles the creation of table `avatar_block`.
 */
class m180716_095806_create_avatar_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('avatar_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'avatar_size' => $this->string(50),
            'hide_avatar' => $this->boolean(),
        ]);

        $this->createIndex('idx-avatar_block-page_id', 'avatar_block', 'page_id', false);
        $this->addForeignKey("fk-avatar_block-page_id", "avatar_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-avatar_block-page_id','avatar_block');
        $this->dropIndex('idx-avatar_block-page_id','avatar_block');

        $this->dropTable('avatar_block');
    }
}
