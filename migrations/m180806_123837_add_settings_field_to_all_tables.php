<?php

use yii\db\Migration;

/**
 * Class m180806_123837_add_settings_field_to_all_tables
 */
class m180806_123837_add_settings_field_to_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('text_block', 'visible', $this->boolean());
        $this->addColumn('text_block', 'title_text', $this->string(255));
        $this->addColumn('text_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('text_block', 'name_in_menu', $this->string(255));

        $this->addColumn('link_block', 'visible', $this->boolean());
        $this->addColumn('link_block', 'title_text', $this->string(255));
        $this->addColumn('link_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('link_block', 'name_in_menu', $this->string(255));

        $this->addColumn('html_block', 'visible', $this->boolean());
        $this->addColumn('html_block', 'title_text', $this->string(255));
        $this->addColumn('html_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('html_block', 'name_in_menu', $this->string(255));

        $this->addColumn('avatar_block', 'visible', $this->boolean());
        $this->addColumn('avatar_block', 'title_text', $this->string(255));
        $this->addColumn('avatar_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('avatar_block', 'name_in_menu', $this->string(255));

        $this->addColumn('social_block', 'visible', $this->boolean());
        $this->addColumn('social_block', 'title_text', $this->string(255));
        $this->addColumn('social_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('social_block', 'name_in_menu', $this->string(255));

        $this->addColumn('video_block', 'visible', $this->boolean());
        $this->addColumn('video_block', 'title_text', $this->string(255));
        $this->addColumn('video_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('video_block', 'name_in_menu', $this->string(255));

        $this->addColumn('messenger_block', 'visible', $this->boolean());
        $this->addColumn('messenger_block', 'title_text', $this->string(255));
        $this->addColumn('messenger_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('messenger_block', 'name_in_menu', $this->string(255));

        $this->addColumn('question_block', 'visible', $this->boolean());
        $this->addColumn('question_block', 'title_text', $this->string(255));
        $this->addColumn('question_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('question_block', 'name_in_menu', $this->string(255));

        $this->addColumn('carusel_block', 'visible', $this->boolean());
        $this->addColumn('carusel_block', 'title_text', $this->string(255));
        $this->addColumn('carusel_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('carusel_block', 'name_in_menu', $this->string(255));

        $this->addColumn('unique_selling_block', 'visible', $this->boolean());
        $this->addColumn('unique_selling_block', 'title_text', $this->string(255));
        $this->addColumn('unique_selling_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('unique_selling_block', 'name_in_menu', $this->string(255));

        $this->addColumn('advantage_block', 'visible', $this->boolean());
        $this->addColumn('advantage_block', 'title_text', $this->string(255));
        $this->addColumn('advantage_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('advantage_block', 'name_in_menu', $this->string(255));

        $this->addColumn('map_block', 'visible', $this->boolean());
        $this->addColumn('map_block', 'title_text', $this->string(255));
        $this->addColumn('map_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('map_block', 'name_in_menu', $this->string(255));

        $this->addColumn('contact_form_block', 'visible', $this->boolean());
        $this->addColumn('contact_form_block', 'title_text', $this->string(255));
        $this->addColumn('contact_form_block', 'visible_in_menu', $this->boolean());
        $this->addColumn('contact_form_block', 'name_in_menu', $this->string(255));

    }

    public function down()
    {
        $this->dropColumn('text_block', 'visible');
        $this->dropColumn('text_block', 'title_text');
        $this->dropColumn('text_block', 'visible_in_menu');
        $this->dropColumn('text_block', 'name_in_menu');

        $this->dropColumn('link_block', 'visible');
        $this->dropColumn('link_block', 'title_text');
        $this->dropColumn('link_block', 'visible_in_menu');
        $this->dropColumn('link_block', 'name_in_menu');

        $this->dropColumn('html_block', 'visible');
        $this->dropColumn('html_block', 'title_text');
        $this->dropColumn('html_block', 'visible_in_menu');
        $this->dropColumn('html_block', 'name_in_menu');

        $this->dropColumn('avatar_block', 'visible');
        $this->dropColumn('avatar_block', 'title_text');
        $this->dropColumn('avatar_block', 'visible_in_menu');
        $this->dropColumn('avatar_block', 'name_in_menu');

        $this->dropColumn('social_block', 'visible');
        $this->dropColumn('social_block', 'title_text');
        $this->dropColumn('social_block', 'visible_in_menu');
        $this->dropColumn('social_block', 'name_in_menu');

        $this->dropColumn('video_block', 'visible');
        $this->dropColumn('video_block', 'title_text');
        $this->dropColumn('video_block', 'visible_in_menu');
        $this->dropColumn('video_block', 'name_in_menu');

        $this->dropColumn('messenger_block', 'visible');
        $this->dropColumn('messenger_block', 'title_text');
        $this->dropColumn('messenger_block', 'visible_in_menu');
        $this->dropColumn('messenger_block', 'name_in_menu');

        $this->dropColumn('question_block', 'visible');
        $this->dropColumn('question_block', 'title_text');
        $this->dropColumn('question_block', 'visible_in_menu');
        $this->dropColumn('question_block', 'name_in_menu');

        $this->dropColumn('carusel_block', 'visible');
        $this->dropColumn('carusel_block', 'title_text');
        $this->dropColumn('carusel_block', 'visible_in_menu');
        $this->dropColumn('carusel_block', 'name_in_menu');

        $this->dropColumn('unique_selling_block', 'visible');
        $this->dropColumn('unique_selling_block', 'title_text');
        $this->dropColumn('unique_selling_block', 'visible_in_menu');
        $this->dropColumn('unique_selling_block', 'name_in_menu');

        $this->dropColumn('advantage_block', 'visible');
        $this->dropColumn('advantage_block', 'title_text');
        $this->dropColumn('advantage_block', 'visible_in_menu');
        $this->dropColumn('advantage_block', 'name_in_menu');

        $this->dropColumn('map_block', 'visible');
        $this->dropColumn('map_block', 'title_text');
        $this->dropColumn('map_block', 'visible_in_menu');
        $this->dropColumn('map_block', 'name_in_menu');

        $this->dropColumn('contact_form_block', 'visible');
        $this->dropColumn('contact_form_block', 'title_text');
        $this->dropColumn('contact_form_block', 'visible_in_menu');
        $this->dropColumn('contact_form_block', 'name_in_menu');
    }
}
