<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_access`.
 */
class m180827_183559_create_users_access_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_access', [
            'id' => $this->primaryKey(),
            'user_from' => $this->integer(),
            'user_to' => $this->integer(),
            'access_type' => $this->integer()->comment('Тип доступа'),
            'view_page' => $this->boolean()->comment('Просматривать страницы'),
            'update_page' => $this->boolean()->comment('Редактировать страницы'),
            'view_application' => $this->boolean()->comment('Просмотривать заявки'),
            'update_application' => $this->boolean()->comment('Редактировать заявки'),
            'view_statics' => $this->boolean()->comment('Просматривать статистику'),
            'update_settings' => $this->boolean()->comment('Редактировать настройки'),
        ]);

        $this->addCommentOnTable('users_access', 'Открыть доступ');

        $this->createIndex('idx-users_access-user_from', 'users_access', 'user_from', false);
        $this->addForeignKey("fk-users_access-user_from", "users_access", "user_from", "users", "id");

        $this->createIndex('idx-users_access-user_to', 'users_access', 'user_to', false);
        $this->addForeignKey("fk-users_access-user_to", "users_access", "user_to", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_access-user_from','users_access');
        $this->dropIndex('idx-users_access-user_from','users_access');

        $this->dropForeignKey('fk-users_access-user_to','users_access');
        $this->dropIndex('idx-users_access-user_to','users_access');

        $this->dropTable('users_access');
    }
}
