<?php

use yii\db\Migration;

/**
 * Handles the creation of table `text_block`.
 */
class m180715_082857_create_text_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('text_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'text_size' => $this->string(50),
            'alignment' => $this->string(50),
            'text' => $this->text(),
        ]);

        $this->createIndex('idx-text_block-page_id', 'text_block', 'page_id', false);
        $this->addForeignKey("fk-text_block-page_id", "text_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-text_block-page_id','text_block');
        $this->dropIndex('idx-text_block-page_id','text_block');
        
        $this->dropTable('text_block');
    }
}
