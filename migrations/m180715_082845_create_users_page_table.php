<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_page`.
 */
class m180715_082845_create_users_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_page', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string(255),
            'link_name' => $this->string(255),
        ]);

        $this->createIndex('idx-users_page-user_id', 'users_page', 'user_id', false);
        $this->addForeignKey("fk-users_page-user_id", "users_page", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-users_page-user_id','users_page');
        $this->dropIndex('idx-users_page-user_id','users_page');

        $this->dropTable('users_page');
    }
}
