<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180714_041755_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'key' => $this->string(255),
            'value' => $this->string(500),
        ]);

        $this->insert('settings',array(
            'name' => 'Instagram Client Id',
            'key' => 'instagram_client_id',
            'value' =>'2504aa92d0904ff59832fb949ec78734',
        ));

        $this->insert('settings',array(
            'name' => 'Instagram Client Secret Key',
            'key' => 'instagram_client_secret_key',
            'value' =>'2ab2a781f1bd4cb4a9ec1ea08cfa283f',
        ));

        $this->insert('settings',array(
            'name' => 'Instagram Api Callback',
            'key' => 'instagram_api_callback',
            'value' =>'http://rieltor.samar.uz/site/instagram',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
