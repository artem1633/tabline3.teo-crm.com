<?php

use yii\db\Migration;

/**
 * Handles the creation of table `unique_selling_block`.
 */
class m180721_155229_create_unique_selling_block_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('unique_selling_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'title' => $this->string(255),
            'text' => $this->text(),
            'image' => $this->string(255),
            'button_text' => $this->string(50),
            'link' => $this->string(255),
        ]);

        $this->createIndex('idx-unique_selling_block-page_id', 'unique_selling_block', 'page_id', false);
        $this->addForeignKey("fk-unique_selling_block-page_id", "unique_selling_block", "page_id", "users_page", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-unique_selling_block-page_id','unique_selling_block');
        $this->dropIndex('idx-unique_selling_block-page_id','unique_selling_block');
        
        $this->dropTable('unique_selling_block');
    }
}
