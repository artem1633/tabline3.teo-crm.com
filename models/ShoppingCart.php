<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shopping_cart".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property int $pay_type_id
 * @property double $summa
 *
 * @property GoodsList[] $goodsLists
 */
class ShoppingCart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shopping_cart';
    }
    public $step;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['pay_type_id'], 'integer'],
            [['summa'], 'number'],
            [['email'], 'email'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            [['name', 'email', 'phone', 'address'], 'required', 'when' => function() {
                   if($this->step == 2) return TRUE;
                   else return FALSE;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'pay_type_id' => 'Тип оплаты',
            'summa' => 'Итого',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsLists()
    {
        return $this->hasMany(GoodsList::className(), ['shopping_cart_id' => 'id']);
    }
}
