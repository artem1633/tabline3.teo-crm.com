<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "columns".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $table_name Таблица
 * @property string $type Поля
 * @property string $name Наименование
 * @property int $status Статус
 * @property int $order_number Сортировка
 *
 * @property Users $user
 */
class Columns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'columns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'order_number'], 'integer'],
            [['table_name', 'type', 'name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'table_name' => 'Таблица',
            'type' => 'Поля',
            'name' => 'Наименование',
            'status' => 'Статус',
            'order_number' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\Users::className(), ['id' => 'user_id']);
    }
}
