<?php

namespace app\models\additional;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "common".
 *
 * @property int $id
 * @property int $user_id
 * @property int $timezone
 * @property int $pixel_id
 * @property int $currency
 * @property string $html_code
 *
 * @property Users $user
 */
class Common extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'common';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'timezone', 'pixel_id', 'currency'], 'integer'],
            [['html_code'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'timezone' => 'Часавой пояс',
            'pixel_id' => 'Facebook Pixel ID',
            'currency' => 'Валюта',
            'html_code' => 'Вставка HTML кода',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\Users::className(), ['id' => 'user_id']);
    }

    /**
     * Добавление первичных значений.
     */
    public function setDefaultValues($user_id)
    {
        $common = new Common();
        $common->user_id = $user_id;
        $common->timezone = 3;
        $common->currency = 1;
        $common->save();
    }

    //Получить Timezone
    public function getTimezoneDescription()
    {
        switch ($this->timezone) {
            case '-12': return 'Etc/GMT-12:00';
            case '-11': return 'Etc/GMT-11:00';
            case '-10': return 'Etc/GMT-10:00';
            case '-9': return 'Etc/GMT-09:00';
            case '-8': return 'Etc/GMT-08:00';
            case '-7': return 'Etc/GMT-07:00';
            case '-6': return 'Etc/GMT-06:00';
            case '-5': return 'Etc/GMT-05:00';
            case '-4': return 'Etc/GMT-04:00';
            case '-3': return 'Etc/GMT-03:00';
            case '-2': return 'Etc/GMT-02:00';
            case '-1': return 'Etc/GMT-01:00';
            case '0': return 'Etc/GMT+00:00';
            case '1': return 'Etc/GMT+01:00';
            case '2': return 'Etc/GMT+02:00';
            case '3': return 'Etc/GMT+03:00';
            case '4': return 'Etc/GMT+04:00';
            case '5': return 'Etc/GMT+05:00';
            case '6': return 'Etc/GMT+06:00';
            case '7': return 'Etc/GMT+07:00';
            case '8': return 'Etc/GMT+08:00';
            case '9': return 'Etc/GMT+09:00';
            case '10': return 'Etc/GMT+10:00';
            case '11': return 'Etc/GMT+11:00';
            case '12': return 'Etc/GMT+12:00';
            case '13': return 'Etc/GMT+13:00';
            case '14': return 'Etc/GMT+14:00';
            default: return "Неизвестно";
        }
    }

    //Получить список Timezone
    public function getTimezoneList()
    {
        return [
            '-12' => 'Etc/GMT-12:00',
            '-11' => 'Etc/GMT-11:00',
            '-10' => 'Etc/GMT-10:00',
            '-9' => 'Etc/GMT-09:00',
            '-8' => 'Etc/GMT-08:00',
            '-7' => 'Etc/GMT-07:00',
            '-6' => 'Etc/GMT-06:00',
            '-5' => 'Etc/GMT-05:00',
            '-4' => 'Etc/GMT-04:00',
            '-3' => 'Etc/GMT-03:00',
            '-2' => 'Etc/GMT-02:00',
            '-1' => 'Etc/GMT-01:00',
            '0' => 'Etc/GMT+00:00',
            '1' => 'Etc/GMT+01:00',
            '2' => 'Etc/GMT+02:00',
            '3' => 'Etc/GMT+03:00',
            '4' => 'Etc/GMT+04:00',
            '5' => 'Etc/GMT+05:00',
            '6' => 'Etc/GMT+06:00',
            '7' => 'Etc/GMT+07:00',
            '8' => 'Etc/GMT+08:00',
            '9' => 'Etc/GMT+09:00',
            '10' => 'Etc/GMT+10:00',
            '11' => 'Etc/GMT+11:00',
            '12' => 'Etc/GMT+12:00',
            '13' => 'Etc/GMT+13:00',
            '14' => 'Etc/GMT+14:00',
        ];
    }

    //Получить валюту
    public function getCurrencyDescription()
    {
        switch ($this->currency) {
            case '1': return 'RUB - Российский рубль';
            case '2': return 'USD - Доллар США';
            case '3': return 'EUR - Евро';
            case '4': return 'KZT - Казахский тенге';
            case '5': return 'UAH - Украинская гривна';
            case '6': return 'BYN - Белорусский рубль';
            case '7': return 'THB - Тайский бат';
            case '8': return 'KGS - Киргизский сом';
            default: return "Неизвестно";
        }
    }

    //Получить лист валют
    public function getCurrencyList()
    {
        return [
            '1' => 'RUB - Российский рубль',
            '2' => 'USD - Доллар США',
            '3' => 'EUR - Евро',
            '4' => 'KZT - Казахский тенге',
            '5' => 'UAH - Украинская гривна',
            '6' => 'BYN - Белорусский рубль',
            '7' => 'THB - Тайский бат',
            '8' => 'KGS - Киргизский сом',
        ];
    }

    //Получить знаки валют.
    public function getCurrencyFont()
    {
        switch ($this->currency) {
            case '1': return '&#8381;';
            case '2': return '&#36;';
            case '3': return '&#8364;';
            case '4': return '&#8376;';
            case '5': return '&#8372;';
            case '6': return 'BYN';
            case '7': return '&#3647;';
            case '8': return 'KGS';
            default: return "Неизвестно";
        }
    }

    //Отправить сообщение на э-майл
    public function sendMessageToEmail($email, $message, $title)
    {
        $message = \Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['email'])
            ->setTo($email)
            ->setSubject($title)
            ->setHtmlBody($message);

        try{
                Yii::$app->mailer->send($message);
           } catch(\Swift_TransportException $e) {
                Yii::$app->session->setFlash('warning', 'Сообщение не был отправлен. Ошибка: '.$e->getMessage());
           } catch(\Exception $e){
                Yii::$app->session->setFlash('error', 'Сообщение не был отправлен. Ошибка: '.$e->getMessage());
           }
    }
}
