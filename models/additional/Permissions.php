<?php

namespace app\models\additional;

use Yii;
use app\models\Users;

/**
 * This is the model class for table "permissions".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $view_user Возможность просматривать информацию о пользователях
 * @property int $delete_user Возможность удалять пользователей
 * @property int $create_user Возможность создавать пользователей
 * @property int $change_tariff Возможность вручную менять тарифный план пользователя и его срок действия
 * @property int $view_page Возможность просматривать страницы
 * @property int $change_content_page Редактирование контентных страниц
 * @property int $change_price_list Редактирование цен на тарифы
 * @property int $answer_to_questions Ответы на сообщения пользователей
 * @property int $change_promocode Изменение срока действия промо-периода для новых пользователей
 * @property int $close_window Отключать у отдельных пользователей всплывающее окно при переходе на страницу
 * @property int $free_period Включение/отключение бесплатного периода использования платного тарифа при регистрации по реферальной ссылке или коду партнерской программы
 * @property int $execute_order Исполнять заявки по выплатам в реферальной программе
 * @property int $unlock_registration Разблокировать для регистрации имя пользователя
 *
 * @property Users $user
 */
class Permissions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permissions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'view_user', 'delete_user', 'create_user', 'change_tariff', 'view_page', 'change_content_page', 'change_price_list', 'answer_to_questions', 'change_promocode', 'close_window', 'free_period', 'execute_order', 'unlock_registration'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'view_user' => 'Возможность просматривать информацию о пользователях',
            'delete_user' => 'Возможность удалять пользователей',
            'create_user' => 'Возможность создавать пользователей',
            'change_tariff' => 'Возможность вручную менять тарифный план пользователя и его срок действия',
            'view_page' => 'Возможность просматривать страницы',
            'change_content_page' => 'Редактирование контентных страниц',
            'change_price_list' => 'Редактирование цен на тарифы',
            'answer_to_questions' => 'Ответы на сообщения пользователей',
            'change_promocode' => 'Изменение срока действия промо-периода для новых пользователей',
            'close_window' => 'Отключать у отдельных пользователей всплывающее окно при переходе на страницу',
            'free_period' => 'Включение/отключение бесплатного периода использования платного тарифа при регистрации по реферальной ссылке или коду партнерской программы',
            'execute_order' => 'Исполнять заявки по выплатам в реферальной программе',
            'unlock_registration' => 'Разблокировать для регистрации имя пользователя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * Добавление первичных значений
     */
    public function setDefaultValues($user_id)
    {
        $permission = new Permissions();
        $permission->user_id = $user_id;
        $permission->save();
    }
}
