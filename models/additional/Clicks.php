<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "clicks".
 *
 * @property int $id
 * @property string $table_name
 * @property int $field_id
 * @property string $name
 * @property string $date
 */
class Clicks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clicks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['field_id'], 'integer'],
            [['date'], 'safe'],
            [['table_name'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Table Name',
            'field_id' => 'Field ID',
            'name' => 'Name',
            'date' => 'Date',
        ];
    }
}
