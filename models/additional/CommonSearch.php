<?php

namespace app\models\additional;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\additional\Common;

/**
 * CommonSearch represents the model behind the search form about `app\models\additional\Common`.
 */
class CommonSearch extends Common
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'timezone', 'pixel_id', 'currency'], 'integer'],
            [['html_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Common::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'timezone' => $this->timezone,
            'pixel_id' => $this->pixel_id,
            'currency' => $this->currency,
        ]);

        $query->andFilterWhere(['like', 'html_code', $this->html_code]);

        return $dataProvider;
    }
}
