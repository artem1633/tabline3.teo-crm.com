<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "write".
 *
 * @property int $id
 * @property string $sender_name Как к вам обращаться
 * @property string $sender_email Как с вами связаться, email
 * @property string $text Вопрос/предложение
 */
class Write extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'write';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sender_email'], 'email'],
            [['text'], 'string'],
            [['sender_email', 'sender_name'], 'required'],
            [['sender_name', 'sender_email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_name' => 'Как к вам обращаться',
            'sender_email' => 'Как с вами связаться, email',
            'text' => 'Вопрос/предложение',
        ];
    }
}
