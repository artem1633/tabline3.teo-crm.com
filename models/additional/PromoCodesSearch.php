<?php

namespace app\models\additional;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\additional\PromoCodes;

/**
 * PromoCodesSearch represents the model behind the search form about `app\models\additional\PromoCodes`.
 */
class PromoCodesSearch extends PromoCodes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'remaining_input', 'protsent', 'ended_input'], 'integer'],
            [['code', 'access_date', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoCodes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'access_date' => $this->access_date,
            'remaining_input' => $this->remaining_input,
            'protsent' => $this->protsent,
            'ended_input' => $this->ended_input,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
