<?php
namespace app\models\additional;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class UploadForm extends Model
{
    public $file;
    public $type;
    public function rules()
    {
        return [
            [['file'], 'file'],
            [['type',], 'integer'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',],
        ];
    }

    public function getType()
    {
        $group = manual\TypeParts::find()->all();
        return ArrayHelper::map($group, 'id', 'name');
    }
}
?>