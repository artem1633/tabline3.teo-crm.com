<?php

namespace app\models\additional;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "promo_codes".
 *
 * @property int $id
 * @property string $code Промокод
 * @property string $access_date Срок действия
 * @property int $remaining_input Количество доступных вводов
 * @property int $status Статус
 * @property int $protsent Процент скидки по данному промо-коду
 * @property int $ended_input Количество совершенных вводов
 */
class PromoCodes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_codes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['access_date'], 'safe'],
            [['code'], 'unique'],
            [['code', 'remaining_input', 'protsent'], 'required'],
            [['remaining_input', 'status', 'protsent', 'ended_input'], 'integer'],
            [['code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Промокод',
            'access_date' => 'Срок действия',
            'remaining_input' => 'Количество доступных вводов',
            'status' => 'Статус',
            'protsent' => 'Процент скидки %',
            'ended_input' => 'Количество совершенных вводов',
        ];
    }

    //Получить список статус
    public function getStatusList()
    {
        return [
            0 => 'Не активен',
            1 => 'Активен',
        ];
    }

    //Получить описание статуса
    public function getStatusDescription()
    {
        switch ($this->status) {
            case 0: return "Не активен";
            case 1: return "Активен";
            default: return "Неизвестно";
        }
    }
}
