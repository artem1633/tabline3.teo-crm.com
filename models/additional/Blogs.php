<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "blogs".
 *
 * @property int $id
 * @property string $title
 * @property string $date
 * @property string $text
 * @property string $image
 */
class Blogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blogs';
    }

    public $file;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['date'], 'safe'],
            [['text'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'date' => 'Дата',
            'text' => 'Текст',
            'image' => 'Фото',
            'file' => 'Фото',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date = date('Y-m-d');
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        if (file_exists('images/blog/'. $this->image)) {
            unlink(Yii::getAlias('images/blog/' .  $this->image));
        }
        return parent::beforeDelete();
    }

}
