<?php

namespace app\models\additional;

use Yii;
use app\models\Users;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "history_pages".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $date Дата
 * @property string $page Страница/Ссылка
 * @property string $status Статус
 *
 * @property Users $user
 */
class HistoryPages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history_pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['date'], 'safe'],
            [['page', 'status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'date' => 'Дата',
            'page' => 'Страница/Ссылка',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    //Получить описание статуса
    public function getStatusDescription()
    {
        switch ($this->status) {
            case 'create': return "Создано страница";
            case 'update': return "Изменено страница";
            case 'delete': return "Удалено страница";
            default: return "Неизвестно";
        }        
    }
}
