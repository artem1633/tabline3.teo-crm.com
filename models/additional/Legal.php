<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "legal".
 *
 * @property int $id
 * @property string $director ФИО директора
 * @property string $address Юридический адрес
 * @property string $mailing_address Почтовый адрес
 * @property string $state_registration Свидетельство о гос. регистрации
 * @property string $inn ИНН
 * @property string $ogrn ОГРН
 * @property string $r_s Р/с
 * @property string $bik Бик
 * @property string $kor_schot К/с
 */
class Legal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const FILE_NAME = 'Privacy_policy.html';
    const OFFER_FILE_NAME = 'offer.html';

    public $content;
    public $offer;

    public static function tableName()
    {
        return 'legal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'offer'], 'safe'],
            [['address', 'mailing_address'], 'string'],
            [['director', 'state_registration', 'inn', 'ogrn', 'r_s', 'bik', 'kor_schot'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'director' => 'ФИО директора',
            'address' => 'Юридический адрес',
            'mailing_address' => 'Почтовый адрес',
            'state_registration' => 'Свидетельство о гос. регистрации',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'r_s' => 'Р/с',
            'bik' => 'Бик',
            'kor_schot' => 'К/с',
        ];
    }

    /**
     * @return Политика конфиденциальности
     */
    public static function getPrivacy()
    {
        if(file_exists(self::FILE_NAME))
        {
            return new self(['content' => file_get_contents(self::FILE_NAME)]);
        }

        return new self();
    }

    /**
     * @return bool|int
     */
    public function savePrivacy()
    {
        return file_put_contents(self::FILE_NAME, $this->content);
    }

    /**
     * @return Договор оферта
     */
    public static function getOffer()
    {
        if(file_exists(self::OFFER_FILE_NAME))
        {
            return new self(['offer' => file_get_contents(self::OFFER_FILE_NAME)]);
        }

        return new self();
    }

    /**
     * @return bool|int
     */
    public function saveOffer()
    {
        return file_put_contents(self::OFFER_FILE_NAME, $this->offer);
    }
}
