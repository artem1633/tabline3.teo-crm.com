<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "goods_list".
 *
 * @property int $id
 * @property int $shopping_cart_id
 * @property int $product_id
 * @property int $count
 *
 * @property Products $product
 * @property ShoppingCart $shoppingCart
 */
class GoodsList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shopping_cart_id', 'product_id', 'count'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['shopping_cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\ShoppingCart::className(), 'targetAttribute' => ['shopping_cart_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shopping_cart_id' => 'Корзинка',
            'product_id' => 'Продукт',
            'count' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShoppingCart()
    {
        return $this->hasOne(\app\models\ShoppingCart::className(), ['id' => 'shopping_cart_id']);
    }
}
