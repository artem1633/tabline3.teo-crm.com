<?php

namespace app\models\additional;

use Yii;

/**
 * This is the model class for table "tariffs".
 *
 * @property int $id
 * @property string $name
 * @property string $key
 * @property string $price_list
 */
class Tariffs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tariffs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_list'], 'string'],
            [['name', 'key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование тарифа',
            'key' => 'Ключ',
            'price_list' => 'Цены и сроки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['tariff_id' => 'id']);
    }
}
