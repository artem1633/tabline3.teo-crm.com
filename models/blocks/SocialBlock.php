<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "social_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $display
 * @property int $facebook
 * @property string $facebook_link
 * @property string $facebook_title
 * @property int $instagram
 * @property string $instagram_link
 * @property string $instagram_title
 * @property int $vk
 * @property string $vk_link
 * @property string $vk_title
 * @property int $odno
 * @property string $odno_link
 * @property string $odno_title
 * @property int $twitter
 * @property string $twitter_link
 * @property string $twitter_title
 * @property int $youtube
 * @property string $youtube_link
 * @property string $youtube_title
 * @property int $pinterest
 * @property string $pinterest_link
 * @property string $pinterest_title
 * @property int $snapchat
 * @property string $snapchat_link
 * @property string $snapchat_title
 *
 * @property UsersPage $page
 */
class SocialBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'facebook', 'instagram', 'vk', 'odno', 'twitter', 'youtube', 'pinterest', 'snapchat', 'visible', 'visible_in_menu'], 'integer'],
            [['display', 'facebook_link', 'facebook_title', 'instagram_link', 'instagram_title', 'vk_link', 'vk_title', 'odno_link', 'odno_title', 'twitter_link', 'twitter_title', 'youtube_link', 'youtube_title', 'pinterest_link', 'pinterest_title', 'snapchat_link', 'snapchat_title', 'title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'display' => 'Отображения',
            'facebook' => 'Facebook',
            'facebook_link' => 'Ссылка',
            'facebook_title' => 'Заголовок',
            'instagram' => 'Instagram',
            'instagram_link' => 'Ссылка',
            'instagram_title' => 'Заголовок',
            'vk' => 'ВКонтакте',
            'vk_link' => 'Ссылка',
            'vk_title' => 'Заголовок',
            'odno' => 'Одноклассники',
            'odno_link' => 'Ссылка',
            'odno_title' => 'Заголовок',
            'twitter' => 'Twitter',
            'twitter_link' => 'Ссылка',
            'twitter_title' => 'Заголовок',
            'youtube' => 'Youtube',
            'youtube_link' => 'Ссылка',
            'youtube_title' => 'Заголовок',
            'pinterest' => 'Pinterest',
            'pinterest_link' => 'Ссылка',
            'pinterest_title' => 'Заголовок',
            'snapchat' => 'Snapchat',
            'snapchat_link' => 'Ссылка',
            'snapchat_title' => 'Заголовок',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить список Отображений.
    public function getDisplayList()
    {
        return [
            'with_text' => 'Ссылки на всю ширину с текстом',
            'without_text' => 'Компактные ссылки без текста',
            'without_design' => 'Ссылки на всю ширину без оформления',
            'with_design' => 'Круглые ссылки с оформлением',
        ];
    }

    //Получить данные блока
    public function getItemValues($items)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/social-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/social-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                .  $items . 
            '</div>';

        return $name;
    }

    //Ввод началных значении
    public function setDefaultValues()
    {
        $this->facebook_link = 'https://www.facebook.com/';
        $this->facebook_title = 'Facebook';
        $this->instagram_link = 'https://www.instagram.com/';
        $this->instagram_title = 'Instagram';
        $this->vk_link = 'https://vk.com/';
        $this->vk_title = 'ВКонтакте';
        $this->odno_link = 'https://www.odnoklassniki.ru/';
        $this->odno_title = 'Одноклассники';
        $this->twitter_link = 'https://twitter.com/';
        $this->twitter_title = 'Twitter';
        $this->youtube_link = 'https://www.youtube.com/';
        $this->youtube_title = 'Youtube';
        $this->pinterest_link = 'https://www.pinterest.com/';
        $this->pinterest_title = 'Pinterest';
        $this->snapchat_link = 'https://www.snapchat.com/';
        $this->snapchat_title = 'Snapchat';
        $this->visible = 1;
        $this->visible_in_menu = 1;
        $this->name_in_menu = 'Социальные сети';
    }
}
