<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\additional\Clicks;

/**
 * This is the model class for table "messenger_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $display
 * @property int $whatsapp
 * @property string $whatsapp_title
 * @property string $whatsapp_phone
 * @property string $whatsapp_text
 * @property int $telegram
 * @property string $telegram_title
 * @property string $telegram_user
 * @property int $facebook
 * @property string $facebook_title
 * @property string $facebook_user
 * @property int $vk
 * @property string $vk_title
 * @property string $vk_user
 * @property int $viber
 * @property string $viber_title
 * @property string $viber_phone
 * @property int $skype
 * @property string $skype_title
 * @property string $skype_user
 * @property int $phone
 * @property string $phone_number
 * @property int $email
 * @property string $email_title
 *
 * @property UsersPage $page
 */
class MessengerBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messenger_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'whatsapp', 'telegram', 'facebook', 'vk', 'viber', 'skype', 'phone', 'email', 'visible', 'visible_in_menu'], 'integer'],
            [['whatsapp_text'], 'string'],
            [['display'], 'string', 'max' => 50],
            [['whatsapp_title', 'whatsapp_phone', 'telegram_title', 'telegram_user', 'facebook_title', 'facebook_user', 'vk_title', 'vk_user', 'viber_title', 'viber_phone', 'skype_title', 'skype_user', 'phone_number', 'email_title', 'title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'display' => 'Отображения',
            'whatsapp' => 'WhatsApp',
            'whatsapp_title' => 'Заголовок ссылки',
            'whatsapp_phone' => 'Номер телефона WhatsApp',
            'whatsapp_text' => 'Текст шаблон сообщения',
            'telegram' => 'Telegram',
            'telegram_title' => 'Заголовок ссылки',
            'telegram_user' => 'Имя пользователя Telegram',
            'facebook' => 'Facebook',
            'facebook_title' => 'Заголовок ссылки',
            'facebook_user' => 'Имя пользователя Facebook',
            'vk' => 'ВКонтакте',
            'vk_title' => 'Заголовок ссылки',
            'vk_user' => 'Имя пользователя VK',
            'viber' => 'Viber',
            'viber_title' => 'Заголовок ссылки',
            'viber_phone' => 'Имя пользователя Viber',
            'skype' => 'Skype',
            'skype_title' => 'Заголовок ссылки',
            'skype_user' => 'Имя пользователя Skype',
            'phone' => 'Звонок',
            'phone_number' => 'Номер телефона',
            'email' => 'E-mail',
            'email_title' => 'Адрес электронной почты',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    public function afterDelete() 
    {
        parent::afterDelete();
        $clicks = Clicks::find()->where(['field_id' => $this->id, 'table_name' => 'messenger_block'])->all();
        foreach ($clicks as $value) {
            $value->delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить список отображений.
    public function getDisplayList()
    {
        return [
            'with_icons' => 'Иконки приложений',
            'without_design' => 'Ссылки на всю ширину без оформления',
            'with_design' => 'Ссылки на всю ширину с оформлением',
            'with_icons_design' => 'Компактные ссылки с оформлением',
            'with_link_design' => 'Круглые ссылки с оформлением',
        ];
    }

    //Получить данные блока
    public function getItemValues($items)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/messenger-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/messenger-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . $items .  
            '</div>';

        return $name;
    }
}
