<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\AvatarBlock;

/**
 * AvatarBlockSearch represents the model behind the search form about `app\models\blocks\AvatarBlock`.
 */
class AvatarBlockSearch extends AvatarBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['avatar_size', 'hide_avatar', 'title_text', 'name_in_menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AvatarBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'visible' => $this->visible,
            'visible_in_menu' => $this->visible_in_menu,
        ]);

        $query->andFilterWhere(['like', 'avatar_size', $this->avatar_size])
            ->andFilterWhere(['like', 'name_in_menu', $this->name_in_menu])
            ->andFilterWhere(['like', 'title_text', $this->title_text])
            ->andFilterWhere(['like', 'hide_avatar', $this->hide_avatar]);

        return $dataProvider;
    }
}
