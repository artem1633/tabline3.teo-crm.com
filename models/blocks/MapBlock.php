<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

/**
 * This is the model class for table "map_block".
 *
 * @property int $id
 * @property int $page_id Страница
 * @property int $center_x Координата X
 * @property int $center_y Координата Y
 * @property int $lock_map Зафиксировать карту
 * @property int $add_mark Добавить отдельные ссылки для каждой метки
 * @property int $show_select Показывать выбор карты
 * @property int $show_zoom Показывать опцию масштабирования
 * @property int $show_panorama Показывать интерфейс панорамы улиц
 *
 * @property UsersPage $page
 */
class MapBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'map_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'lock_map', 'add_mark', 'show_select', 'show_zoom', 'show_panorama', 'visible', 'visible_in_menu'], 'integer'],
            [['center_x', 'center_y', 'title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'center_x' => 'Координата X',
            'center_y' => 'Координата Y',
            'lock_map' => 'Зафиксировать карту',
            'add_mark' => 'Добавить отдельные ссылки для каждой метки',
            'show_select' => 'Показывать выбор карты и интерфейс панорамы улиц',
            'show_zoom' => 'Показывать опцию масштабирования',
            'show_panorama' => 'Показывать интерфейс панорамы улиц',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapMarks()
    {
        return $this->hasMany(MapMarks::className(), ['map_id' => 'id']);
    }

    //Получить данные блока
    public function getItemValues($map)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/map-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/map-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','data-pjax'=>'0'
                    ])
                . $map .  
            '</div>';

        return $name;
    }

    //Начальное значение x
    public function getCoordinateX()
    {
        return '55.7372';
    }

    //Начальное значение y
    public function getCoordinateY()
    {
        return '37.6066';
    }
}
