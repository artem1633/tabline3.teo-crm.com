<?php

namespace app\models\blocks;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\MessengerBlock;

/**
 * MessengerBlockSearch represents the model behind the search form about `app\models\blocks\MessengerBlock`.
 */
class MessengerBlockSearch extends MessengerBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['display', 'whatsapp', 'whatsapp_title', 'whatsapp_phone', 'whatsapp_text', 'telegram', 'telegram_title', 'telegram_user', 'facebook', 'facebook_title', 'facebook_user', 'vk', 'vk_title', 'vk_user', 'viber', 'viber_title', 'viber_phone', 'skype', 'skype_title', 'skype_user', 'phone', 'phone_number', 'email', 'email_title', 'title_text', 'name_in_menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MessengerBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'visible' => $this->visible,
            'visible_in_menu' => $this->visible_in_menu,
        ]);

        $query->andFilterWhere(['like', 'display', $this->display])
            ->andFilterWhere(['like', 'whatsapp', $this->whatsapp])
            ->andFilterWhere(['like', 'whatsapp_title', $this->whatsapp_title])
            ->andFilterWhere(['like', 'whatsapp_phone', $this->whatsapp_phone])
            ->andFilterWhere(['like', 'whatsapp_text', $this->whatsapp_text])
            ->andFilterWhere(['like', 'telegram', $this->telegram])
            ->andFilterWhere(['like', 'telegram_title', $this->telegram_title])
            ->andFilterWhere(['like', 'telegram_user', $this->telegram_user])
            ->andFilterWhere(['like', 'facebook', $this->facebook])
            ->andFilterWhere(['like', 'facebook_title', $this->facebook_title])
            ->andFilterWhere(['like', 'facebook_user', $this->facebook_user])
            ->andFilterWhere(['like', 'vk', $this->vk])
            ->andFilterWhere(['like', 'vk_title', $this->vk_title])
            ->andFilterWhere(['like', 'vk_user', $this->vk_user])
            ->andFilterWhere(['like', 'viber', $this->viber])
            ->andFilterWhere(['like', 'viber_title', $this->viber_title])
            ->andFilterWhere(['like', 'viber_phone', $this->viber_phone])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'skype_title', $this->skype_title])
            ->andFilterWhere(['like', 'skype_user', $this->skype_user])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'email_title', $this->email_title])
            ->andFilterWhere(['like', 'name_in_menu', $this->name_in_menu])
            ->andFilterWhere(['like', 'title_text', $this->title_text]);

        return $dataProvider;
    }
}
