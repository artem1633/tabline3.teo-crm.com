<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\UsersPage;

/**
 * This is the model class for table "contact_form_block".
 *
 * @property int $id
 * @property int $page_id
 * @property int $name
 * @property string $name_title
 * @property string $name_description
 * @property int $telephone
 * @property string $telephone_title
 * @property string $telephone_description
 * @property int $email
 * @property string $email_title
 * @property string $email_description
 * @property int $line
 * @property string $line_title
 * @property string $line_description
 * @property int $number
 * @property string $number_title
 * @property string $number_description
 * @property int $list
 * @property string $list_title
 * @property string $list_description
 * @property int $check
 * @property string $check_title
 * @property string $check_description
 * @property int $check_selected
 * @property int $select
 * @property string $select_title
 * @property string $select_description
 * @property int $text
 * @property string $text_title
 * @property string $text_description
 * @property int $country
 * @property string $country_title
 * @property string $country_description
 *
 * @property UsersPage $page
 */
class ContactFormBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_form_block';
    }
    public $type;
    public $list_datas;
    public $select_datas;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'name', 'telephone', 'email', 'line', 'number', 'list', 'check', 'check_selected', 'select', 'text', 'country', 'visible', 'visible_in_menu', 'name_required', 'telephone_required', 'email_required', 'line_required', 'number_required', 'list_required', 'check_required', 'select_required', 'text_required', 'country_required'], 'integer'],
            [['line_description', 'select_data', 'list_data'], 'string'],
            [['name_title', 'name_description', 'telephone_title', 'telephone_description', 'email_title', 'email_description', 'line_title', 'number_title', 'number_description', 'list_title', 'check_title', 'check_description', 'select_title', 'text_title', 'text_description', 'country_title', 'country_description', 'type', 'select_description', 'list_description', 'title_text', 'name_in_menu', 'button_text', 'action_click', 'sms', 'site', 'page'], 'string', 'max' => 255],
            [['button_color', 'button_background'], 'string', 'max' => 10],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['name_description'], 'required', 'when' => function() {
                   if($this->name_required) { return TRUE;}
                   else return FALSE;
            }],
            [['telephone_description'], 'required', 'when' => function() {
                   if($this->telephone_required) return TRUE;
                   else return FALSE;
            }],
            [['email_description'], 'required', 'when' => function() {
                   if($this->email_required) return TRUE;
                   else return FALSE;
            }],
            [['line_description'], 'required', 'when' => function() {
                   if($this->line_required) return TRUE;
                   else return FALSE;
            }],
            [['number_description'], 'required', 'when' => function() {
                   if($this->number_required) return TRUE;
                   else return FALSE;
            }],
            [['list_datas'], 'required', 'when' => function() {
                   if($this->list_required) return TRUE;
                   else return FALSE;
            }],
            [['check_description'], 'required', 'when' => function() {
                   if($this->check_required) return TRUE;
                   else return FALSE;
            }],
            [['select_description'], 'required', 'when' => function() {
                   if($this->select_required) return TRUE;
                   else return FALSE;
            }],
            [['text_description'], 'required', 'when' => function() {
                   if($this->text_required) return TRUE;
                   else return FALSE;
            }],
            [['country_description'], 'required', 'when' => function() {
                   if($this->country_required) return TRUE;
                   else return FALSE;
            }],

            ['sms', 'required', 'when' => function($model) { return $model->action_click == 'sms'; }, 'enableClientValidation' => false],
            ['site', 'required', 'when' => function($model) { return $model->action_click == 'site'; }, 'enableClientValidation' => false],
            ['page', 'required', 'when' => function($model) { return $model->action_click == 'page'; }, 'enableClientValidation' => false],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'name' => 'Имя',
            'name_title' => 'Название поля',
            'name_description' => $this->name_title,
            'telephone' => 'Телефон',
            'telephone_title' => 'Название поля',
            'telephone_description' => $this->telephone_title,
            'email' => 'E-mail',
            'email_title' => 'Название поля',
            'email_description' => $this->email_title,
            'line' => 'Строка',
            'line_title' => 'Название поля',
            'line_description' => $this->line_title,
            'number' => 'Число',
            'number_title' => 'Название поля',
            'number_description' => $this->number_title,
            'list' => 'Список',
            'list_title' => 'Название поля',
            'list_description' => $this->list_title,
            'list_data' => 'Варианты',
            'check' => 'Галочка',
            'check_title' => 'Название поля',
            'check_description' => $this->check_title,
            'check_selected' => 'Выбрано по умолчанию',
            'select' => 'Выбор',
            'select_title' => 'Название поля',
            'select_description' => $this->select_title,
            'select_data' => 'Варианты',
            'text' => 'Текстовое поле',
            'text_title' => 'Название поля',
            'text_description' => $this->text_title,
            'country' => 'Страна',
            'country_title' => 'Название поля',
            'country_description' => $this->country_title,
            'type' => 'Поля',
            'list_datas' => $this->list_title,
            'select_datas' => 'Варианты',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
            'name_required' => 'Обязательное поле',
            'telephone_required' => 'Обязательное поле',
            'email_required' => 'Обязательное поле',
            'line_required' => 'Обязательное поле',
            'number_required' => 'Обязательное поле',
            'list_required' => 'Обязательное поле',
            'check_required' => 'Обязательное поле',
            'select_required' => 'Обязательное поле',
            'text_required' => 'Обязательное поле',
            'country_required' => 'Обязательное поле',
            'button_text' => 'Текст на кнопке',
            'action_click' => 'Действие после заполнения формы',
            'sms' => 'Показать сообщение',
            'site' => 'Открыть сайт',
            'page' => 'Открыть страницу',
            'button_color' => 'Цвет текста кнопки',
            'button_background' => 'Цвет фона кнопки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить данные блока
    public function getItemValues($form)
    {
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/contact-form-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/contact-form-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br>' .$form .  
            '</div>';

        return $name;
    }

    //Получить список форм
    public function getList()
    {
        return [
            'name' => 'Имя',
            'telephone' => 'Телефон',
            'email' => 'E-mail',
            'line' => 'Строка',
            'number' => 'Число',
            'list' => 'Список',
            'check' => 'Галочка',
            'select' => 'Выбор',
            'text' => 'Текстовое поле',
            'country' => 'Страна',
        ];
    }

    //Получить список действий.
    public function getActionList()
    {
       return [
            'sms' => 'Показать сообщение',
            'site' => 'Открыть сайт',
            'page' => 'Открыть страницу',
        ];
    }

    public function getListDatas()
    {
        if($this->list_data != null && !is_array($this->list_data)) $list_datas = explode(',', $this->list_data);
        $result = [];
        foreach ($list_datas as $value) {
            $result [] = [
                'id' => $value,
                'title' => $value,
            ];            
        }
        return ArrayHelper::map($result,'id', 'title');
    }

    public function getSelectList()
    {
        if($this->select_data != null && !is_array($this->select_data)) $select_datas = explode(',', $this->select_data);
        $result = [];
        foreach ($select_datas as $value) {
            $result [] = [
                'id' => $value,
                'title' => $value,
            ];            
        }
        return ArrayHelper::map($result,'id', 'title');
    }

    //Получить список страниц пользователя.
    public function getUsersPages()
    {
        $page = UsersPage::find()->where(['id' => $this->page_id])->one();
        $pages = UsersPage::find()->where(['user_id' => $page->user_id])->all();
        return ArrayHelper::map($pages,'id', 'name');
    }

    //Ввод началных значении
    public function setDefaultValues($page_id)
    {
        $this->name = 0;
        $this->name_title = 'Имя';
        $this->name_description = 'Введите ваше имя';
        $this->telephone = 0;
        $this->telephone_title = 'Телефон';
        $this->telephone_description = 'Введите номер телефона';
        $this->email = 0;
        $this->email_title = 'E-mail';
        $this->email_description = 'Введите ваш email';
        $this->line = 0;
        $this->line_title = 'Строка';
        $this->line_description = 'Введите текст здесь';
        $this->number = 0;
        $this->number_title = 'Число';
        $this->number_description = 'Введите любое число';
        $this->list = 0;
        $this->list_title = 'Список';
        $this->list_description = 'Выбор из списка';
        $this->check = 0;
        $this->check_title = 'Галочка';
        $this->check_description = 'Описание поля';
        $this->check_selected = 0;
        $this->select = 0;
        $this->select_title = 'Выбор';
        $this->select_description = 'Описание поля';
        $this->text = 0;
        $this->text_title = 'Текстовое поле';
        $this->text_description = 'Поля для ввода текста';
        $this->country = 0;
        $this->country_title = 'Страна';
        $this->country_description = 'Выберите';
        $this->visible = 1;
        $this->visible_in_menu = 1;
        $this->name_in_menu = 'Форма контактов';
        $this->button_text = 'Отправить';
        $this->button_color = '#000000;';
        $this->button_background = '#337ab7';
        $this->sms = 'Спасибо за вашу заявку';
        $this->site = 'http://';
        $this->page = UsersPage::findOne($page_id)->link_name;
        $this->action_click = 'sms';
    }

    //Получить список стран
    public function getListCountries()
    {
        return [
            "Afghanistan" => "Afghanistan",
            "Aland Islands" => "Aland Islands",
            "Albania" => "Albania",
            "Algeria" => "Algeria",
            "American Samoa" => "American Samoa",
            "Andorra" => "Andorra",
            "Angola" => "Angola",
            "Anguilla" => "Anguilla",
            "Antarctica" => "Antarctica",
            "Antigua and Barbuda" => "Antigua and Barbuda",
            "Argentina" => "Argentina",
            "Armenia" => "Armenia",
            "Aruba" => "Aruba",
            "Australia" => "Australia",
            "Austria" => "Austria",
            "Azerbaijan" => "Azerbaijan",
            "Bahamas" => "Bahamas",
            "Bahrain" => "Bahrain",
            "Bangladesh" => "Bangladesh",
            "Barbados" => "Barbados",
            "Belarus" => "Belarus",
            "Belgium" => "Belgium",
            "Belize" => "Belize",
            "Benin" => "Benin",
            "Bermuda" => "Bermuda",
            "Bhutan" => "Bhutan",
            "Bolivia, Plurinational State of" => "Bolivia, Plurinational State of",
            "Bonaire, Sint Eustatius and Saba" => "Bonaire, Sint Eustatius and Saba",
            "Bosnia and Herzegovina" => "Bosnia and Herzegovina",
            "Botswana" => "Botswana",
            "Bouvet Island" => "Bouvet Island",
            "Brazil" => "Brazil",
            "British Indian Ocean Territory" => "British Indian Ocean Territory",
            "Brunei Darussalam" => "Brunei Darussalam",
            "Bulgaria" => "Bulgaria",
            "Burkina Faso" => "Burkina Faso",
            "Burundi" => "Burundi",
            "Cambodia" => "Cambodia",
            "Cameroon" => "Cameroon",
            "Canada" => "Canada",
            "Cape Verde" => "Cape Verde",
            "Cayman Islands" => "Cayman Islands",
            "Central African Republic" => "Central African Republic",
            "Chad" => "Chad",
            "Chile" => "Chile",
            "China" => "China",
            "Christmas Island" => "Christmas Island",
            "Cocos (Keeling) Islands" => "Cocos (Keeling) Islands",
            "Colombia" => "Colombia",
            "Comoros" => "Comoros",
            "Congo" => "Congo",
            "Congo, the Democratic Republic of the" => "Congo, the Democratic Republic of the",
            "Cook Islands" => "Cook Islands",
            "Costa Rica" => "Costa Rica",
            "Côte d'Ivoire" => "Côte d'Ivoire",
            "Croatia" => "Croatia",
            "Cuba" => "Cuba",
            "Curaçao" => "Curaçao",
            "Cyprus" => "Cyprus",
            "Czech Republic" => "Czech Republic",
            "Denmark" => "Denmark",
            "Djibouti" => "Djibouti",
            "Dominica" => "Dominica",
            "Dominican Republic" => "Dominican Republic",
            "Ecuador" => "Ecuador",
            "Egypt" => "Egypt",
            "El Salvador" => "El Salvador",
            "Equatorial Guinea" => "Equatorial Guinea",
            "Eritrea" => "Eritrea",
            "Estonia" => "Estonia",
            "Ethiopia" => "Ethiopia",
            "Falkland Islands (Malvinas)" => "Falkland Islands (Malvinas)",
            "Faroe Islands" => "Faroe Islands",
            "Fiji" => "Fiji",
            "Finland" => "Finland",
            "France" => "France",
            "French Guiana" => "French Guiana",
            "French Polynesia" => "French Polynesia",
            "French Southern Territories" => "French Southern Territories",
            "Gabon" => "Gabon",
            "Gambia" => "Gambia",
            "Georgia" => "Georgia",
            "Germany" => "Germany",
            "Ghana" => "Ghana",
            "Gibraltar" => "Gibraltar",
            "Greece" => "Greece",
            "Greenland" => "Greenland",
            "Grenada" => "Grenada",
            "Guadeloupe" => "Guadeloupe",
            "Guam" => "Guam",
            "Guatemala" => "Guatemala",
            "Guernsey" => "Guernsey",
            "Guinea" => "Guinea",
            "Guinea-Bissau" => "Guinea-Bissau",
            "Guyana" => "Guyana",
            "Haiti" => "Haiti",
            "Heard Island and McDonald Islands" => "Heard Island and McDonald Islands",
            "Holy See (Vatican City State)" => "Holy See (Vatican City State)",
            "Honduras" => "Honduras",
            "Hong Kong" => "Hong Kong",
            "Hungary" => "Hungary",
            "Iceland" => "Iceland",
            "India" => "India",
            "Indonesia" => "Indonesia",
            "Iran, Islamic Republic of" => "Iran, Islamic Republic of",
            "Iraq" => "Iraq",
            "Ireland" => "Ireland",
            "Isle of Man" => "Isle of Man",
            "Israel" => "Israel",
            "Italy" => "Italy",
            "Jamaica" => "Jamaica",
            "Japan" => "Japan",
            "Jersey" => "Jersey",
            "Jordan" => "Jordan",
            "Kazakhstan" => "Kazakhstan",
            "Kenya" => "Kenya",
            "Kiribati" => "Kiribati",
            "Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of",
            "Korea, Republic of" => "Korea, Republic of",
            "Kuwait" => "Kuwait",
            "Kyrgyzstan" => "Kyrgyzstan",
            "Lao People's Democratic Republic" => "Lao People's Democratic Republic",
            "Latvia" => "Latvia",
            "Lebanon" => "Lebanon",
            "Lesotho" => "Lesotho",
            "Liberia" => "Liberia",
            "Libya" => "Libya",
            "Liechtenstein" => "Liechtenstein",
            "Lithuania" => "Lithuania",
            "Luxembourg" => "Luxembourg",
            "Macao" => "Macao",
            "Macedonia, the former Yugoslav Republic of" => "Macedonia, the former Yugoslav Republic of",
            "Madagascar" => "Madagascar",
            "Malawi" => "Malawi",
            "Malaysia" => "Malaysia",
            "Maldives" => "Maldives",
            "Mali" => "Mali",
            "Malta" => "Malta",
            "Marshall Islands" => "Marshall Islands",
            "Martinique" => "Martinique",
            "Mauritania" => "Mauritania",
            "Mauritius" => "Mauritius",
            "Mayotte" => "Mayotte",
            "Mexico" => "Mexico",
            "Micronesia, Federated States of" => "Micronesia, Federated States of",
            "Moldova, Republic of" => "Moldova, Republic of",
            "Monaco" => "Monaco",
            "Mongolia" => "Mongolia",
            "Montenegro" => "Montenegro",
            "Montserrat" => "Montserrat",
            "Morocco" => "Morocco",
            "Mozambique" => "Mozambique",
            "Myanmar" => "Myanmar",
            "Namibia" => "Namibia",
            "Nauru" => "Nauru",
            "Nepal" => "Nepal",
            "Netherlands" => "Netherlands",
            "New Caledonia" => "New Caledonia",
            "New Zealand" => "New Zealand",
            "Nicaragua" => "Nicaragua",
            "Niger" => "Niger",
            "Nigeria", 'title' => "Nigeria",
            "Niue" => "Niue",
            "Norfolk Island" => "Norfolk Island",
            "Northern Mariana Islands" => "Northern Mariana Islands",
            "Norway" => "Norway",
            "Oman" => "Oman",
            "Pakistan" => "Pakistan",
            "Palau" => "Palau",
            "Palestinian Territory, Occupied" => "Palestinian Territory, Occupied",
            "Panama" => "Panama",
            "Papua New Guinea" => "Papua New Guinea",
            "Paraguay" => "Paraguay",
            "Peru" => "Peru",
            "Philippines" => "Philippines",
            "Pitcairn" => "Pitcairn",
            "Poland" => "Poland",
            "Portugal" => "Portugal",
            "Puerto Rico" => "Puerto Rico",
            "Qatar" => "Qatar",
            "Réunion" => "Réunion",
            "Romania" => "Romania",
            "Russian Federation" => "Russian Federation",
            "Rwanda" => "Rwanda",
            "Saint Barthélemy" => "Saint Barthélemy",
            "Saint Helena, Ascension and Tristan da Cunha" => "Saint Helena, Ascension and Tristan da Cunha",
            "Saint Kitts and Nevis" => "Saint Kitts and Nevis",
            "Saint Lucia" => "Saint Lucia",
            "Saint Martin (French part)" => "Saint Martin (French part)",
            "Saint Pierre and Miquelon" => "Saint Pierre and Miquelon",
            "Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines",
            "Samoa" => "Samoa",
            "San Marino" => "San Marino",
            "Sao Tome and Principe" => "Sao Tome and Principe",
            "Saudi Arabia" => "Saudi Arabia",
            "Senegal" => "Senegal",
            "Serbia" => "Serbia",
            "Seychelles" => "Seychelles",
            "Sierra Leone" => "Sierra Leone",
            "Singapore" => "Singapore",
            "Sint Maarten (Dutch part)" => "Sint Maarten (Dutch part)",
            "Slovakia" => "Slovakia",
            "Slovenia" => "Slovenia",
            "Solomon Islands" => "Solomon Islands",
            "Somalia" => "Somalia",
            "South Africa" => "South Africa",
            "South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands",
            "South Sudan" => "South Sudan",
            "Spain" => "Spain",
            "Sri Lanka" => "Sri Lanka",
            "Sudan" => "Sudan",
            "Suriname" => "Suriname",
            "Svalbard and Jan Mayen" => "Svalbard and Jan Mayen",
            "Swaziland" => "Swaziland",
            "Sweden" => "Sweden",
            "Switzerland" => "Switzerland",
            "Syrian Arab Republic" => "Syrian Arab Republic",
            "Taiwan, Province of China" => "Taiwan, Province of China",
            "Tajikistan" => "Tajikistan",
            "Tanzania, United Republic of" => "Tanzania, United Republic of",
            "Thailand" => "Thailand",
            "Timor-Leste" => "Timor-Leste",
            "Togo" => "Togo",
            "Tokelau" => "Tokelau",
            "Tonga" => "Tonga",
            "Trinidad and Tobago" => "Trinidad and Tobago",
            "Tunisia" => "Tunisia",
            "Turkey" => "Turkey",
            "Turkmenistan" => "Turkmenistan",
            "Turks and Caicos Islands" => "Turks and Caicos Islands",
            "Tuvalu" => "Tuvalu",
            "Uganda" => "Uganda",
            "Ukraine" => "Ukraine",
            "United Arab Emirates" => "United Arab Emirates",
            "United Kingdom" => "United Kingdom",
            "United States" => "United States",
            "United States Minor Outlying Islands" => "United States Minor Outlying Islands",
            "Uruguay" => "Uruguay",
            "Uzbekistan" => "Uzbekistan",
            "Vanuatu" => "Vanuatu",
            "Venezuela, Bolivarian Republic of" => "Venezuela, Bolivarian Republic of",
            "Viet Nam" => "Viet Nam",
            "Virgin Islands, British" => "Virgin Islands, British",
            "Virgin Islands, U.S." => "Virgin Islands, U.S.",
            "Wallis and Futuna" => "Wallis and Futuna",
            "Western Sahara" => "Western Sahara",
            "Yemen" => "Yemen",
            "Zambia" => "Zambia",
            "Zimbabwe" => "Zimbabwe",
        ];

    }
}
