<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "question_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $question_title
 * @property string $answer
 *
 * @property UsersPage $page
 */
class QuestionBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'number', 'visible', 'visible_in_menu'], 'integer'],
            [['question_title', 'answer'], 'string'],
            [['title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'question_title' => 'Вопрос',
            'answer' => 'Ответ',
            'number' => 'Номер',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'question_block'")
            ->one();
        if($res){
            return $res["AUTO_INCREMENT"];
        }
    }

    //Получить список действии
    public function getItemValues($questions)
    {
        $tekst = 'Вопросы и ответы';
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/question-block/remove','number'=> $this->number ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/question-block/update', 'number'=>$this->number, 'page_id'=>$this->page_id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br>' .$questions .  
            '</div>';

        return $name;
    }
}
