<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "separator_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $separator_size
 *
 * @property UsersPage $page
 */
class SeparatorBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'separator_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],
            [['separator_size'], 'string', 'max' => 50],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'separator_size' => 'Размер разделителя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить список размеров
    public function getAlignmentSize()
    {
        return [
            'small' => 'Маленький',
            'normal' => 'Средний',
            'large' => 'Большой',
        ];
    }

    //Получить описение размера
    public function getAlignmentSizeDescription()
    {
        switch ($this->separator_size) {
            case 'small': return "30px";
            case 'normal': return "60px";
            case 'large': return "90px";
            default: return "Неизвестно";
        }
    }

    //Получить данные блока
    public function getItemValues()
    {
        $name = '
            <div class="grid-item">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/separator-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/separator-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . '<br><div style="display:flex; align-items: center; flex-direction: column; justify-content: center; height:'.$this->getAlignmentSizeDescription().' ">_____________________</div>' .  
            '</div>';

        return $name;
    }
}
