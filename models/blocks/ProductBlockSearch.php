<?php

namespace app\models\blocks;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\ProductBlock;

/**
 * ProductBlockSearch represents the model behind the search form about `app\models\blocks\ProductBlock`.
 */
class ProductBlockSearch extends ProductBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'number', 'product_id', 'contact_form_id'], 'integer'],
            [['view', 'checkbox', 'foto_visible', 'name_visible', 'description_visible', 'price_visible', 'contact_form_visible', 'button_visible', 'visible', 'title_text', 'visible_in_menu', 'name_in_menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'number' => $this->number,
            'product_id' => $this->product_id,
            'contact_form_id' => $this->contact_form_id,
        ]);

        $query->andFilterWhere(['like', 'view', $this->view])
            ->andFilterWhere(['like', 'checkbox', $this->checkbox])
            ->andFilterWhere(['like', 'foto_visible', $this->foto_visible])
            ->andFilterWhere(['like', 'name_visible', $this->name_visible])
            ->andFilterWhere(['like', 'description_visible', $this->description_visible])
            ->andFilterWhere(['like', 'price_visible', $this->price_visible])
            ->andFilterWhere(['like', 'contact_form_visible', $this->contact_form_visible])
            ->andFilterWhere(['like', 'button_visible', $this->button_visible])
            ->andFilterWhere(['like', 'visible', $this->visible])
            ->andFilterWhere(['like', 'title_text', $this->title_text])
            ->andFilterWhere(['like', 'visible_in_menu', $this->visible_in_menu])
            ->andFilterWhere(['like', 'name_in_menu', $this->name_in_menu]);

        return $dataProvider;
    }
}
