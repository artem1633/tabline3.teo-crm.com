<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "avatar_block".
 *
 * @property int $id
 * @property int $page_id
 * @property string $avatar_size
 * @property int $hide_avatar
 *
 * @property UsersPage $page
 */
class AvatarBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'avatar_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'hide_avatar', 'visible', 'visible_in_menu'], 'integer'],
            [['avatar_size'], 'string', 'max' => 50],
            [['title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'avatar_size' => 'Размер аватара',
            'hide_avatar' => 'Скрыть имя профиля под аватаром',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить список размеров
    public function getSizeList()
    {
        return [
            'small' => 'Маленький (65х65px)',
            'normal' => 'Средний (95х95px)',
            'big' => 'Большой (125х125px)',
            'very_big' => 'Очень большой (150х150px)',
        ];
    }

    //Получить описания размера
    public function getSizeDescription()
    {
        switch ($this->avatar_size) {
            case 'small': return "65";
            case 'normal': return "95";
            case 'big': return "125";
            case 'very_big': return "150";
            default: return "Неизвестно";
        }
    }

    //Получить данные блока
    public function getItemValues()
    {
        $size = $this->getSizeDescription();

        if (!file_exists('avatars/'.Yii::$app->user->identity->avatar) || Yii::$app->user->identity->avatar == '') {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
        } else {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->avatar;
        }

        if($this->hide_avatar) $user_name='';
        else $user_name = '<center>'.Yii::$app->user->identity->fio.'</center>';

        $name = '
            <div class="grid-item text-danger" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/avatar-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/avatar-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ]). 
            '</div>' 
            .   Html::img($path, [
                'style' => 'width:'.$size.'px; height:'.$size.'px;',
                'class' => 'img-circle',
            ]) . $user_name;

        return $name;
    }
}
