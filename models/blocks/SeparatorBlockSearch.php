<?php

namespace app\models\blocks;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\SeparatorBlock;

/**
 * SeparatorBlockSearch represents the model behind the search form about `app\models\blocks\SeparatorBlock`.
 */
class SeparatorBlockSearch extends SeparatorBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id'], 'integer'],
            [['separator_size'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeparatorBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
        ]);

        $query->andFilterWhere(['like', 'separator_size', $this->separator_size]);

        return $dataProvider;
    }
}
