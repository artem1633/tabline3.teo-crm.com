<?php

namespace app\models\blocks;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\SocialBlock;

/**
 * SocialBlockSearch represents the model behind the search form about `app\models\blocks\SocialBlock`.
 */
class SocialBlockSearch extends SocialBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['display', 'facebook', 'facebook_link', 'facebook_title', 'instagram', 'instagram_link', 'instagram_title', 'vk', 'vk_link', 'vk_title', 'odno', 'odno_link', 'odno_title', 'twitter', 'twitter_link', 'twitter_title', 'youtube', 'youtube_link', 'youtube_title', 'pinterest', 'pinterest_link', 'pinterest_title', 'snapchat', 'snapchat_link', 'snapchat_title', 'title_text', 'name_in_menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SocialBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'visible' => $this->visible,
            'visible_in_menu' => $this->visible_in_menu,
        ]);

        $query->andFilterWhere(['like', 'display', $this->display])
            ->andFilterWhere(['like', 'facebook', $this->facebook])
            ->andFilterWhere(['like', 'facebook_link', $this->facebook_link])
            ->andFilterWhere(['like', 'facebook_title', $this->facebook_title])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'instagram_link', $this->instagram_link])
            ->andFilterWhere(['like', 'instagram_title', $this->instagram_title])
            ->andFilterWhere(['like', 'vk', $this->vk])
            ->andFilterWhere(['like', 'vk_link', $this->vk_link])
            ->andFilterWhere(['like', 'vk_title', $this->vk_title])
            ->andFilterWhere(['like', 'odno', $this->odno])
            ->andFilterWhere(['like', 'odno_link', $this->odno_link])
            ->andFilterWhere(['like', 'odno_title', $this->odno_title])
            ->andFilterWhere(['like', 'twitter', $this->twitter])
            ->andFilterWhere(['like', 'twitter_link', $this->twitter_link])
            ->andFilterWhere(['like', 'twitter_title', $this->twitter_title])
            ->andFilterWhere(['like', 'youtube', $this->youtube])
            ->andFilterWhere(['like', 'youtube_link', $this->youtube_link])
            ->andFilterWhere(['like', 'youtube_title', $this->youtube_title])
            ->andFilterWhere(['like', 'pinterest', $this->pinterest])
            ->andFilterWhere(['like', 'pinterest_link', $this->pinterest_link])
            ->andFilterWhere(['like', 'pinterest_title', $this->pinterest_title])
            ->andFilterWhere(['like', 'snapchat', $this->snapchat])
            ->andFilterWhere(['like', 'snapchat_link', $this->snapchat_link])
            ->andFilterWhere(['like', 'snapchat_title', $this->snapchat_title])
            ->andFilterWhere(['like', 'name_in_menu', $this->name_in_menu])
            ->andFilterWhere(['like', 'title_text', $this->title_text]);

        return $dataProvider;
    }
}
