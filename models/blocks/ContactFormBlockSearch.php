<?php

namespace app\models\blocks;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\blocks\ContactFormBlock;

/**
 * ContactFormBlockSearch represents the model behind the search form about `app\models\blocks\ContactFormBlock`.
 */
class ContactFormBlockSearch extends ContactFormBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['name', 'name_title', 'name_description', 'telephone', 'telephone_title', 'telephone_description', 'email', 'email_title', 'email_description', 'line', 'line_title', 'line_description', 'number', 'number_title', 'number_description', 'list', 'list_title', 'list_description', 'check', 'check_title', 'check_description', 'check_selected', 'select', 'select_title', 'select_description', 'text', 'text_title', 'text_description', 'country', 'country_title', 'country_description', 'list_data', 'select_data', 'title_text', 'name_in_menu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactFormBlock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_id' => $this->page_id,
            'visible' => $this->visible,
            'visible_in_menu' => $this->visible_in_menu,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_title', $this->name_title])
            ->andFilterWhere(['like', 'name_description', $this->name_description])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'telephone_title', $this->telephone_title])
            ->andFilterWhere(['like', 'telephone_description', $this->telephone_description])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'email_title', $this->email_title])
            ->andFilterWhere(['like', 'email_description', $this->email_description])
            ->andFilterWhere(['like', 'line', $this->line])
            ->andFilterWhere(['like', 'line_title', $this->line_title])
            ->andFilterWhere(['like', 'line_description', $this->line_description])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'number_title', $this->number_title])
            ->andFilterWhere(['like', 'number_description', $this->number_description])
            ->andFilterWhere(['like', 'list', $this->list])
            ->andFilterWhere(['like', 'list_title', $this->list_title])
            ->andFilterWhere(['like', 'list_description', $this->list_description])
            ->andFilterWhere(['like', 'list_data', $this->list_data])
            ->andFilterWhere(['like', 'check', $this->check])
            ->andFilterWhere(['like', 'check_title', $this->check_title])
            ->andFilterWhere(['like', 'check_description', $this->check_description])
            ->andFilterWhere(['like', 'check_selected', $this->check_selected])
            ->andFilterWhere(['like', 'select', $this->select])
            ->andFilterWhere(['like', 'select_title', $this->select_title])
            ->andFilterWhere(['like', 'select_description', $this->select_description])
            ->andFilterWhere(['like', 'select_data', $this->select_data])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'text_title', $this->text_title])
            ->andFilterWhere(['like', 'text_description', $this->text_description])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'country_title', $this->country_title])
            ->andFilterWhere(['like', 'country_description', $this->country_description])
            ->andFilterWhere(['like', 'name_in_menu', $this->name_in_menu])
            ->andFilterWhere(['like', 'title_text', $this->title_text]);

        return $dataProvider;
    }
}
