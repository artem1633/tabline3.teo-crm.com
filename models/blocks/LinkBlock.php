<?php

namespace app\models\blocks;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\additional\Clicks;

/**
 * This is the model class for table "link_block".
 *
 * @property int $id
 * @property int $page_id
 * @property int $sorting
 * @property string $caption
 * @property string $action_click
 * @property string $site_url
 * @property string $call
 * @property string $email
 * @property string $other_page_name
 *
 * @property UsersPage $page
 */
class LinkBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'link_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'visible', 'visible_in_menu'], 'integer'],
            [['caption'], 'required'],
            [['caption', 'site_url', 'call', 'email', 'other_page_name', 'title_text', 'name_in_menu'], 'string', 'max' => 255],
            [['action_click'], 'string', 'max' => 50],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],

            ['site_url', 'required', 'when' => function($model) { return $model->action_click == 'site_url'; }, 'enableClientValidation' => false],
            ['call', 'required', 'when' => function($model) { return $model->action_click == 'call'; }, 'enableClientValidation' => false],
            ['email', 'required', 'when' => function($model) { return $model->action_click == 'email'; }, 'enableClientValidation' => false],
            ['other_page_name', 'required', 'when' => function($model) { return $model->action_click == 'other_page_name'; }, 'enableClientValidation' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'caption' => 'Заголовок',
            'action_click' => 'Действие',
            'site_url' => 'Открыть сайт',
            'call' => 'Позвонить',
            'email' => 'Отправить эл. письмо',
            'other_page_name' => 'Открыть страницу',
            'visible' => 'Блок виден всем посетителям',
            'visible_in_menu' => 'Блок виден в меню',
            'title_text' => 'Заголовок',
            'name_in_menu' => 'Название блока в меню',
        ];
    }

    public function afterDelete() 
    {
        parent::afterDelete();
        $clicks = Clicks::find()->where(['field_id' => $this->id, 'table_name' => 'link_block'])->all();
        foreach ($clicks as $value) {
            $value->delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(\app\models\UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить данные действий.
    public function getActionList()
    {
        return [
            'site_url' => 'Открыть сайт',
            'call' => 'Позвонить',
            'email' => 'Отправить эл. письмо',
            'other_page_name' => 'Открыть страницу',
        ];
    }

    //Получить описание действий.
    public function getActionDescription()
    {
        switch ($this->action_click) {
            case 'site_url': return "Открыть сайт";
            case 'call': return "Позвонить";
            case 'email': return "Отправить эл. письмо";
            case 'other_page_name': return "Открыть страницу";
            default: return "Неизвестно";
        }
    }

    //Получить данные блока
    public function getItemValues()
    {
        switch ($this->action_click) {
            case 'site_url': $url = 'http://' . $this->site_url;
            case 'call': $url = 'http://' . $this->call;
            case 'email': $url = 'http://' . $this->email;
            case 'other_page_name': $url = 'http://' . $this->other_page_name;
            default: $url = '';
        }

        $tekst = Html::a( $this->caption, $url, [ 'style'=>'font-size:18px; font-weight:bold;','data-pjax'=>'0','target' => '_blank',]);
        $name = '
            <div class="grid-item" style="text-align:center;">
                '. 
                Html::a('<i class="glyphicon glyphicon-trash"></i>',
                    ['/link-block/remove','id'=> $this->id ],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 
                        'class'=>'pull-right',
                        'role'=>'modal-remote',
                        'data-confirm'=>false, 
                        'data-method'=>false,
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ])
                .' '.
                Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                    ['/link-block/update','id'=>$this->id],
                    [
                        'style'=>'font-size:10px;margin-left:4px;', 'class'=>'pull-right','role'=>'modal-remote'
                    ])
                . $tekst .  
            '</div>';

        return $name;
    }
}
