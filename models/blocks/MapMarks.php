<?php

namespace app\models\blocks;

use Yii;

/**
 * This is the model class for table "map_marks".
 *
 * @property int $id
 * @property int $map_id
 * @property string $coordinate_x
 * @property string $coordinate_y
 * @property string $title
 * @property string $description
 * @property string $color
 *
 * @property MapBlock $map
 */
class MapMarks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'map_marks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['map_id'], 'integer'],
            [['description'], 'string'],
            [['coordinate_x', 'coordinate_y', 'title'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 10],
            [['map_id'], 'exist', 'skipOnError' => true, 'targetClass' => MapBlock::className(), 'targetAttribute' => ['map_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'map_id' => 'Карта',
            'coordinate_x' => 'Координата X',
            'coordinate_y' => 'Координата Y',
            'title' => 'Адрес',
            'description' => 'Описание',
            'color' => 'Цвет метки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMap()
    {
        return $this->hasOne(MapBlock::className(), ['id' => 'map_id']);
    }
}
