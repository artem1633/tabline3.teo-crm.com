<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\additional\Columns;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property int $page_id Страница
 * @property string $place Место 
 * @property string $date Дата заявки
 * @property string $status Статус
 * @property int $account_number Номер счета 
 * @property double $budget Бюджет
 * @property string $pay Оплата
 * @property string $purpose_of_payment Назначение платежа
 * @property string $pay_type Тип оплаты
 * @property string $pay_date Дата оплаты
 * @property string $utm UTM-метки
 * @property string $name Имя
 * @property string $contacts Контакты
 * @property string $fields Полей
 *
 * @property UsersPage $page
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    public $search_status;
    public $search_page;
    public $search_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'account_number'], 'integer'],
            [['date', 'pay_date'], 'safe'],
            [['budget'], 'number'],
            [['contacts', 'fields'], 'string'],
            [['place', 'utm', 'name'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
            [['pay', 'purpose_of_payment', 'pay_type'], 'string', 'max' => 50],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Заявка',
            'page_id' => 'Страница',
            'place' => 'Место',
            'date' => 'Дата заявки',
            'status' => 'Статус',
            'account_number' => 'Номер счета',
            'budget' => 'Бюджет',
            'pay' => 'Оплата',
            'purpose_of_payment' => 'Назначение платежа',
            'pay_type' => 'Тип оплаты',
            'pay_date' => 'Дата оплаты',
            'utm' => 'UTM-метки',
            'name' => 'Имя',
            'contacts' => 'Контакты',
            'fields' => 'Поля',
            'search_name' => 'Имя',
            'search_page' => 'Страница',
            'search_status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(UsersPage::className(), ['id' => 'page_id']);
    }

    //Получить описание статуса
    public function getStatusDescription()
    {
        switch ($this->status) {
            case 'new':     return "Новая";
            case 'working': return "В работе";
            case 'ready':   return "Выполнена";
            case 'cancel':  return "Отменена";
            default:        return "Неизвестно";
        }
    }

    //Получить список статусов.
    public function getStatusList()
    {
        return [
            'new' => 'Новая',
            'working' => 'В работе',
            'ready' => 'Выполнена',
            'cancel' => 'Отменена',
        ];
    }

    //Получить список страниц пользователя
    public function getPageList()
    {
        $page = UsersPage::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        return ArrayHelper::map($page, 'id', 'name');
    }

    //Получить описание оплаты
    public function getPayDescription()
    {
        switch ($this->pay) {
            case 'payed':       return "Оплачен";
            case 'not_payed':   return "Не оплачен";
            default:            return "Неизвестно";
        }
    }

    //Получить список оплаты
    public function getPayList()
    {
        return [
            'payed' => 'Оплачен',
            'not_payed' => 'Не оплачен',
        ];
    }

    //Получить список тип оплат.
    public function getPayTypeList()
    {
        return [
            'cash' => 'Наличные',
            'electron_pay' => 'Электронный платеж',
        ];
    }

    //Получить описание типов оплаты.
    public function getPayTypeDescription()
    {
        switch ($this->pay_type) {
            case 'cash':            return "Наличные";
            case 'electron_pay':    return "Электронный платеж";
            default:                return "Неизвестно";
        }
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'applications'")
            ->one();
        if($res){
            return $res["AUTO_INCREMENT"];
        }
    }

    //Загрузить начальные значения.
    public function setDefaultValues($user_id)
    {
        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'id'; //Заявка
        $column->name = 'Заявка';
        $column->status = 1;
        $column->order_number = 1;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'status'; //Статус
        $column->name = 'Статус';
        $column->status = 1;
        $column->order_number = 2;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'page_id'; //Страница
        $column->name = 'Страница';
        $column->status = 1;
        $column->order_number = 3;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'name'; //Имя
        $column->name = 'Имя';
        $column->status = 1;
        $column->order_number = 4;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'place'; //Место
        $column->name = 'Место';
        $column->status = 1;
        $column->order_number = 5;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'pay'; //Оплата
        $column->name = 'Оплата';
        $column->status = 1;
        $column->order_number = 6;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'date'; //Дата заявки
        $column->name = 'Дата заявки';
        $column->status = 1;
        $column->order_number = 7;
        $column->save();



        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'account_number'; //Назначение платежа
        $column->name = 'Назначение платежа'; 
        $column->status = 0;
        $column->order_number = 1;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'purpose_of_payment'; //Номер счета
        $column->name = 'Номер счета'; 
        $column->status = 0;
        $column->order_number = 2;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'pay_type'; //Тип оплаты
        $column->name = 'Тип оплаты'; 
        $column->status = 0;
        $column->order_number = 3;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'pay_date'; //Дата оплаты
        $column->name = 'Дата оплаты'; 
        $column->status = 0;
        $column->order_number = 4;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'utm'; //UTM-метки
        $column->name = 'UTM-метки'; 
        $column->status = 0;
        $column->order_number = 5;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'contacts'; //Контакты
        $column->name = 'Контакты'; 
        $column->status = 0;
        $column->order_number = 6;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'fields'; //Поля
        $column->name = 'Поля'; 
        $column->status = 0;
        $column->order_number = 7;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'applications';
        $column->type = 'budget'; //Бюджет
        $column->name = 'Бюджет'; 
        $column->status = 0;
        $column->order_number = 8;
        $column->save();

    }

    //Получить активные колонки.
    public function getColumns()
    {
        $user_id = Yii::$app->user->identity->id;
        $active_columns = Columns::find()
            ->where([
                'user_id' => $user_id, 
                'status' => 1, 
                'table_name' => 'applications'
            ])
            ->orderBy([ 'order_number' => SORT_ASC ])
            ->all();
        $result = [];
        $result [] = [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ];
        foreach ($active_columns as $value) {

            if($value->type == 'id') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'id',
                    'content' => function($data){
                        return '№ ' . $data->id;
                    },
               ];
            }

            if($value->type == 'status') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'status',
                    'content' => function($data){
                        return $data->getStatusDescription();
                    },
               ]; 
            }

            if($value->type == 'page_id') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'page_id',
                    'content' => function($data){
                        return $data->page->name;
                    },
               ];
            }

            if($value->type == 'name') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'name',
               ];
            }

            if($value->type == 'place') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'place',
               ];
            }

            if($value->type == 'pay') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'pay',
                    'content' => function($data){
                        return $data->getPayDescription();
                    },
               ];
            }

            if($value->type == 'date') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'date',
                    'content' => function($data){
                        return date('H:i:s d.m.Y', strtotime($data->date) );
                    },
               ];
            }

            if($value->type == 'account_number') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'account_number',
                    'content' => function($data){
                        return '№ ' . $data->account_number;
                    },
               ];
            }

            if($value->type == 'purpose_of_payment') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'purpose_of_payment',
               ];
            }

            if($value->type == 'pay_type') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'pay_type',
                    'content' => function($data){
                        return $data->getPayTypeDescription();
                    },
               ];
            }

            if($value->type == 'pay_date') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'pay_date',
                    'content' => function($data){
                        return date('H:i:s d.m.Y', strtotime($data->pay_date) );
                    },
               ];
            }

            if($value->type == 'utm') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'utm',
               ];
            }

            if($value->type == 'contacts') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'contacts',
               ];
            }

            if($value->type == 'fields') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'fields',
               ];
            }

            if($value->type == 'budget') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'budget',
               ];
            }
        }

        $result [] = [            
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'vAlign'=>'middle',
            'template' => '{view} {delete}',
            'urlCreator' => function($action, $model, $key, $index) { 
                    return Url::to([$action,'id'=>$key]);
            },
            'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
            'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
            'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-toggle'=>'tooltip',
                'data-confirm-title'=>'Подтвердите действие',
                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
        ]; 

        return $result;
    }
}
