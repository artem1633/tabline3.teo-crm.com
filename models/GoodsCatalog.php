<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;

/**
 * This is the model class for table "goods_catalog".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 *
 * @property Users $user
 */
class GoodsCatalog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods_catalog';
    }
    public $products;
    public $goods_count;

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->id;
                },
            ],
        ];
    }

    public static function find()
    {
        if(Yii::$app->user->isGuest == false) {
            if(Yii::$app->user->identity->type == 3) {
                $user_id = Yii::$app->user->identity->id;
            }else{
                $user_id = null;
            }
        }else{
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
           'user_id' => $user_id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'hide'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Наименование',
            'hide' => 'Скрыть коллекцию из навигации в каталоге',
            'products' => 'Укажите товар чтобы добавить его в каталога',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['catalog_id' => 'id']);
    }

    //Получить список продуктов
    public function getProductsList()
    {
        $products = Products::find()->all();
        return ArrayHelper::map( $products , 'id', 'name');
    }
}
