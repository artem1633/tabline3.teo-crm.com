<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\behaviors\BlameableBehavior;
use app\models\additional\Columns;
use app\models\additional\Common;
use app\base\AppActiveQuery;
/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property double $last_price
 * @property int $catalog_id
 * @property double $weight
 * @property int $delivery
 * @property int $view_in_catalog
 * @property string $photos
 * @property string $status
 *
 * @property GoodsCatalog $catalog
 * @property Users $user
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    public $search_catalog;
    public $search_name;
    public $search_price;
    public $search_catalog_archive;
    public $search_name_archive;
    public $search_price_archive;

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->id;
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            if(Yii::$app->user->identity->type == 3) $user_id = Yii::$app->user->identity->id;
            else $user_id = null;
        } else {
            $user_id = null;
        }

        return new AppActiveQuery(get_called_class(), [
           'user_id' => $user_id,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', /*'catalog_id',*/ 'delivery', 'view_in_catalog'], 'integer'],
            [['date_cr', 'date_up'], 'safe'],
            [['name', 'price'], 'required'],
            [['description', 'photos'], 'string'],
            [['last_price', 'weight', 'price'], 'number', 'min' => 0],
            [['name', 'link'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
            //[['catalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => GoodsCatalog::className(), 'targetAttribute' => ['catalog_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['catalog_id', 'validateCatalog'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Название товара',
            'description' => 'Описание',
            'price' => 'Цена',
            'last_price' => 'Старая цена',
            'catalog_id' => 'Каталог',
            'weight' => 'Вес товара',
            'delivery' => 'Доставка',
            'view_in_catalog' => 'Товар отоброжается в каталоге',
            'photos' => 'Фотографии товара',
            'status' => 'Статус',
            'date_cr' => 'Создано',
            'date_up' => 'Изменено',
            'link' => 'Ссылка',
            'search_catalog' => 'Каталог',
            'search_name' => 'Название товара',
            'search_price' => 'Цена',
            'search_catalog_archive' => 'Каталог',
            'search_name_archive' => 'Название товара',
            'search_price_archive' => 'Цена',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date_cr = date('Y-m-d H:i:s');
        }
        $this->date_up = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }

    //Новый каталог
    public function validateCatalog($attribute, $params)
    {
        $catalog = GoodsCatalog::find()->where([ 'id' => $this->catalog_id ])->one();
        if (!isset($catalog)){
            
            $catalog = new GoodsCatalog();
            $catalog->name = $this->catalog_id;
            
            if ($catalog->save()){//находим новый каталог
                $this->catalog_id = $catalog->id;
            }else{
                $this->addError($attribute,"Не создана новый каталог товара");
            }
        }
    }   

    //Получить список каталог товаров.
    public function getCatalogList()
    {
        $catalog = GoodsCatalog::find()->all();
        return ArrayHelper::map( $catalog , 'id', 'name');
    }

    //Получить описание статуса.
    public function getStatusDescription()
    {
        switch ($this->status) {
            case 'active': return "Активно";
            case 'archive': return "Не активно";
            default: return "Неизвестно";
        }
    }

    //Получить статуса лист.
    public function getStatusList()
    {
        return [
            'active' => 'Активно',
            'archive' => 'Не активно',
        ];
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'products'")
            ->one();
        if($res) {
            return $res["AUTO_INCREMENT"];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(GoodsCatalog::className(), ['id' => 'catalog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductOptions()
    {
        return $this->hasMany(ProductOptions::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductBlocks()
    {
        return $this->hasMany(ProductBlock::className(), ['product_id' => 'id']);
    }

    //Загрузить начальные значения.
    public function setDefaultValues($user_id)
    {
        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'name'; //Название товара
        $column->name = 'Название товара';
        $column->status = 1;
        $column->order_number = 1;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'status'; //Статус
        $column->name = 'Статус';
        $column->status = 1;
        $column->order_number = 2;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'price'; //Цена
        $column->name = 'Цена';
        $column->status = 1;
        $column->order_number = 3;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'weight'; //Вес товара
        $column->name = 'Вес товара';
        $column->status = 1;
        $column->order_number = 4;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'catalog_id'; //Каталог
        $column->name = 'Каталог';
        $column->status = 1;
        $column->order_number = 5;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'date_cr'; //Создано
        $column->name = 'Создано';
        $column->status = 1;
        $column->order_number = 6;
        $column->save();




        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'description'; //Описание
        $column->name = 'Описание'; 
        $column->status = 0;
        $column->order_number = 1;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'delivery'; //Доставка
        $column->name = 'Доставка'; 
        $column->status = 0;
        $column->order_number = 2;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'view_in_catalog'; //Товар отоброжается в каталоге
        $column->name = 'Товар отоброжается в каталоге'; 
        $column->status = 0;
        $column->order_number = 3;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'date_up'; //Изменено
        $column->name = 'Изменено'; 
        $column->status = 0;
        $column->order_number = 4;
        $column->save();

        $column = new Columns();
        $column->user_id = $user_id;
        $column->table_name = 'products';
        $column->type = 'link'; //Ссылка
        $column->name = 'Ссылка'; 
        $column->status = 0;
        $column->order_number = 5;
        $column->save();
    }

    //Получить активные колонки.
    public function getColumns()
    {
        $user_id = Yii::$app->user->identity->id;
        $active_columns = Columns::find()->where(['user_id' => $user_id, 'status' => 1, 'table_name' => 'products'])->orderBy([ 'order_number' => SORT_ASC ])->all();
        $result = [];
        $result [] = [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ];
        foreach ($active_columns as $value) {
            if($value->type == 'status') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'status',
                    'content' => function($data){
                        return '<span class="label label-success">'.$data->getStatusDescription().'</span>';
                    }
               ]; 
            }

            if($value->type == 'name') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'name',
               ]; 
            }

            if($value->type == 'catalog_id') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'catalog_id',
                    'content' => function($data){
                        return '<span class="label label-default">'.$data->catalog->name.'</span>';
                    }
               ]; 
            }

            if($value->type == 'price') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'price',
                    'content' => function($data){
                        $common = Common::find()->where(['user_id' => $data->user_id])->one();
                        if($data->last_price != null) $last = '<s style="color:#b5b5b5!important;">' . $data->last_price . ' ' . $common->getCurrencyFont() . '</s> ';
                        else $last = '';
                        return '<b>' . $last . $data->price . ' ' . $common->getCurrencyFont() . '</b>';
                    }
               ]; 
            }

            if($value->type == 'weight') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'weight',
               ]; 
            }

            if($value->type == 'delivery') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'delivery',
                    'content' => function($data){
                        if($data->view_in_catalog == 1) return 'Да';
                        else return 'Нет';
                    }
               ]; 
            }

            if($value->type == 'date_cr') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'date_cr',
                    'content' => function($data){
                        $common = Common::find()->where(['user_id' => $data->user_id ])->one();
                        $date_cr = date('H:i:s d.m.Y', strtotime( ($common->timezone - 3) . ' hour' , strtotime ( $data->date_cr ) ) );
                        return $date_cr;
                    }
               ]; 
            }

            if($value->type == 'description') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'description',
               ]; 
            }

            if($value->type == 'view_in_catalog') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'view_in_catalog',
                    'content' => function($data){
                        if($data->view_in_catalog == 1) return 'Да';
                        else return 'Нет';
                    }
               ]; 
            }

            if($value->type == 'date_up') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'date_up',
                    'content' => function($data){
                        return date('H:i:s d.m.Y', strtotime($data->date_up) );
                    }
               ]; 
            }

            if($value->type == 'link') {
                $result [] = [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'link',
               ]; 
            }
        }

        $result [] = [            
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update} {delete}',
            'dropdown' => false,
            'vAlign'=>'middle',
            'urlCreator' => function($action, $model, $key, $index) { 
                    return Url::to([$action,'id'=>$key]);
            },
            'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
            'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
            'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-toggle'=>'tooltip',
                'data-confirm-title'=>'Подтвердите действие',
                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
        ]; 

        return $result;
    }

}
