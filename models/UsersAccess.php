<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_access".
 *
 * @property int $id
 * @property int $user_from
 * @property int $user_to
 * @property int $access_type Тип доступа
 * @property int $view_page Просмотр страниц
 * @property int $update_page Редактирование страницы
 * @property int $view_application Просмотр заявок
 * @property int $update_application Редактирование заявок
 * @property int $view_statics Просмотр статистики
 * @property int $update_settings Редактирование настроек
 *
 * @property Users $userFrom
 * @property Users $userTo
 */
class UsersAccess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_access';
    }
    public $user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_from', 'user_to', 'access_type', 'view_page', 'update_page', 'view_application', 'update_application', 'view_statics', 'update_settings'], 'integer'],
            [['user'], 'string', 'max' => 255],
            [['user_from'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_from' => 'id']],
            [['user_to'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_to' => 'id']],
            ['user', 'validateUserName'],
            [['user'], 'required', 'when' => function() {
                   if($this->isNewRecord) return TRUE;
                   else return FALSE;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_from' => 'Профиль',
            'user_to' => 'Аккаунт',
            'user' => 'Укажите имя профиля которому вы хотите открыть доступ',
            'access_type' => 'Тип доступа',
            'view_page' => 'Просматривать страницы',
            'update_page' => 'Редактировать страницы',
            'view_application' => 'Просмотривать заявки',
            'update_application' => 'Редактировать заявки',
            'view_statics' => 'Просматривать статистику',
            'update_settings' => 'Редактировать настройки',
        ];
    }

    public function beforeSave($insert)
    {
        $this->view_page = 1;
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserFrom()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_to']);
    }

    public function validateUserName($attribute)
    { 
        $user = $this->user;
        $users = Users::find()->where(['login' => $user])->one();
        if($users == null) {
            $this->addError($attribute, 'Укажите имя профиля которому вы хотите открыть доступ');
        }else{
            $this->user_to = $users->id;  
        }
    }
}
