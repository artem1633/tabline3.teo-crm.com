<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $avatar;
    public $user_id;
    public $social; //1-instagram; 2-facebook; 3-vk; 4-google account

    public $login;
    public $password;
    public $telephone;
    public $promocode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'login', 'password'], 'required'],
            [['telephone', 'avatar'], 'string'],
            [['password', 'promocode'], 'string', 'max' => 255],
            [['login'], 'email'],
            [['social'], 'integer'],
            [['user_id'], 'string', 'max' => 500],
            [['login'], 'unique', 'targetClass' => '\app\models\User'],
            [['telephone'], 'unique', 'targetClass' => '\app\models\User'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'avatar' => 'Аватар',
            'login' => 'Логин',
            'password' => 'Пароль',
            'telephone' => 'Телефон',
            'promocode' => 'Промокод'
        ];
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($type = 3)
    {
        if($this->validate() === false){
            return null;
        }

        $user = new Users();
        $user->fio = $this->fio;
        $user->login = $this->login;
        $user->telephone = $this->telephone;
        $user->password = $this->password;
        $user->type = $type;
        $user->avatar = $this->avatar;
        $user->promo_code = $this->promocode;

        switch ($this->social) {
            case 1: $user->instagram_user_id = $this->user_id;
            case 2: $user->facebook_user_id = $this->user_id;
            case 3: $user->vk_user_id = $this->user_id;
            case 4: $user->google_account_user_id = $this->user_id;
            default: "";
        }

        if($user->save()) {
            if($this->social != null) {
                $extension = '';
                $url = $user->avatar;
                for ($i = strlen($url) - 1; $i > -1 ; $i--) {
                    if($url[$i] == '.'){ $extension = $url[$i] . $extension ; break; }
                    $extension = $url[$i] . $extension ;
                }
                $img = 'avatars/'.$user->id.$extension;
                file_put_contents($img, file_get_contents($url));
                $user->avatar = $user->id.$extension;
            }

            $session = Yii::$app->session;
            if($session['relation'] != null) {
                $user->relation = $session['relation'];
            }else{
                $user->relation = $user->id;
            }

            $user->save();
            return $user;
        }

        return null;
    }

}