<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class Template
 */
class Template extends Model
{

    public function getColumnsTemplate($index, $name)
    {
        return '<div class="grid-item text-danger" style="font-weight:bold;margin-left:160px;font-size:16px;"> Столбец  # '.$index. ' '. $name . '</div>';
    }
}