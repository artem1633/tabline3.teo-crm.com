<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Applications;
use app\models\UsersPage;
use app\models\additional\Contacts;

/**
 * ApplicationsSearch represents the model behind the search form about `app\models\Applications`.
 */
class ApplicationsSearch extends Applications
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'account_number'], 'integer'],
            [['place', 'date', 'status', 'pay', 'purpose_of_payment', 'pay_type', 'pay_date', 'utm', 'name', 'contacts', 'fields'], 'safe'],
            [['budget'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id, $post)
    {
        $result = [];
        $pages = UsersPage::find()->where(['user_id' => $id])->all();
        foreach ($pages as $value) {
            $result [] = $value->id;
        }

        if(Yii::$app->user->identity->type != 3 ) $query = Applications::find();
        else $query = Applications::find()->where(['page_id' => array_unique($result) ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'page_id' => $this->page_id,
            'date' => $this->date,
            'account_number' => $this->account_number,
            'budget' => $this->budget,
            'pay_date' => $this->pay_date,
        ]);

        if(isset($post['ApplicationsSearch']['search_status'])) $query->andFilterWhere(['like', 'status', $this->place]);
        if(isset($post['ApplicationsSearch']['search_page'])) $query->andFilterWhere(['like', 'page_id', $this->place]);
        if(isset($post['ApplicationsSearch']['search_name'])) $query->andFilterWhere(['like', 'name', $this->place]);

        $query->andFilterWhere(['like', 'place', $this->place])
            //->andFilterWhere(['like', 'status', $post['ApplicationsSearch']['search_status'] ])
            //->andFilterWhere(['like', 'page_id', $post['ApplicationsSearch']['search_page'] ])
            ->andFilterWhere(['like', 'pay', $this->pay])
            ->andFilterWhere(['like', 'purpose_of_payment', $this->purpose_of_payment])
            ->andFilterWhere(['like', 'pay_type', $this->pay_type])
            ->andFilterWhere(['like', 'utm', $this->utm])
            //->andFilterWhere(['like', 'name', $post['ApplicationsSearch']['search_name'] ])
            ->andFilterWhere(['like', 'contacts', $this->contacts])
            ->andFilterWhere(['like', 'fields', $this->fields]);

        return $dataProvider;
    }
}
