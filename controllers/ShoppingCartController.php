<?php

namespace app\controllers;

use Yii;
use app\models\ShoppingCart;
use app\models\ShoppingCartSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Products;
use app\models\additional\GoodsList;

/**
 * ShoppingCartController implements the CRUD actions for ShoppingCart model.
 */
class ShoppingCartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShoppingCart models.
     * @return mixed
     */
    public function actionIndex($user_id)
    {    
        $request = Yii::$app->request;
        $model = new ShoppingCart();
        $post = $request->post();
        $products = [];
        $counts = [];
        $session = Yii::$app->session;

        if($post['ShoppingCart']['step'] == null) {
            $model->step = 1;
            $count = 0;
            foreach ($session['list'] as $key => $value) {
                $products [] = $value['product_id'];
                $counts [] = $value['count'];
            }
        }

        if($post['ShoppingCart']['step'] == 1) {
            $model->step = 2;
            $session['products'] = $post['products'];
            $session['counts'] = $post['counts'];
        }

        if($post['ShoppingCart']['step'] == 2) {
            if($model->load($request->post()) && $model->validate()) $model->step = 3;
            else $model->step = 2;
        }

        if($post['ShoppingCart']['step'] == 3) {
            if($model->load($request->post()) && $model->validate()) {
                $summa = 0;
                foreach ($session['products'] as $key => $value) {
                    $product = Products::findOne($value);
                    $summa += $product->price * $session['counts'][$key];
                }
                $model->summa = $summa;
                $model->save();

                foreach ($session['products'] as $key => $value) {
                    $product = Products::findOne($value);
                    $goodList = new GoodsList();
                    $goodList->shopping_cart_id = $model->id;
                    $goodList->product_id = $value;
                    $goodList->count = $session['counts'][$key];
                    $goodList->save();
                }

                $session['products'] = null;
                $session['counts'] = null;
                $session['list'] = null;
                Yii::$app->session->setFlash('success', 'Успешно выполнено!');
                return $this->redirect(['/products/shop?user_id='.$user_id]);
            }
            else $model->step = 3;
        }

        return $this->render('create', [
            'model' => $model,
            'products' => $products,
            'counts' => $counts,
            'user_id' => $user_id,
        ]);
    }

    /**
     * Creates a new ShoppingCart model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCart()
    {
        $request = Yii::$app->request;
        $model = new ShoppingCart();

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
       
    }

    /**
     * Delete an existing ShoppingCart model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the ShoppingCart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShoppingCart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShoppingCart::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
