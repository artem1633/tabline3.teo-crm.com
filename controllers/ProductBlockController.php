<?php

namespace app\controllers;

use Yii;
use app\models\blocks\ProductBlock;
use app\models\blocks\ProductBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

use app\models\PageBlocks;
use app\models\Directory;
use app\models\UsersPage;
use app\models\Products;

/**
 * ProductBlockController implements the CRUD actions for ProductBlock model.
 */
class ProductBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new ProductBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new ProductBlock();
        $model->visible = 1;
        $model->visible_in_menu = 1;
        $model->foto_visible = 1;
        $model->name_visible = 1;
        $model->description_visible = 1;
        $model->price_visible = 1;
        $model->contact_form_visible = 1;
        $model->button_visible = 1;
        $model->checkbox = 1;
        $model->name_in_menu = 'Товары и услуги';

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){
                                
                $post = $request->post();
                $model->load($post);
                $number = ProductBlock::getLastId();
                $i=-1;
                $proverka = 0;
                foreach ($post['products'] as $value) {
                    $i++;
                    $product = new ProductBlock();
                    $product->page_id = $page_id;
                    $product->number = $number;
                    $product->view = $model->view;
                    $product->product_id = $value;
                    $product->checkbox = $model->checkbox;
                    $product->foto_visible = $model->foto_visible;
                    $product->name_visible = $model->name_visible;
                    $product->description_visible = $model->description_visible;
                    $product->price_visible = $model->price_visible;
                    $product->contact_form_visible = $model->contact_form_visible;
                    $product->button_visible = $model->button_visible;
                    $product->visible = $model->visible;
                    $product->title_text = $model->title_text;
                    $product->visible_in_menu = $model->visible_in_menu;
                    $product->name_in_menu = $model->name_in_menu;
                    if($product->save()) $proverka = 1;
                }

                if($proverka) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'product_block';
                    $page_block->sorting = UsersPage::getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Товары и услуги",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/product-block/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($request->post()) {

                $post = $request->post();
                $model->load($post);
                $number = ProductBlock::getLastId();
                $i=-1;
                $proverka = 0;
                foreach ($post['products'] as $value) {
                    $i++;
                    $product = new ProductBlock();
                    $product->page_id = $page_id;
                    $product->number = $number;
                    $product->view = $model->view;
                    $product->product_id = $value;
                    $product->checkbox = $model->checkbox;
                    $product->foto_visible = $model->foto_visible;
                    $product->name_visible = $model->name_visible;
                    $product->description_visible = $model->description_visible;
                    $product->price_visible = $model->price_visible;
                    $product->contact_form_visible = $model->contact_form_visible;
                    $product->button_visible = $model->button_visible;
                    $product->visible = $model->visible;
                    $product->title_text = $model->title_text;
                    $product->visible_in_menu = $model->visible_in_menu;
                    $product->name_in_menu = $model->name_in_menu;
                    if($product->save()) $proverka = 1;
                }

                if($proverka) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'product_block';
                    $page_block->sorting = UsersPage::getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return $this->redirect(['/users-page/page', 'user_id' => UsersPage::findOne($page_id)->user_id ]);
            } else {
                return $this->render('/blocks/product-block/create', [
                    'model' => $model,
                ]);
            }
        }       
    }

    /**
     * Updates an existing ProductBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($number, $page_id)
    {
        $request = Yii::$app->request;
        $products = ProductBlock::find()->where(['number' => $number])->all();
        $count = ProductBlock::find()->where(['number' => $number])->count();
        $model = ProductBlock::find()->where(['number' => $number])->one();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if( $request->post() ) {

                $post = $request->post();
                $model->load($request->post());
                //удаление слайды из бд
                foreach ($products as $product) {
                    $isset = 0;
                    foreach ($post['product_id'] as $key => $value) {
                        if( $value == $product->id ) $isset = 1;
                    }
                    if($isset == 0) $product->delete();
                }

                $max_number = -1;
                foreach ($post['product_id'] as $key => $value) {
                    $max_number = $key;
                    $product = ProductBlock::findOne($value);
                    if($product != null) {
                        $product->page_id = $page_id;
                        $product->number = $number;
                        $product->view = $model->view;
                        $product->product_id = $post['products'][$key];
                        $product->checkbox = $model->checkbox;
                        $product->foto_visible = $model->foto_visible;
                        $product->name_visible = $model->name_visible;
                        $product->description_visible = $model->description_visible;
                        $product->price_visible = $model->price_visible;
                        $product->contact_form_visible = $model->contact_form_visible;
                        $product->button_visible = $model->button_visible;
                        $product->visible = $model->visible;
                        $product->title_text = $model->title_text;
                        $product->visible_in_menu = $model->visible_in_menu;
                        $product->name_in_menu = $model->name_in_menu;
                        $product->save();
                    }
                }

                $i = -1;
                if(ProductBlock::find()->where(['number' => $number])->one() == null) $max_number = -1;
                foreach ($post['products'] as $key => $value) {
                    $i++;
                    if($key > $max_number) {

                        $product = new ProductBlock();
                        $product->page_id = $page_id;
                        $product->number = $number;
                        $product->view = $model->view;
                        $product->product_id = $value;
                        $product->checkbox = $model->checkbox;
                        $product->foto_visible = $model->foto_visible;
                        $product->name_visible = $model->name_visible;
                        $product->description_visible = $model->description_visible;
                        $product->price_visible = $model->price_visible;
                        $product->contact_form_visible = $model->contact_form_visible;
                        $product->button_visible = $model->button_visible;
                        $product->visible = $model->visible;
                        $product->title_text = $model->title_text;
                        $product->visible_in_menu = $model->visible_in_menu;
                        $product->name_in_menu = $model->name_in_menu;
                        $product->save();
                    }
                }
                
                if(ProductBlock::find()->where(['number' => $number])->one() == null) {
                    PageBlocks::find()->where(['table_name' => 'product_block', 'field_id' => $number ])->one()->delete();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/product-block/update', [
                        'products' => $products,
                        'number' => $number,
                        'count' => $count,
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($request->post()) {

                $post = $request->post();
                $model->load($request->post());
                //удаление слайды из бд
                foreach ($products as $product) {
                    $isset = 0;
                    foreach ($post['product_id'] as $key => $value) {
                        if( $value == $product->id ) $isset = 1;
                    }
                    if($isset == 0) $product->delete();
                }

                $max_number = -1;
                foreach ($post['product_id'] as $key => $value) {
                    $max_number = $key;
                    $product = ProductBlock::findOne($value);
                    if($product != null){
                        $product->page_id = $page_id;
                        $product->number = $number;
                        $product->view = $model->view;
                        $product->product_id = $post['products'][$key];
                        $product->checkbox = $model->checkbox;
                        $product->foto_visible = $model->foto_visible;
                        $product->name_visible = $model->name_visible;
                        $product->description_visible = $model->description_visible;
                        $product->price_visible = $model->price_visible;
                        $product->contact_form_visible = $model->contact_form_visible;
                        $product->button_visible = $model->button_visible;
                        $product->visible = $model->visible;
                        $product->title_text = $model->title_text;
                        $product->visible_in_menu = $model->visible_in_menu;
                        $product->name_in_menu = $model->name_in_menu;
                        $product->save();
                    }
                }

                $i = -1;
                if(ProductBlock::find()->where(['number' => $number])->one() == null) $max_number = -1;
                foreach ($post['products'] as $key => $value) {
                    $i++;
                    if($key > $max_number) {

                        $product = new ProductBlock();
                        $product->page_id = $page_id;
                        $product->number = $number;
                        $product->view = $model->view;
                        $product->product_id = $value;
                        $product->checkbox = $model->checkbox;
                        $product->foto_visible = $model->foto_visible;
                        $product->name_visible = $model->name_visible;
                        $product->description_visible = $model->description_visible;
                        $product->price_visible = $model->price_visible;
                        $product->contact_form_visible = $model->contact_form_visible;
                        $product->button_visible = $model->button_visible;
                        $product->visible = $model->visible;
                        $product->title_text = $model->title_text;
                        $product->visible_in_menu = $model->visible_in_menu;
                        $product->name_in_menu = $model->name_in_menu;
                        $product->save();
                    }
                }
                
                if(ProductBlock::find()->where(['number' => $number])->one() == null) {
                    PageBlocks::find()->where(['table_name' => 'product_block', 'field_id' => $number ])->one()->delete();
                }

                return $this->redirect(['/users-page/page', 'user_id' => UsersPage::findOne($page_id)->user_id ]);
            }else{
                return $this->render('/blocks/product-block/update', [
                    'products' => $products,
                    'number' => $number,
                    'count' => $count,
                    'model' => $model,
                ]);
            }
        }       
    }

    //Получение списоков товар
    public function actionLists()
    {
        $product = \app\models\Products::find()->where(['user_id' => Yii::$app->user->identity->id, 'status' => 'active'])->all();
        $data = '';
        foreach ($product as $value) {
           $data .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $data;
    }

    /**
     * Delete an existing ProductBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($number)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'product_block', 'field_id' => $number ])->one();
        if($pageBlock != null){
            $pageBlock->delete();
        }
        $products = ProductBlock::find()->where(['number' => $number])->all();
        foreach ($products as $value) {
            $value->delete();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the ProductBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
