<?php

namespace app\controllers;

use Yii;
use app\models\blocks\MapBlock;
use app\models\blocks\MapBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;
use app\models\UsersPage;
use app\models\blocks\MapMarks;

use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

/**
 * MapBlockController implements the CRUD actions for MapBlock model.
 */
class MapBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new MapBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new MapBlock(); 
        $model->page_id = $page_id;
        $model->center_x = $model->getCoordinateX();
        $model->center_y = $model->getCoordinateY();
        $model->visible = 1;
        $model->visible_in_menu = 1;
        $model->name_in_menu = 'Карта';
        $model->save();

        $page_block = new PageBlocks();
        $page_block->page_id = $page_id;
        $page_block->table_name = 'map_block';
        $page_block->sorting = $model->page->getSortNumber($page_id);
        $page_block->field_id = $model->id;
        $page_block->save();
        return $this->redirect(['update', 'id' => $model->id]);     
    }

    /**
     * Updates an existing MapBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $marks = [];
        $marks_list = MapMarks::find()->where(['map_id' => $id])->all();
        
        foreach ($marks_list as $value) {
            $marks [] = new Placemark(new Point($value->coordinate_x, $value->coordinate_y), 
                    [ 
                        'balloonContent' => '<strong>'.$value->title.'</strong> &nbsp&nbsp'.
                        Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                            ['/map-marks/update','id'=>$value->id],
                            [
                                'style'=>'font-size:14px;', 
                                'data-pjax'=>'0', 
                                'title' => 'Изменить метку',
                            ]
                        ).
                        '&nbsp'.
                        Html::a('<i class="glyphicon glyphicon-trash text-danger"></i>',
                            ['/map-marks/delete','id'=> $value->id ],
                            [
                                'style'=>'font-size:14px;',
                                'role'=>'modal-remote',
                                'title' => 'Удалить',
                                'data-confirm'=>false, 
                                'data-method'=>false,
                                'data-request-method'=>'post',
                                'data-toggle'=>'tooltip',
                                'data-confirm-title'=>'Подтвердите действие',
                                'data-confirm-message'=>'Вы уверены что хотите удалить этого метку?'
                        ])
                        .' <br>'.$value->description, ], 
                    [
                        'draggable' => false,
                        'preset' => 'islands#dotIcon',
                        'iconColor' => $value->color,
                ] 
            );
            
        } 

        return $this->render('/blocks/map-block/update', [
            'model' => $model,
            'marks' => $marks,
        ]);
    }

    //Настройка карты
    public function actionSetting($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#map-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Настройка",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/map-block/setting_form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Delete an existing MapBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'map_block', 'field_id' => $id ])->one();
        $mapMarks = MapMarks::find()->where(['map_id' => $id])->all();
        
        if($pageBlock != null) {
            $pageBlock->delete();
        }
        
        foreach ($mapMarks as $value) {
            $value->delete();
        }
        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the MapBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MapBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MapBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
