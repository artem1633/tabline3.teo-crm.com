<?php

namespace app\controllers;

use Yii;
use app\models\Applications;
use app\models\ApplicationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\additional\Contacts;
use app\models\additional\Columns;
use app\models\Template;

/**
 * ApplicationsController implements the CRUD actions for Applications model.
 */
class ApplicationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $tariff = Yii::$app->user->identity->tariff->key;
        if($tariff != 'pro') return $this->redirect(['/site/errors', 'type' => 1 ]);
        $this->enableCsrfValidation = true;
        return parent::beforeAction($action);        
    }

    /**
     * Lists all Applications models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $request = Yii::$app->request;
        $post = $request->post();
        $searchModel = new ApplicationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->id, $post);
        $contact = new Contacts();
        $contactProvider = $contact->searchContacts(Yii::$app->request->queryParams,Yii::$app->user->identity->id, $post);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'contactProvider' => $contactProvider,
            'contact' => $contact,
            'post' => $post,
        ]);
    }


    /**
     * Displays a single Applications model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Заявка №". $id,
                    'size' => 'normal',
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Updates an existing Applications model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    //изменить статус заявок
    public function actionSetStatus($status,$id)
    {
        $model = $this->findModel($id);
        $model->status = $status;
        $model->save();
    }

    //изменение значения полю мин.оплаты на просмотре заявок
    public function actionSetPayType($status,$id)
    {
        $model = $this->findModel($id);
        $model->pay_type = $status;
        if($status != null) {
            $model->pay = 'payed';
            $model->status = 'ready';
            $model->pay_date = date('Y-m-d H:i:s');
        }else{
            $model->pay = 'not_payed';
            $model->status = 'working';
            $model->pay_date = null;
        }

        $model->save();
    }

    /**
     * Delete an existing Applications model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    //Настроить колонки таблицы “Заявок”.
    public function actionSorting()
    {
        $request = Yii::$app->request;
        $user_id = Yii::$app->user->identity->id;       

        if ($request->post()) {
            $post = $request->post();
            $command = Yii::$app->db->createCommand();
            //aktiv columnlarni bazaga yozilishi
            $active = $post['active'];
            $strlen = strlen( $active );
            $id = ''; $order_number = 0;
            for( $i = 0; $i <= $strlen; $i++ ) {
                $char = substr( $active, $i, 1 );             
                if($char == ',') {
                    $order_number++;
                    $command->update('columns',['status'=>1,'order_number'=>$order_number],['id' => $id])->execute();
                    $id = '';
                }
                else $id .= $char;
            }

            $order_number++;
            $command->update('columns',['status'=>1,'order_number'=>$order_number],['id'=>$id])->execute();

            //aktiv bolmagan columnlarni bazaga yozilishi
            $noactive = $post['no-active'];
            $strlen = strlen( $noactive );
            $id = ''; 
            for( $i = 0; $i <= $strlen; $i++ ) {
                $char = substr( $noactive, $i, 1 );             
                if($char == ',') {
                    $command->update('columns', ['status' => 0 ], [ 'id' => $id ])->execute();
                    $id = '';
                }
                else $id .= $char;
            }
            $command->update('columns', ['status' => 0 ], [ 'id' => $id ])->execute();

            return $this->redirect(['/applications/index']);
        }else{
            $column = Columns::find()->where(['user_id'=>$user_id,'table_name'=>'applications'])->one();
            if($column == null ) {
                Applications::setDefaultValues($user_id);
            }

            $columns = Columns::find()
                ->where([
                    'user_id' => $user_id, 
                    'status' => 1, 
                    'table_name' => 'applications'
                ])
                ->orderBy([ 'order_number' => SORT_ASC ])
                ->all();
            $active = []; $i=0;
            foreach ($columns as $column) {
                $i++;
                $name = Template::getColumnsTemplate($i, $column->name);
                $active += [
                    $column->id => ['content' => $name],
                ];
            }

            $columns = Columns::find()->where(['user_id' => $user_id, 'status' => 0, 'table_name' => 'applications'])->all();
            $noactive = []; $i=0;
            foreach ($columns as $column) {
                $i++;
                $name = Template::getColumnsTemplate($i, $column->name);
                $noactive += [
                    $column->id => ['content' => $name],
                ];
            }
            
            return $this->render('sorting',[
                'active' => $active,
                'noactive' => $noactive,
            ]);
        }
    }


    /**
     * Finds the Applications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Applications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Applications::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
