<?php

namespace app\controllers;

use Yii;
use app\models\UsersPage;
use app\models\UsersPageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\blocks\TextBlock;
use app\models\PageBlocks;
use app\models\blocks\LinkBlock;
use app\models\blocks\SeparatorBlock;
use app\models\blocks\HtmlBlock;
use app\models\blocks\AvatarBlock;
use app\models\blocks\SocialBlock;
use app\models\blocks\VideoBlock;
use app\models\blocks\MessengerBlock;
use app\models\blocks\QuestionBlock;
use app\models\blocks\UniqueSellingBlock;
use app\models\blocks\CaruselBlock;
use app\models\blocks\AdvantageBlock;
use app\models\blocks\MapBlock;
use app\models\blocks\ContactFormBlock;
use app\models\blocks\ProductBlock;
use app\models\additional\Clicks;
use app\models\additional\HistoryPages;
use yii\web\UploadedFile;

use app\models\blocks\MapMarks;
use katzz0\yandexmaps\Map;
use katzz0\yandexmaps\JavaScript;
use katzz0\yandexmaps\objects\Placemark;
use katzz0\yandexmaps\Polyline;
use katzz0\yandexmaps\Point;
use katzz0\yandexmaps\Canvas as YandexMaps;

/**
 * UsersPageController implements the CRUD actions for UsersPage model.
 */
class UsersPageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsersPage models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersPageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($user_id)
    {
        $pages = UsersPage::find()->where(['user_id' => $user_id])->andWhere(['!=', 'is_main', 1 ])->all();
        $actual = UsersPage::find()->where(['user_id' => $user_id, 'is_main' => 1])->one();
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Страницы",
                    'content'=>$this->renderAjax('view', [
                        'pages' => $pages,
                        'user_id' => $user_id,
                        'actual' => $actual,
                    ]),
                ];    
        }else{
            return $this->render('view', [
                'pages' => $pages,
                'user_id' => $user_id,
                'actual' => $actual,
            ]);
        }
    }

    /**
     * Updates an existing Users Page model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'content'=>$this->renderAjax('_update_form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    //Вкладка "Страница"
    public function actionPage($user_id = null, $id = null)
    {    
        $request = Yii::$app->request;
        if($request->post()) {
            $post = $request->post();
            $active = $post['active'];
            $strlen = strlen( $active );
            $page_block_id = ''; $order_number = 0;
            for( $i = 0; $i <= $strlen; $i++ ) {
                $char = substr( $active, $i, 1 );             
                if($char == ',') {
                    $order_number++;
                    Yii::$app->db->createCommand()->update('page_blocks', 
                        [ 'sorting' => $order_number ], 
                        [ 'id' => $page_block_id ])
                    ->execute();
                    $page_block_id = '';
                }
                else $page_block_id .= $char;
            }
            $order_number++;
            Yii::$app->db->createCommand()->update('page_blocks', 
                [ 'sorting' => $order_number ], 
                [ 'id' => $page_block_id ])
            ->execute();
            Yii::$app->session->setFlash('success', "Успешно выполнено");
        }

        if($user_id != null) {
            $model = UsersPage::find()->where(['user_id' => $user_id, 'active' => 1])->one();
        }
        if($id != null) {
            $model = UsersPage::findOne($id);
        }

        $active = [];
        if($model != null) {
            $i=0;
            $blocks = PageBlocks::find()->where(['page_id' => $model->id])->orderBy([ 'sorting' => SORT_ASC])->all();
            foreach ($blocks as $block) {
                $i++;
                if( $block->table_name == 'text_block') {
                    $textBlock = TextBlock::findOne($block->field_id);
                    $name = $textBlock->getItemValues();
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }
                if( $block->table_name == 'link_block') {
                    $linkBlock = LinkBlock::findOne($block->field_id);
                    $name = $linkBlock->getItemValues();
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }
                if( $block->table_name == 'separator_block') {
                    $separatorBlock = SeparatorBlock::findOne($block->field_id);
                    $name = $separatorBlock->getItemValues();
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }
                if( $block->table_name == 'html_block') {
                    $htmlBlock = HtmlBlock::findOne($block->field_id);
                    $name = $htmlBlock->getItemValues();
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'avatar_block') {
                    $avatarBlock = AvatarBlock::findOne($block->field_id);
                    $name = $avatarBlock->getItemValues();
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'social_block') {
                    $socialBlock = SocialBlock::findOne($block->field_id);
                    $name = $socialBlock->getItemValues($this->actionGetSocial($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'video_block') {
                    $videoBlock = VideoBlock::findOne($block->field_id);
                    $name = $videoBlock->getItemValues($this->actionGetVideo($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'messenger_block') {
                    $messengerBlock = MessengerBlock::findOne($block->field_id);
                    $name = $messengerBlock->getItemValues($this->actionGetMessenger($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'question_block') {
                    $questionBlock = QuestionBlock::find()->where(['number' => $block->field_id])->one();
                    $name = $questionBlock->getItemValues($this->actionGetQuestions($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'carusel_block') {
                    $caruselBlock = CaruselBlock::find()->where(['number' => $block->field_id])->one();
                    $name = $caruselBlock->getItemValues($this->actionGetCarusel($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'advantage_block') {
                    $advantageBlock = AdvantageBlock::find()->where(['number' => $block->field_id])->one();
                    $name = $advantageBlock->getItemValues($this->actionGetAdvantage($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'unique_selling_block') {
                    $uniqueSellingBlock = UniqueSellingBlock::findOne($block->field_id);
                    $name = $uniqueSellingBlock->getItemValues($this->actionGetUniqueSelling($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'map_block') {
                    $mapBlock = MapBlock::findOne($block->field_id);
                    $name = $mapBlock->getItemValues($this->actionGetMap($mapBlock));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'contact_form_block') {
                    $contactFormBlock = ContactFormBlock::findOne($block->field_id);
                    $name = $contactFormBlock->getItemValues($this->actionGetContact($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'page_block') {
                    $usersPage = UsersPage::findOne($block->field_id);
                    $name = $usersPage->getItemValues();
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }

                if( $block->table_name == 'product_block') {
                    $productBlock = ProductBlock::find()->where(['number' => $block->field_id])->one();
                    $name = $productBlock->getItemValues($this->actionGetProduct($block->field_id));
                    $active += [
                        $block->id => ['content' => $name],
                    ];
                }
            }
        }
        
        return $this->render('page', [
            'model' => $model,
            'user_id' => $user_id,
            'id' => $id,
            'active' => $active,
        ]);
    }

    //Изменение место блока.
    public function actionChangeOrder($page_id)
    {
        $request = Yii::$app->request;
        $model = UsersPage::findOne($page_id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model != null) {
            $active = []; $i=0;
            $columns = TextBlock::find()->where(['page_id' => $model->id])->orderBy([ 'sorting' => SORT_ASC])->all();
            foreach ($columns as $column) {
                $i++;
                $size = $column->getTextSizeDescription();
                $align = $column->getAlignmentSizeDescription();
                if($column->text == '') {
                    $text = 'Редактировать текст';
                }else {
                    $text = $column->text;
                }
                $name = UsersPage::getColumnsTemplate($align, $size, $text);
                $active += [
                    $column->id => ['content' => $name],
                ];
            }
        }

        if($request->post()) {

            $post = $request->post();
            $active = $post['active'];
            $strlen = strlen( $active );
            $id = ''; $order_number = 0;
            
            for( $i = 0; $i <= $strlen; $i++ )  {
                $char = substr( $active, $i, 1 );             
                if($char == ',') {
                    $order_number++;
                    Yii::$app->db->createCommand()->update('text_block', 
                        [ 'sorting' => $order_number ], 
                        [ 'id' => $id ])
                    ->execute();
                    $id = '';
                }
                else $id .= $char;
            }
            $order_number++;
            Yii::$app->db->createCommand()->update('text_block', 
                [ 'sorting' => $order_number ], 
                [ 'id' => $id ])
            ->execute();
        }else{
            return [
                'title'=> "Изменить порядок",
                'content'=>$this->renderAjax('change_form', [
                    'model' => $model,
                    'user_id' => $user_id,
                    'active' => $active,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];    
        }
    }

    //Добавить новую страницу.
    public function actionAdd($user_id, $active, $is_main)
    {
        $request = Yii::$app->request;
        $model = new UsersPage();
        $model->user_id = $user_id;
        $model->active = $active;
        $model->is_main = $is_main;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->load($request->post()) && $model->save()){
            return [
                'forceReload'=>'#page-pjax',
                'forceClose'=>true,
            ];    
        }else{
             return [
                'title'=> "Добавить",
                'content'=>$this->renderAjax('add', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    //Добавить новых шаблон страниц.
    public function actionAddTemplate($id = null)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($id == null) {
            $model = new UsersPage();
            $model->user_id = null;
            $model->active = 0;
            $model->is_main = 0;
            $model->type = 1;            
        }else{
            $model = $this->findModel($id);
        }

        if($model->load($request->post()) && $model->save()){
            $model->file = UploadedFile::getInstance($model, 'file');
            if(!empty($model->file)) {
                $model->file->saveAs('images/templates/'.$model->id.'.'.$model->file->extension);
                Yii::$app->db->createCommand()->update('users_page', 
                    [ 'image' => $model->id.'.'.$model->file->extension ], 
                    [ 'id' => $model->id ])
                ->execute();
            }

            return [
                'forceReload'=>'#template-pjax',
                'forceClose'=>true,
            ];    
        }else{
             return [
                'title'=> "Добавить",
                'content'=>$this->renderAjax('_template_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
            ];        
        }
    }

    // Добавить новую страницу.
    public function actionAddPage($page_id)
    {
        $request = Yii::$app->request;
        $model = new UsersPage();

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post())){
            $page_block = new PageBlocks();
            $page_block->page_id = $page_id;
            $page_block->table_name = 'page_block';
            $page_block->sorting = $model->getSortNumber($page_id);
            $page_block->field_id = $model->page_number;
            $page_block->save();
            return [
                'forceReload'=>'#page-pjax',
                'forceClose'=>true,
            ];         
        }else{           
            return [
                'title'=> "Добавить страницу",
                'content'=>$this->renderAjax('/users-page/_add_page', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])        
            ];         
        }
    }

    //Добавить новый блок
    public function actionAddBlock($page_id)
    {
        $request = Yii::$app->request;
        $tariff = Yii::$app->user->identity->tariff->key;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Новый блок",
                'size' => 'normal',
                'content'=>$this->renderAjax('blocks', [
                    'page_id' => $page_id,
                    'tariff' => $tariff,
                ]),
            ];
        }else{
            return $this->render('blocks', [
                'page_id' => $page_id,
                'tariff' => $tariff,
            ]);
        }
    }

    //Страница "Шаблоны страниц"
    public function actionTemplate()
    {
        $templates = UsersPage::find()->where(['type' => 1])->all();
        return $this->render('templates', [
            'templates' => $templates,
        ]);
    }

    //Импортировать шаблон страниц
    public function actionImportTemplate($type, $template_id)
    {
        $template = UsersPage::findOne($template_id);
        $user_id = Yii::$app->user->identity->id;
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($type == 'main') {
            $userPage = UsersPage::find()->where(['user_id' => $user_id, 'is_main' => 1])->one();
        }
        if($userPage == null || $type == 'profile') {
            $model = new UsersPage();
            $model->user_id = $user_id;
            if($type == 'main') {
                $model->active = 1;
                $model->is_main = 1;
            }else{
                $model->active = 0;
                $model->is_main = 0;
            }

            if($model->load($request->post()) && $model->save()) {
                $model->setTemplateBlocks($template_id);
                return [
                    'title'=> "Создать страница",
                    'content'=> '<span style="color:red; font-size:16px; font-weight:bold;"><center>Успешно выполнено</center></span>',
                    'forceReload'=>'#template-pjax',
                    //'forceClose'=>true,
                ];    
            }else{
                return [
                    'title'=> "Добавить",
                    'content'=>$this->renderAjax('_add_form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];      
            }
        }else{
            $userPage->setTemplateBlocks($template_id);
            return [
                'title'=> "Создать страница",
                'content'=> '<span style="color:red; font-size:16px; font-weight:bold;"><center>Успешно выполнено</center></span>',
                'forceReload'=>'#template-pjax',
                //'forceClose'=>true,
            ];
        }

    }

    // Изменение актуальных страниц пользователя на другую страницу. 
    public function actionSetMain($user_id, $page_id)
    {
        $actual = UsersPage::find()->where(['user_id' => $user_id, 'active' => 1])->one();
        if($actual != null) {
            $actual->active = 0;
            $actual->save();
        }

        $next_actual = UsersPage::find()->where(['id' => $page_id])->one();
        if($next_actual != null) {
            $next_actual->active = 1;
            $next_actual->save();
        }
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax',];  
    }

    //Просмотр карты.
    public function actionGetMap($map)
    {
        $marks = [];
        $marks_list = MapMarks::find()->where(['map_id' => $map->id])->all();
        
        foreach ($marks_list as $value) {
            $marks [] = new Placemark(new Point($value->coordinate_x, $value->coordinate_y), 
                    [ 
                        'balloonContent' => '<strong>'.$value->title.'</strong>'
                        .' <br>'.$value->description, ], 
                    [
                        'draggable' => false,
                        'preset' => 'islands#dotIcon',
                        'iconColor' => $value->color,
                ] 
            );            
        }
        return $this->renderPartial('/users-page/blocks/_map_block_in_phone', ['model' => $map, 'marks' => $marks,]);
    }

    //Получить блока "Вопросы и ответы"
    public function actionGetQuestions($id)
    {
        $questionBlock = QuestionBlock::find()->where(['number' => $id])->all();
        return $this->renderPartial('/users-page/blocks/_question_block_in_phone', ['questionBlock' => $questionBlock]);
    }

    //Получить блока "Уникалные предложение"
    public function actionGetUniqueSelling($id)
    {
        $uniqueSellingBlock = UniqueSellingBlock::findOne($id);
        return $this->renderPartial('/users-page/blocks/_unique_selling_block_in_phone', ['uniqueSellingBlock' => $uniqueSellingBlock]);
    }
    
    //Получить блока "Карусел картинок"
    public function actionGetCarusel($id)
    {
        $caruselBlock = CaruselBlock::find()->where(['number' => $id])->all();
        return $this->renderPartial('/users-page/blocks/_carusel_block_in_phone', ['caruselBlock' => $caruselBlock]);
    }

    //Получить блока "Мессенджеры"
    public function actionGetMessenger($id)
    {
        $messengerBlock = MessengerBlock::findOne($id);
        return $this->renderPartial('/users-page/blocks/_messenger_block_in_phone', ['messengerBlock' => $messengerBlock]);
    }

    //Получить блока "Социальные сети"
    public function actionGetSocial($id)
    {
        $socialBlock = SocialBlock::findOne($id);
        return $this->renderPartial('/users-page/blocks/_social_block_in_phone', ['socialBlock' => $socialBlock]);
    }

    //Получить блока "Форма контактов"
    public function actionGetContact($id)
    {
        $contactBlock = ContactFormBlock::findOne($id);
        return $this->renderPartial('/users-page/blocks/_contact_form_block_in_phone', ['model' => $contactBlock]);
    }

    //Получить блока "Преимущества"
    public function actionGetAdvantage($id)
    {
        $advantageBlock = AdvantageBlock::find()->where(['number' => $id])->all();
        return $this->renderPartial('/users-page/blocks/_advantage_block_in_phone', ['advantageBlock' => $advantageBlock]);
    }

    //Получить блока "Видео"
    public function actionGetVideo($id)
    {
        $videoBlock = VideoBlock::findOne($id);
        return $this->renderPartial('/users-page/blocks/_video_block_in_phone', ['videoBlock' => $videoBlock]);
    }

    //Получить блока "Продукты"
    public function actionGetProduct($id)
    {
        $productBlock = ProductBlock::find()->where(['number' => $id])->all();
        $user_id = ProductBlock::find()->where(['number' => $id])->one()->page->user_id;
        if($user_id == null) $user_id = Yii::$app->user->identity->id;
        return $this->renderPartial('/users-page/blocks/_product_block_in_phone', ['productBlock' => $productBlock, 'user_id' => $user_id]);
    }

    //Удалить блок страниц
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'page_block', 'field_id' => $id ])->one();
        if($pageBlock != null)
        {
            $pageBlock->delete();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    //Статистика кликов
    public function actionSetClick($id, $table_name)
    {
        $click = new Clicks();
        $click->table_name = $table_name;
        $click->date = date('Y-m-d');

        if($table_name == 'page_block') {
            $user_page = UsersPage::findOne($id);
            $click->field_id = $user_page->id;
            $click->name = $user_page->name;
        }

        if($table_name == 'video_block') {
            $videoBlock = VideoBlock::findOne($id);
            $click->field_id = $videoBlock->id;
            $click->name = $videoBlock->link;
        }

        if($table_name == 'link_block') {
            $linkBlock = LinkBlock::findOne($id);
            $click->field_id = $linkBlock->id;
            $click->name = $linkBlock->caption;
        }

        if($table_name == 'messenger_block') {
            $messengerBlock = MessengerBlock::findOne($id);
            $click->field_id = $messengerBlock->id;
            $click->name = null;
        }

        if($table_name == 'carusel_block') {
            $click->field_id = $id;
            $click->name = null;
        }

        if($table_name == 'social_block') {
            $click->field_id = $id;
            $click->name = null;
        }

        $click->save();
    }

    /**
     * Delete an existing UsersPage model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
        }else{
            return $this->redirect(['index']);
        }       
    }

    /**
     * Finds the UsersPage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersPage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersPage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
