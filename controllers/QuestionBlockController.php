<?php

namespace app\controllers;

use Yii;
use app\models\blocks\QuestionBlock;
use app\models\blocks\QuestionBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;

/**
 * QuestionBlockController implements the CRUD actions for QuestionBlock model.
 */
class QuestionBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new QuestionBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;   
        $model = new QuestionBlock();      
        $model->visible = 1;
        $model->visible_in_menu = 1;
        $model->name_in_menu = 'Вопросы и ответы';

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->post()){
                $post = $request->post();
                $number = QuestionBlock::getLastId();
                $proverka = 0;
                foreach ($post['name'] as $key => $value) {
                    if($value != null && $value != '') {
                        $proverka = 1;
                        $question = new QuestionBlock(); 
                        $question->number = $number;
                        $question->page_id = $page_id;
                        $question->question_title = $value;
                        $question->answer = $post['answer'][$key];
                        $question->visible = $post['QuestionBlock']['visible'];
                        $question->visible_in_menu = $post['QuestionBlock']['visible_in_menu'];
                        $question->title_text = $post['QuestionBlock']['title_text'];
                        $question->name_in_menu = $post['QuestionBlock']['name_in_menu'];
                        $question->save();
                    }
                }

                if($proverka == 1) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'question_block';
                    $page_block->sorting = $question->page->getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }
                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Вопросы и ответы",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/question-block/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($request->post()) {
                $post = $request->post();
                $number = QuestionBlock::getLastId();
                $proverka = 0;
                foreach ($post['name'] as $key => $value) {
                    if($value != null && $value != '') {
                        $proverka = 1;
                        $question = new QuestionBlock(); 
                        $question->number = $number;
                        $question->page_id = $page_id;
                        $question->question_title = $value;
                        $question->answer = $post['answer'][$key];
                        $question->visible = $post['QuestionBlock']['visible'];
                        $question->visible_in_menu = $post['QuestionBlock']['visible_in_menu'];
                        $question->title_text = $post['QuestionBlock']['title_text'];
                        $question->name_in_menu = $post['QuestionBlock']['name_in_menu'];
                        $question->save();
                    }
                }

                if($proverka == 1) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'question_block';
                    $page_block->sorting = $question->page->getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }
                return $this->redirect(['users', 'id' => 1]);
            } else {
                return $this->render('/blocks/question-block/create', [
                    'model' => $model,
                ]);
            }
        }       
    }

    /**
     * Updates an existing QuestionBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($number, $page_id)
    {
        $request = Yii::$app->request;
        $questions = QuestionBlock::find()->where(['number' => $number])->all();
        $count = QuestionBlock::find()->where(['number' => $number])->count();
        $model = QuestionBlock::find()->where(['number' => $number])->one();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if( $request->post() ){

                $post = $request->post();
                $number = $post['number'];
                $proverka = 0;
                PageBlocks::find()->where([ 'field_id' => $number, 'table_name' => 'question_block' ])->one()->delete();
                foreach ($questions as $value) {
                    $value->delete();
                }
                foreach ($post['name'] as $key => $value){
                    if($value != null && $value != ''){
                        $proverka = 1;
                        $question = new QuestionBlock(); 
                        $question->number = $number;
                        $question->page_id = $page_id;
                        $question->question_title = $value;
                        $question->answer = $post['answer'][$key];
                        $question->visible = $post['QuestionBlock']['visible'];
                        $question->visible_in_menu = $post['QuestionBlock']['visible_in_menu'];
                        $question->title_text = $post['QuestionBlock']['title_text'];
                        $question->name_in_menu = $post['QuestionBlock']['name_in_menu'];
                        $question->save();
                    }
                }
                
                if($proverka == 1) {
                    $page_block = new PageBlocks();
                    $page_block->page_id = $page_id;
                    $page_block->table_name = 'question_block';
                    $page_block->sorting = $question->page->getSortNumber($page_id);
                    $page_block->field_id = $number;
                    $page_block->save();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/question-block/update', [
                        'questions' => $questions,
                        'number' => $number,
                        'count' => $count,
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Delete an existing QuestionBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($number)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'question_block', 'field_id' => $number ])->one();
        if($pageBlock != null){
            $pageBlock->delete();
        }
        $questions = QuestionBlock::find()->where(['number' => $number])->all();
        foreach ($questions as $value) {
            $value->delete();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the QuestionBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuestionBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuestionBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
