<?php

namespace app\controllers;

use Yii;
use app\models\blocks\SocialBlock;
use app\models\blocks\SocialBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;
use app\models\additional\Clicks;

/**
 * SocialBlockController implements the CRUD actions for SocialBlock model.
 */
class SocialBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new SocialBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new SocialBlock(); 
        $model->page_id = $page_id;
        $model->setDefaultValues();

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $page_block = new PageBlocks();
                $page_block->page_id = $page_id;
                $page_block->table_name = 'social_block';
                $page_block->sorting = $model->page->getSortNumber($page_id);
                $page_block->field_id = $model->id;
                $page_block->save();
                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Добавить социальные сети",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/social-block/create', [
                        'model' => $model,
                        'day_count' => 0,
                        'week_count' => 0,
                        'month_count' => 0,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('/blocks/social-block/create', [
                    'model' => $model,
                    'day_count' => 0,
                        'week_count' => 0,
                        'month_count' => 0,
                ]);
            }
        }       
    }

    /**
     * Updates an existing SocialBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $week = date('Y-m-d', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) );

        $day_count = Clicks::find()
            ->where([
                'table_name' => 'social_block', 
                'field_id' => $id 
            ])
            ->andWhere(['date' => date('Y-m-d')])
            ->count(); 

        $week_count = Clicks::find()
            ->where([
                'table_name' => 'social_block', 
                'field_id' => $id 
            ])
            ->andWhere(['between', 'date', $week, date('Y-m-d') ])
            ->count(); 

        $month_count = Clicks::find()
            ->where([
                'table_name' => 'social_block', 
                'field_id' => $id 
            ])
            ->andWhere(['between', 'date', date('Y-m-01'), date('Y-m-d') ])
            ->count();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/blocks/social-block/update', [
                        'model' => $model,
                        'day_count' => $day_count,
                        'week_count' => $week_count,
                        'month_count' => $month_count,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Delete an existing SocialBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'social_block', 'field_id' => $id ])->one();
        if($pageBlock != null) {
            $pageBlock->delete();
        }
        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the SocialBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SocialBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SocialBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
