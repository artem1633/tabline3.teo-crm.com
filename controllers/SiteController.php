<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegisterForm;
use MetzWeb\Instagram\Instagram;
use app\models\Settings;
use app\models\Users;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    //Вход на страницу пользователя
    public function actionPage()
    {
        $this->redirect([ '/users-page/page?user_id=' . Yii::$app->user->identity->id ]);
    }

    //Авторизация
    public function actionAvtorizatsiya()
    {
        if(isset(Yii::$app->user->identity->id)) {
            return $this->render('error', [
                'name' => 'Ошибка',
                'message' => '',
            ]);
        }else{
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    // Ошибки сайта
    public function actionErrors($type = null)
    {
        $message = '';
        switch ($type) {
            case 0: $message = 'Запрашиваемой страницы не существует.';
            case 1: $message = 'У вас нет прав войти на эту страницу';
            case 2: $message = 'Для того что бы реализовать это действия у вас нет прав';
            default: $message = '';
        }

        return $this->render('error', [
            'name' => 'Ошибка',
            'message' => $message,
        ]);
    }

    // Авторизовать через Инстаграмма
    public function actionInstagram()
    {
        $instagram = new Instagram(array(
            'apiKey'      => Settings::find()->where(['key' => 'instagram_client_id'])->one()->value,
            'apiSecret'   => Settings::find()->where(['key' => 'instagram_client_secret_key'])->one()->value,
            'apiCallback' => Settings::find()->where(['key' => 'instagram_api_callback'])->one()->value,
        ));

        $code = $_GET['code'];
        $data = $instagram->getOAuthToken($code);

        $session = Yii::$app->session;
        if(isset($data->user->full_name)) {
            $session['social'] = 1;
            $session['user_id'] = $data->user->id;
            $session['full_name'] = $data->user->full_name;
            $session['profile_picture'] = $data->user->profile_picture;

            $user = Users::find()->where([ 'instagram_user_id' => $session['user_id'] ])->one();
            if($user != null) {
                $login = new LoginForm();
                $login->social = $session['social'];
                $login->user_id = $session['user_id'];
                $login->username = $user->login;
                $login->password = $user->password;
                if ($login->loginWithSocialNetwork()) {
                    Yii::$app->session->setFlash('error', 'Вы уже зарегистрировались на этом аккаунтом');
                    return $this->goBack();
                }
            }
        }
        
        $this->redirect(['register']);
    }

    // Авторизовать через ВК
    public function actionVkontakte()
    {
        $app_id = Settings::find()->where(['key' => 'vk_id_app'])->one()->value;
        $app_secret = Settings::find()->where(['key' => 'vk_secret_key'])->one()->value;
        $my_url = Settings::find()->where(['key' => 'vk_api_callback'])->one()->value;
     
        if(isset($_GET["code"])) { 
            $code = $_GET["code"]; 
        }
        $token_url =  'https://api.vk.com/oauth/access_token?client_id='.
            $app_id.
            '&client_secret='.
            $app_secret.
            '&code='.$code.
            '&redirect_uri='.$my_url;

        $params = json_decode(@file_get_contents($token_url));

        $user_id = $params->user_id;
        $request_params = array(
            'uid' => $user_id,
            'fields' => 'photo,nickname',
            'v' => '5.52',
            'access_token' => $params->access_token,
            'sig' => md5('/method/getProfiles?uid='.
                $user_id.
                '&fields=photo,nickname&v=5.52&access_token='.
                $params->access_token.
                $params->secret)
        );
        $get_params = http_build_query($request_params);
        $result = json_decode(file_get_contents('https://api.vk.com/method/getProfiles?'. $get_params));

        $session = Yii::$app->session;
        if(isset($result->response[0]->first_name)) {
            $session['social'] = 3;
            $session['user_id'] = $result->response[0]->id;
            $session['full_name'] = $result->response[0]->first_name . ' ' . $result->response[0]->last_name;
            $session['profile_picture'] = $result->response[0]->photo;
        }

        $user = Users::find()->where([ 'vk_user_id' => $session['user_id'] ])->one();
        if($user != null) {
            $login = new LoginForm();
            $login->social = $session['social'];
            $login->user_id = $session['user_id'];
            $login->username = $user->login;
            $login->password = $user->password;
            if ($login->loginWithSocialNetwork()) {
                return $this->goBack();
            }
        }
        $this->redirect(['register']);
    }

    // Авторизовать через Facebook
    public function actionFacebook()
    {
        $client_id  = Settings::find()->where(['key' => 'facebook_client_id'])->one()->value;
        $client_secret  = Settings::find()->where(['key' => 'facebook_client_secret'])->one()->value;
        $redirect_uri  = Settings::find()->where(['key' => 'facebook_redirect_uri'])->one()->value;

        $url = 'https://www.facebook.com/dialog/oauth';

        if (isset($_GET['code'])) {

            $result = false;
            $params = array(
                'client_id'     => $client_id,
                'redirect_uri'  => $redirect_uri,
                'client_secret' => $client_secret,
                'code'          => $_GET['code']
            );

            $url = 'https://graph.facebook.com/oauth/access_token';
            $tokenInfo = null;
            parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

            if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
                $params = array('access_token' => $tokenInfo['access_token']);

                $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . 
                    urldecode(http_build_query($params))), true);

                if (isset($userInfo['id'])) {
                    $userInfo = $userInfo;
                    $result = true;
                }
            }
        }
    }

    // Авторизовать через Google
    public function actionGoogle()
    {
        $client_id_google = Settings::find()->where(['key' => 'google_client_id'])->one()->value;
        $client_secret_key = Settings::find()->where(['key' => 'google_client_secret_key'])->one()->value;
        $redirect_uri_google = Settings::find()->where(['key' => 'google_redirect_uri'])->one()->value;
        $url_google = 'https://accounts.google.com/o/oauth2/auth';

        if (isset($_GET['code'])) {
            $result = false;

            $params = array(
                'client_id'     => $client_id_google,
                'client_secret' => $client_secret_key,
                'redirect_uri'  => $redirect_uri_google,
                'grant_type'    => 'authorization_code',
                'code'          => $_GET['code']
            );

            $url = 'https://accounts.google.com/o/oauth2/token';
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result_curl = curl_exec($curl);
        curl_close($curl);

        $tokenInfo = json_decode($result_curl, true);

        if (isset($tokenInfo['access_token'])) {
            $params['access_token'] = $tokenInfo['access_token'];

            $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . 
                urldecode(http_build_query($params))), true);
            if (isset($userInfo['id'])) {
                $userInfo = $userInfo;
                $result = true;
            }

            if ($result) {
                $session = Yii::$app->session;

                $session['social'] = 4;
                $session['user_id'] = $userInfo['id'];
                $session['full_name'] = $userInfo['name'];
                $session['profile_picture'] = $userInfo['picture'];

                $user = Users::find()->where([ 'google_account_user_id' => $session['user_id'] ])->one();
                if($user != null) {
                    $login = new LoginForm();
                    $login->social = $session['social'];
                    $login->user_id = $session['user_id'];
                    $login->username = $user->login;
                    $login->password = $user->password;
                    if ($login->loginWithSocialNetwork()) {
                        return $this->goBack();
                    }
                }
                $this->redirect(['register']);
            }
        }
    }

    // Окно регистрации
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();
        $session = Yii::$app->session;
        if(isset($session['social'])) $model->social = $session['social'];
        if(isset($session['user_id'])) $model->user_id = (string)$session['user_id'];
        if(isset($session['full_name'])) $model->fio = $session['full_name'];
        if(isset($session['profile_picture'])) $model->avatar = $session['profile_picture'];
        if(isset($session['partner_code'])) $model->paternity_user_code = $session['partner_code'];

        if($model->load(Yii::$app->request->post()) && $model->register()) {
            $login = new LoginForm();
            $login->username = $model->login;
            $login->password = $model->password;
            if ($login->login()) {
                return $this->goBack();
            }
            else{
                return $this->render('register', [
                    'model' => $model,
                ]);
            }
        } 
        else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    //Регистрировать через кода партнера.
    public function actionPartner($code)
    {
        $user = Users::find()->where(['partner_code' => $code])->one();
        if($user != null) {
            if(!Yii::$app->user->isGuest) Yii::$app->user->logout();
            $session = Yii::$app->session;
            $session['partner_code'] = $code;
            return $this->redirect(['login']);
        }
        else {
            return $this->goBack();
        }
    }

}
