<?php

namespace app\controllers;

use Yii;
use app\models\blocks\UniqueSellingBlock;
use app\models\blocks\UniqueSellingBlockSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PageBlocks;
use yii\web\UploadedFile;

/**
 * UniqueSellingBlockController implements the CRUD actions for UniqueSellingBlock model.
 */
class UniqueSellingBlockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new UniqueSellingBlock model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($page_id)
    {
        $request = Yii::$app->request;
        $model = new UniqueSellingBlock(); 
        $model->page_id = $page_id;
        $model->visible = 1;
        $model->visible_in_menu = 1;
        $model->name_in_menu = 'Предложение';

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $page_block = new PageBlocks();
                $page_block->page_id = $page_id;
                $page_block->table_name = 'unique_selling_block';
                $page_block->sorting = $model->page->getSortNumber($page_id);
                $page_block->field_id = $model->id;
                $page_block->save();

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)) {

                    $model->file->saveAs('images/unique-selling-block/' . $model->id. '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('unique_selling_block', 
                        [ 'image' => $model->id. '.' . $model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }

                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Уникальное торговое предложение",
                    'size' => 'large',
                    'content'=>$this->renderAjax('/blocks/unique-selling-block/create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('/blocks/unique-selling-block/create', [
                    'model' => $model,
                ]);
            }
        }       
    }

    /**
     * Updates an existing UniqueSellingBlock model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $model->file = UploadedFile::getInstance($model, 'file');
                
                if(!empty($model->file)) {
                    if (file_exists('images/unique-selling-block/'.$model->image)) {
                        unlink(Yii::getAlias('images/unique-selling-block/'. $model->image));
                    }
                    $model->file->saveAs('images/unique-selling-block/' . $model->id. '.' . $model->file->extension);
                    Yii::$app->db->createCommand()->update('unique_selling_block', 
                        [ 'image' => $model->id. '.' . $model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }
                return [
                    'forceReload'=>'#page-pjax',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('/blocks/unique-selling-block/update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Delete an existing UniqueSellingBlock model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $pageBlock = PageBlocks::find()->where(['table_name' => 'unique_selling_block', 'field_id' => $id ])->one();
        if($pageBlock != null) {
            $pageBlock->delete();
        }

        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#page-pjax'];
    }

    /**
     * Finds the UniqueSellingBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UniqueSellingBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UniqueSellingBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
