<?php

namespace app\controllers;

use Yii;
use app\models\UsersAccess;
use app\models\UsersAccessSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * UsersAccessController implements the CRUD actions for UsersAccess model.
 */
class UsersAccessController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsersAccess models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersAccessSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 1);
        $dataProvider1 = $searchModel->search(Yii::$app->request->queryParams, 2);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProvider1,
        ]);
    }

    /**
     * Creates a new UsersAccess model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new UsersAccess();  
        $model->user_from = Yii::$app->user->identity->id;
        $model->view_page = 1;
        $model->access_type = 1;
        $model->update_page = 1;
        $model->view_application = 1;
        $model->update_application = 1;
        $model->view_statics = 1;
        $model->update_settings = 1;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post()) && $model->validate()){
                $access = UsersAccess::find()
                    ->where([
                        'user_from' => Yii::$app->user->identity->id, 
                        'user_to' => $model->user_to
                    ])
                    ->one();
                if($access == null) {
                    $model->save();
                }else{
                    $access->access_type = $model->access_type;
                    $access->view_page = $model->view_page;
                    $access->update_page = $model->update_page;
                    $access->view_application = $model->view_application;
                    $access->update_application = $model->update_application;
                    $access->view_statics = $model->view_statics;
                    $access->update_settings = $model->update_settings;
                    $access->save();
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Совместный доступ",
                    'size' => 'normal',
                    'forceClose'=>true,
                ];         
            }else{           
                return [
                    'title'=> "Совместный доступ",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])        
                ];         
            }
        }
       
    }

    /**
     * Updates an existing UsersAccess model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->user = $model->userTo->login;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Совместный доступ",
                    'size' => 'normal',
                    'forceClose'=>true,
                ];    
            }else{
                 return [
                    'title'=> "Совместный доступ",
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    //Просмотр совместный доступ пользователя
    public function actionViewAccess($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->user = $model->userFrom->login;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;            
            return [
                'title'=> "Совместный доступ",
                'content'=>$this->renderAjax('_access_view_form', [
                    'model' => $model,
                ]),
            ];
        }
    }

    /**
     * Delete an existing UsersAccess model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete an existing UsersAccess model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemove($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#access-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the UsersAccess model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsersAccess the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersAccess::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
