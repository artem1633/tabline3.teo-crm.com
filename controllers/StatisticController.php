<?php

namespace app\controllers;

use app\models\LogsSearch;
use Yii;
use yii\web\Controller;
use app\models\additional\Clicks;
use app\models\UsersPage;
use app\models\blocks\VideoBlock;
use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class StatisticController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу статистику
     * @return mixed
     */
    public function actionIndex()
    {
        $user_id = Yii::$app->user->identity->id;
        $type = Yii::$app->user->identity->type;
        $tariff = Yii::$app->user->identity->tariff->key;
        $pagesId = [];
        $dayStatistics = [];
        $weekStatistics = [];
        $monthStatistics = [];

        if($tariff == 'pro') {
            $pageClickDay = Clicks::find()->where(['table_name' => 'page_block'])->all(); 
            foreach ($pageClickDay as $value) {

                $page = UsersPage::findOne($value->field_id);
                if($type == 3) {
                    if($user_id == $page->user_id) $pagesId [] = $value->field_id;
                }
                else $pagesId [] = $value->field_id;
            }

            $pagesId = array_unique($pagesId);

            foreach ($pagesId as $value) {
                $page = UsersPage::findOne($value);
                $week = date('Y-m-d', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) );
                $day_count = Clicks::find()
                    ->where([
                        'table_name' => 'page_block', 
                        'field_id' => $value 
                    ])
                    ->andWhere(['date' => date('Y-m-d')])
                    ->count(); 

                $week_count = Clicks::find()
                    ->where([
                        'table_name' => 'page_block', 
                        'field_id' => $value 
                    ])
                    ->andWhere(['between', 'date', $week, date('Y-m-d') ])
                    ->count(); 

                $month_count = Clicks::find()
                    ->where([
                        'table_name' => 'page_block', 
                        'field_id' => $value 
                    ])
                    ->andWhere(['between', 'date', date('Y-m-01'), date('Y-m-d') ])
                    ->count(); 

                $dayStatistics [] = [
                    'title' => 'Ссылка / ' . $page->name,
                    'count' => $day_count,
                ];

                $weekStatistics [] = [
                    'title' => 'Ссылка / ' . $page->name,
                    'count' => $week_count,
                ];

                $monthStatistics [] = [
                    'title' => 'Ссылка / ' . $page->name,
                    'count' => $month_count,
                ];
            }
        }

        $pagesId = [];
        $pageClickDay = Clicks::find()->where(['table_name' => 'video_block'])->all(); 
        foreach ($pageClickDay as $value) {
            
            $video = VideoBlock::findOne($value->field_id);
            if($type == 3) {
                if($user_id == $video->page->user_id) $pagesId [] = $value->field_id;                
            }
            else $pagesId [] = $value->field_id;
        }
        $pagesId = array_unique($pagesId);
        
        foreach ($pagesId as $value) {
            $video = VideoBlock::findOne($value);
            $week = date('Y-m-d', strtotime('-7 day' , strtotime ( date('Y-m-d') ) ) );
            $day_count = Clicks::find()
                ->where([
                    'table_name' => 'video_block', 
                    'field_id' => $value 
                ])
                ->andWhere(['date' => date('Y-m-d')])
                ->count(); 

            $week_count = Clicks::find()
                ->where([
                    'table_name' => 'video_block', 
                    'field_id' => $value 
                ])
                ->andWhere(['between', 'date', $week, date('Y-m-d') ])
                ->count(); 

            $month_count = Clicks::find()
                ->where([
                    'table_name' => 'video_block', 
                    'field_id' => $value 
                ])
                ->andWhere(['between', 'date', date('Y-m-01'), date('Y-m-d') ])
                ->count(); 
                
            $dayStatistics [] = [
                'title' => 'Видео / ' . $video->link,
                'count' => $day_count,
            ];

            $weekStatistics [] = [
                'title' => 'Видео / ' . $video->link,
                'count' => $week_count,
            ];

            $monthStatistics [] = [
                'title' => 'Видео / ' . $video->link,
                'count' => $month_count,
            ];
        }

        return $this->render('index', [
            'dayStatistics' => $dayStatistics,
            'weekStatistics' => $weekStatistics,
            'monthStatistics' => $monthStatistics,
        ]);

    }
}
